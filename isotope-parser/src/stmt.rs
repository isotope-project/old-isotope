use super::*;

/// Parse a statement
pub fn stmt(input: &str) -> IResult<&str, Stmt> {
    alt((map(let_, Stmt::Let), map(join, Stmt::Join)))(input)
}

/// Parse a let-statement
pub fn let_(input: &str) -> IResult<&str, Let> {
    map(
        tuple((
            tag(LET),
            ws,
            opt_ident,
            opt(ws),
            opt(preceded(
                preceded(tag(":"), opt(ws)),
                terminated(expr, opt(ws)),
            )),
            tag("="),
            opt(ws),
            expr,
            opt(ws),
            tag(";"),
        )),
        |(_let, _ws0, ident, _ws1, ty, _eq, _ws2, value, _ws3, _sc)| Let {
            ident: ident.map(str::to_string),
            ty: ty.map(Arc::new),
            value: Arc::new(value),
        },
    )(input)
}

/// Parse a join-statement
pub fn join(input: &str) -> IResult<&str, Join> {
    map(
        tuple((
            join_kind,
            preceded(
                opt(ws),
                alt((
                    pair(opt(terminated(decimal, ws)), expr),
                    map(natural, |n| (None, Expr::Natural(n))),
                )),
            ),
            opt(preceded(delimited(opt(ws), tag(MAP_ARROW), opt(ws)), expr)),
            preceded(opt(ws), tag(";")),
        )),
        |(form, (work_limit, source), target, _sc)| Join {
            form,
            source: Arc::new(source),
            work_limit,
            target: target.map(Arc::new),
        },
    )(input)
}

/// Parse a join kind
pub fn join_kind(input: &str) -> IResult<&str, Form> {
    alt((
        value(Form::Null, tag(ASSERT_EQ)),
        value(Form::Head, tag(HEAD)),
        value(Form::Beta, tag(BETA)),
        value(Form::Eta, tag(ETA)),
        value(Form::BetaEta, tag(BETA_ETA)),
    ))(input)
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn join_parses() {
        let examples = ["#eq #true => #false;"];
        for example in &examples {
            let (rest, _parsed) = join(example).unwrap();
            assert_eq!(rest, "");
        }
    }
}
