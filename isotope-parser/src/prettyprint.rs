/*!
Prettyprint `isotope` ASTs
*/
use super::*;
use std::fmt::{self, Display, Formatter};

pub use pretty::*;

impl Stmt {
    /// Prettyprint a statement
    pub fn pretty<'a, D, A>(&'a self, allocator: &'a D) -> DocBuilder<'a, D, A>
    where
        D: DocAllocator<'a, A>,
        D::Doc: Clone,
        A: Clone,
    {
        match self {
            Stmt::Let(l) => l.pretty(allocator),
            Stmt::Join(j) => j.pretty(allocator),
        }
    }
}

impl Let {
    /// Prettyprint a let-statement
    pub fn pretty<'a, D, A>(&'a self, allocator: &'a D) -> DocBuilder<'a, D, A>
    where
        D: DocAllocator<'a, A>,
        D::Doc: Clone,
        A: Clone,
    {
        allocator
            .text(LET)
            .append(allocator.space())
            .append(allocator.text(self.ident.as_deref().unwrap_or("_")))
            .append(allocator.line())
            .append(if let Some(ty) = &self.ty {
                allocator
                    .text(":")
                    .append(allocator.space())
                    .append(ty.pretty(allocator))
                    .append(allocator.line())
            } else {
                allocator.nil()
            })
            .append(allocator.text(ASSIGN))
            .append(allocator.space())
            .append(self.value.pretty(allocator))
            .append(allocator.text(";"))
            .group()
    }
}

impl Join {
    /// Prettyprint a join-statement
    pub fn pretty<'a, D, A>(&'a self, allocator: &'a D) -> DocBuilder<'a, D, A>
    where
        D: DocAllocator<'a, A>,
        D::Doc: Clone,
        A: Clone,
    {
        self.form
            .pretty(allocator)
            .append(if let Some(work_limit) = self.work_limit {
                allocator
                    .space()
                    .append(allocator.text(format!("{}", work_limit)))
            } else {
                allocator.nil()
            })
            .append(allocator.line())
            .append(self.source.pretty(allocator))
            .append(if let Some(target) = &self.target {
                allocator
                    .text(MAP_ARROW)
                    .append(allocator.space())
                    .append(target.pretty(allocator))
            } else {
                allocator.nil()
            })
            .group()
    }
}

impl Expr {
    /// Prettyprint an expression
    pub fn pretty<'a, D, A>(&'a self, allocator: &'a D) -> DocBuilder<'a, D, A>
    where
        D: DocAllocator<'a, A>,
        D::Doc: Clone,
        A: Clone,
    {
        match self {
            Expr::Ident(this) => allocator.text(&this[..]),
            Expr::Var(this) => allocator.text(format!("#{}", this)),
            Expr::App(this) => this.pretty(allocator),
            Expr::Lambda(this) => this.pretty(allocator),
            Expr::Pi(this) => this.pretty(allocator),
            Expr::Universe(this) => this.pretty(allocator),
            Expr::Natural(this) => allocator.text(format!("{}", this)),
            Expr::Boolean(true) => allocator.text("#true"),
            Expr::Boolean(false) => allocator.text("#false"),
            Expr::Bool => allocator.text("#bool"),
            Expr::Unit => allocator.text("#unit"),
            Expr::Annotated(this) => this.pretty(allocator),
            Expr::Scope(this) => this.pretty(allocator),
            Expr::Enum(this) => this.pretty(allocator),
            Expr::Case(this) => this.pretty(allocator),
            Expr::Variant(this) => allocator.text("'").append(allocator.text(&this[..])),
        }
    }

    /// Convert an expression to a prettyprinted string
    pub fn to_pretty_string(&self, width: usize) -> String {
        let mut buf = Vec::new();
        self.pretty::<_, ()>(&RcAllocator)
            .1
            .render(width, &mut buf)
            .unwrap();
        String::from_utf8(buf).unwrap()
    }
}

impl App {
    /// Prettyprint an application
    pub fn pretty<'a, D, A>(&'a self, allocator: &'a D) -> DocBuilder<'a, D, A>
    where
        D: DocAllocator<'a, A>,
        D::Doc: Clone,
        A: Clone,
    {
        allocator
            .text("(")
            .append(
                allocator
                    .intersperse(self.0.iter().map(|e| e.pretty(allocator)), allocator.line())
                    .group(),
            )
            .append(allocator.text(")"))
    }
}

impl Lambda {
    /// Prettyprint a lambda function
    pub fn pretty<'a, D, A>(&'a self, allocator: &'a D) -> DocBuilder<'a, D, A>
    where
        D: DocAllocator<'a, A>,
        D::Doc: Clone,
        A: Clone,
    {
        let begin = if let Some(ty) = &self.param_ty {
            allocator
                .text("(λ")
                .append(allocator.space())
                .append(allocator.text(self.param_name.as_deref().unwrap_or("_")))
                .append(allocator.line())
                .append(allocator.text(":"))
                .append(allocator.space())
                .append(ty.pretty(allocator))
        } else if let Some(name) = &self.param_name {
            allocator.text("(λ").append(allocator.text(&name[..]))
        } else {
            allocator.text("(λ")
        };
        begin
            .append(allocator.line())
            .append(allocator.text("."))
            .append(allocator.space())
            .append(self.result.pretty(allocator))
            .append(allocator.text(")"))
            .group()
    }
}

impl Pi {
    /// Prettyprint a pi type
    pub fn pretty<'a, D, A>(&'a self, allocator: &'a D) -> DocBuilder<'a, D, A>
    where
        D: DocAllocator<'a, A>,
        D::Doc: Clone,
        A: Clone,
    {
        if let Some(name) = &self.param_name {
            allocator
                .text("(Π")
                .append(allocator.space())
                .append(allocator.text(name.as_deref().unwrap_or("_")))
                .append(allocator.line())
                .append(allocator.text(":"))
                .append(allocator.space())
                .append(self.param_ty.pretty(allocator))
                .append(allocator.line())
                .append(allocator.text("."))
                .append(allocator.space())
                .append(self.result.pretty(allocator))
                .append(allocator.text(")"))
                .group()
        } else {
            allocator
                .text("(")
                .append(self.param_ty.pretty(allocator))
                .append(allocator.line())
                .append(allocator.text(FN_ARROW))
                .append(allocator.line())
                .append(self.result.pretty(allocator))
                .append(allocator.text(")"))
                .group()
        }
    }
}

impl Universe {
    /// Prettyprint a universe
    pub fn pretty<'a, D, A>(&'a self, allocator: &'a D) -> DocBuilder<'a, D, A>
    where
        D: DocAllocator<'a, A>,
        D::Doc: Clone,
        A: Clone,
    {
        allocator.text(format!("{}", self))
    }
}

impl Display for Universe {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "{}{}",
            if self.is_var() {
                UNIVERSE_VAR
            } else {
                UNIVERSE
            },
            self.level()
        )
    }
}

impl Enum {
    /// Prettyprint an enumeration
    pub fn pretty<'a, D, A>(&'a self, allocator: &'a D) -> DocBuilder<'a, D, A>
    where
        D: DocAllocator<'a, A>,
        D::Doc: Clone,
        A: Clone,
    {
        allocator
            .text("#enum {")
            .append(allocator.line())
            .append(
                allocator.intersperse(
                    self.0
                        .iter()
                        .map(|variant| allocator.text("'").append(allocator.text(&variant[..]))),
                    allocator.line(),
                ),
            )
            .append(allocator.line())
            .append(allocator.text("}"))
            .group()
    }
}

impl Case {
    /// Prettyprint a case statement
    pub fn pretty<'a, D, A>(&'a self, allocator: &'a D) -> DocBuilder<'a, D, A>
    where
        D: DocAllocator<'a, A>,
        D::Doc: Clone,
        A: Clone,
    {
        allocator
            .text("#case {")
            .append(allocator.line())
            .append(allocator.intersperse(
                self.0.iter().map(|branch| branch.pretty(allocator)),
                allocator.text(";").append(allocator.line()),
            ))
            .append(allocator.line())
            .append(allocator.text("}"))
            .group()
    }
}

impl Branch {
    /// Prettyprint a branch of a case statement
    pub fn pretty<'a, D, A>(&'a self, allocator: &'a D) -> DocBuilder<'a, D, A>
    where
        D: DocAllocator<'a, A>,
        D::Doc: Clone,
        A: Clone,
    {
        self.pattern
            .pretty(allocator)
            .append(allocator.line())
            .append(allocator.text(MAP_ARROW))
            .append(allocator.line())
            .append(self.result.pretty(allocator))
            .group()
    }
}

impl Annotated {
    /// Prettyprint an annotated term
    pub fn pretty<'a, D, A>(&'a self, allocator: &'a D) -> DocBuilder<'a, D, A>
    where
        D: DocAllocator<'a, A>,
        D::Doc: Clone,
        A: Clone,
    {
        allocator
            .text("(")
            .append(self.term.pretty(allocator))
            .append(allocator.line())
            .append(allocator.text(":"))
            .append(allocator.space())
            .append(self.ty.pretty(allocator))
            .append(allocator.text(")"))
            .group()
    }
}

impl Scope {
    /// Prettyprint a scope
    pub fn pretty<'a, D, A>(&'a self, allocator: &'a D) -> DocBuilder<'a, D, A>
    where
        D: DocAllocator<'a, A>,
        D::Doc: Clone,
        A: Clone,
    {
        let sep = if self.stmts.is_empty() {
            allocator.nil()
        } else {
            allocator.hardline()
        };
        allocator
            .text("{")
            .append(sep.clone())
            .append(allocator.intersperse(
                self.stmts.iter().map(|stmt| stmt.pretty(allocator)),
                allocator.hardline(),
            ))
            .append(sep.clone())
            .append(self.result.pretty(allocator))
            .append(sep)
            .append(allocator.text("}"))
    }
}

impl Form {
    /// Prettyprint a reduction form
    pub fn pretty<'a, D, A>(&'a self, allocator: &'a D) -> DocBuilder<'a, D, A>
    where
        D: DocAllocator<'a, A>,
        D::Doc: Clone,
        A: Clone,
    {
        let txt = match self {
            Form::Null => ASSERT_EQ,
            Form::Head => HEAD,
            Form::Beta => BETA,
            Form::Eta => ETA,
            Form::BetaEta => BETA_ETA,
        };
        allocator.text(txt)
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn prettyprint_round_trip() {
        let examples = [
            "#true",
            "#false",
            "#bool",
            "λx.x",
            "#lambda x.x",
            "f x",
            "(f x)(f x)",
            "(λx.x)(λx.x)",
            "{
                #let x = y;
                x
            }",
            "(x: A)",
        ];
        for example in examples {
            let (rest, parsed) = expr(example).unwrap();
            debug_assert_eq!(rest, "");
            let pretty = parsed.to_pretty_string(70);
            let (rest, reparsed) = expr(&pretty[..]).unwrap();
            assert_eq!(rest, "", "{:?} for {:?}", pretty, example);
            assert_eq!(parsed, reparsed, "{:?} != {:?}", example, pretty);
        }
    }
}
