/*!
# isotope-parser

A parser for a simple textual representation of the `isotope` language, designed mainly for debugging and testing purposes
*/
#![forbid(missing_debug_implementations, missing_docs, unsafe_code)]

use nom::branch::*;
use nom::bytes::complete::*;
use nom::character::complete::*;
use nom::combinator::*;
use nom::multi::*;
use nom::sequence::*;
use nom::*;
use num_bigint::BigUint;
use smol_str::SmolStr;
use std::sync::Arc;

pub mod ast;
pub mod prettyprint;
pub mod token;
pub mod utils;

use ast::*;
use token::*;
use utils::*;

mod expr;
mod stmt;
pub use expr::*;
pub use stmt::*;

use smallvec::{smallvec, SmallVec};
