/*!
Parsers for `isotope` expressions
*/
use super::*;

/// Parse an `isotope` expression
pub fn expr(input: &str) -> IResult<&str, Expr> {
    let (mut rest, mut curr) = atom(input)?;
    let mut app = smallvec![];
    let pi = loop {
        match preceded(opt(ws), alt((map(atom, Some), value(None, tag(FN_ARROW)))))(rest) {
            Ok((rem, Some(arg))) => {
                rest = rem;
                app.push(Arc::new(curr));
                curr = arg;
            }
            Ok((rem, None)) => {
                rest = rem;
                break true;
            }
            Err(nom::Err::Incomplete(n)) => return Err(nom::Err::Incomplete(n)),
            Err(_) => break false,
        };
    };

    if !app.is_empty() {
        app.push(Arc::new(curr));
        curr = Expr::App(App(app));
    }

    if pi {
        let (rem, result) = preceded(opt(ws), expr)(rest)?;
        rest = rem;
        curr = Expr::Pi(Pi {
            param_name: None,
            param_ty: Arc::new(curr),
            result: Arc::new(result),
        });
    }

    Ok((rest, curr))
}

/// Parse a string forming a valid `isotope` identifier
///
/// An `isotope` identifier may be any sequence of non-whitespace characters which does not
/// contain a special character. This parser does *not* consume preceding whitespace!
///
/// # Examples
/// ```rust
/// # use isotope_parser::ident;
/// assert_eq!(ident("hello "), Ok((" ", "hello")));
/// assert!(ident(" bye").is_err());
/// assert!(ident("0x35").is_err());
/// assert_eq!(ident("x35"), Ok(("", "x35")));
/// assert_eq!(ident("你好"), Ok(("", "你好")));
/// let arabic = ident("الحروف العربية").unwrap();
/// let desired_arabic = (" العربية" ,"الحروف");
/// assert_eq!(arabic, desired_arabic);
/// ```
pub fn ident(input: &str) -> IResult<&str, &str> {
    verify(is_not(SPECIAL_CHARACTERS), |ident: &str| !is_keyword(ident))(input)
}

/// Parse a variable index
pub fn var(input: &str) -> IResult<&str, u32> {
    preceded(tag("#"), decimal)(input)
}

/// Parse an optional identifier
pub fn opt_ident(input: &str) -> IResult<&str, Option<&str>> {
    alt((value(None, tag(HOLE)), map(ident, Some)))(input)
}

/// Parse an (atomic) `isotope` expression
pub fn atom(input: &str) -> IResult<&str, Expr> {
    alt((
        sexpr,
        map(scope, Expr::Scope),
        map(var, Expr::Var),
        map(lambda, Expr::Lambda),
        map(pi, Expr::Pi),
        map(universe, Expr::Universe),
        map(universe_var, Expr::Universe),
        map(enum_, Expr::Enum),
        map(variant, |variant| Expr::Variant(variant.into())),
        map(natural, Expr::Natural),
        map(boolean, Expr::Boolean),
        value(Expr::Bool, tag(BOOL)),
        map(case, Expr::Case),
        map(ident, |ident| Expr::Ident(ident.into())),
    ))(input)
}

/// Parse an S-expression wrapped in parentheses, with an optional annotation
//TODO: annotation...
pub fn sexpr(input: &str) -> IResult<&str, Expr> {
    map_opt(
        tuple((
            preceded(tag("("), opt(ws)),
            opt(expr),
            delimited(
                opt(ws),
                opt(delimited(preceded(tag(":"), opt(ws)), expr, opt(ws))),
                tag(")"),
            ),
        )),
        |(_ws, expr, ty)| match (expr, ty) {
            (expr, None) => Some(expr.unwrap_or(Expr::App(App(smallvec![])))),
            (Some(term), Some(ty)) => Some(Expr::Annotated(Annotated {
                term: Arc::new(term),
                ty: Arc::new(ty),
            })),
            _ => None,
        },
    )(input)
}

/// Parse a lambda function
pub fn lambda(input: &str) -> IResult<&str, Lambda> {
    map(
        preceded(
            preceded(alt((tag(LAMBDA), tag(SIMPLE_LAMBDA))), opt(ws)),
            pair(
                opt(terminated(
                    pair(
                        opt_ident,
                        opt(preceded(delimited(opt(ws), tag(":"), opt(ws)), expr)),
                    ),
                    delimited(opt(ws), tag("."), opt(ws)),
                )),
                expr,
            ),
        ),
        |(param, result)| {
            let (param_name, param_ty) = param.unwrap_or((None, None));
            Lambda {
                param_name: param_name.map(SmolStr::from),
                param_ty: param_ty.map(Arc::new),
                result: Arc::new(result),
            }
        },
    )(input)
}

/// Parse a pi type
pub fn pi(input: &str) -> IResult<&str, Pi> {
    map(
        preceded(
            alt((terminated(tag(PI), opt(ws)), terminated(tag(SIMPLE_PI), ws))),
            pair(
                terminated(
                    pair(
                        terminated(opt_ident, delimited(opt(ws), tag(":"), opt(ws))),
                        expr,
                    ),
                    delimited(opt(ws), tag("."), opt(ws)),
                ),
                expr,
            ),
        ),
        |((param_name, param_ty), result)| Pi {
            param_name: Some(param_name.map(SmolStr::from)),
            param_ty: Arc::new(param_ty),
            result: Arc::new(result),
        },
    )(input)
}

/// Parse a typing universe
pub fn universe(input: &str) -> IResult<&str, Universe> {
    preceded(tag("#U"), map_res(decimal, Universe::try_const))(input)
}

/// Parse a universe variable
pub fn universe_var(input: &str) -> IResult<&str, Universe> {
    preceded(tag("#uv"), map_res(decimal, Universe::try_var))(input)
}

/// Parse an enumeration
pub fn enum_(input: &str) -> IResult<&str, Enum> {
    preceded(
        tag("#enum"),
        delimited(
            delimited(opt(ws), tag("{"), opt(ws)),
            map(separated_list0(ws, map(variant, SmolStr::new)), Enum),
            preceded(opt(ws), tag("}")),
        ),
    )(input)
}

/// Parse a variant
pub fn variant(input: &str) -> IResult<&str, &str> {
    preceded(tag("'"), ident)(input)
}

/// Parse a case statement
pub fn case(input: &str) -> IResult<&str, Case> {
    preceded(
        tag("#case"),
        delimited(
            delimited(opt(ws), tag("{"), opt(ws)),
            map(
                separated_list0(delimited(opt(ws), tag(";"), opt(ws)), branch),
                Case,
            ),
            preceded(delimited(opt(ws), opt(tag(";")), opt(ws)), tag("}")),
        ),
    )(input)
}

/// Parse a branch of a case statement
pub fn branch(input: &str) -> IResult<&str, Branch> {
    map(
        separated_pair(expr, delimited(opt(ws), tag(MAP_ARROW), opt(ws)), expr),
        |(pattern, result)| Branch {
            pattern: Arc::new(pattern),
            result: Arc::new(result),
        },
    )(input)
}

/// Parse a scope
pub fn scope(input: &str) -> IResult<&str, Scope> {
    delimited(
        preceded(tag("{"), opt(ws)),
        scope_inner,
        preceded(opt(ws), tag("}")),
    )(input)
}

/// Parse an inner scope
pub fn scope_inner(input: &str) -> IResult<&str, Scope> {
    map(
        tuple((separated_list0(opt(ws), stmt), preceded(opt(ws), expr))),
        |(stmts, result)| Scope {
            stmts,
            result: Arc::new(result),
        },
    )(input)
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn expr_parsing() {
        let f = Arc::new(Expr::Ident("f".into()));
        let x = Arc::new(Expr::Ident("x".into()));
        let f_x = Expr::App(App(smallvec![f.clone(), x.clone()]));
        let f_x_arc = Arc::new(f_x.clone());
        let id = Expr::Lambda(Lambda {
            param_name: Some("x".into()),
            param_ty: None,
            result: x,
        });
        let id_arc = Arc::new(id.clone());
        let examples = [
            ("f x", f_x),
            (
                "(f x) (f x)",
                Expr::App(App(smallvec![f_x_arc.clone(), f_x_arc.clone()])),
            ),
            (
                "(f x) (f x)",
                Expr::App(App(smallvec![f_x_arc.clone(), f_x_arc.clone()])),
            ),
            ("λx.x", id),
            (
                "(λx.x)(λx.x)",
                Expr::App(App(smallvec![id_arc.clone(), id_arc.clone()])),
            ),
        ];

        for (input, target) in examples {
            let (rest, output) = expr(input).unwrap();
            assert_eq!(rest, "");
            assert_eq!(output, target)
        }
    }

    #[test]
    fn boolean_parsing() {
        assert_eq!(expr("#true"), Ok(("", Expr::Boolean(true))));
        assert_eq!(expr("#false"), Ok(("", Expr::Boolean(false))));
        assert_eq!(expr("#bool"), Ok(("", Expr::Bool)));
    }

    #[test]
    fn ident_parsing() {
        assert_eq!(ident("x"), Ok(("", "x")));
        assert_eq!(expr("x"), Ok(("", Expr::Ident("x".into()))));
        assert!(ident("#true").is_err());
    }

    #[test]
    fn lambda_parsing() {
        let examples = [
            (
                "λx.x",
                Lambda {
                    param_name: Some("x".into()),
                    param_ty: None,
                    result: Expr::Ident("x".into()).into(),
                },
            ),
            (
                "#lambda x.x",
                Lambda {
                    param_name: Some("x".into()),
                    param_ty: None,
                    result: Expr::Ident("x".into()).into(),
                },
            ),
            (
                "λ#true",
                Lambda {
                    param_name: None,
                    param_ty: None,
                    result: Arc::new(Expr::Boolean(true)),
                },
            ),
            (
                "λ#0",
                Lambda {
                    param_name: None,
                    param_ty: None,
                    result: Arc::new(Expr::Var(0)),
                },
            ),
        ];
        for (input, target) in &examples {
            let (rest, output) = lambda(input).unwrap();
            assert_eq!(rest, "");
            assert_eq!(output, *target);
            let (rest, output) = expr(input).unwrap();
            assert_eq!(rest, "");
            assert_eq!(output, Expr::Lambda(target.clone()));
        }
    }

    #[test]
    fn full_pi_parsing() {
        let examples = [
            (
                "Π _: A. A",
                Pi {
                    param_name: Some(None),
                    param_ty: Expr::Ident("A".into()).into(),
                    result: Expr::Ident("A".into()).into(),
                },
            ),
            (
                "#pi x: A. A",
                Pi {
                    param_name: Some(Some("x".into())),
                    param_ty: Expr::Ident("A".into()).into(),
                    result: Expr::Ident("A".into()).into(),
                },
            ),
        ];
        for (input, target) in &examples {
            let (rest, output) = pi(input).unwrap();
            assert_eq!(rest, "");
            assert_eq!(output, *target);
            let (rest, output) = expr(input).unwrap();
            assert_eq!(rest, "");
            assert_eq!(output, Expr::Pi(target.clone()));
        }
    }

    #[test]
    fn short_pi_parsing() {
        let a_to_a = Expr::Pi(Pi {
            param_name: None,
            param_ty: Expr::Ident("A".into()).into(),
            result: Expr::Ident("A".into()).into(),
        });
        let arc_a_to_a = Arc::new(a_to_a.clone());
        let examples = [
            ("A -> A", a_to_a),
            (
                "X -> A -> A",
                Expr::Pi(Pi {
                    param_name: None,
                    param_ty: Expr::Ident("X".into()).into(),
                    result: arc_a_to_a.clone(),
                }),
            ),
            (
                "(A -> A) -> X",
                Expr::Pi(Pi {
                    param_name: None,
                    param_ty: arc_a_to_a,
                    result: Expr::Ident("X".into()).into(),
                }),
            ),
        ];
        for (input, target) in &examples {
            let (rest, output) = expr(input).unwrap();
            assert_eq!(rest, "");
            assert_eq!(output, *target);
        }
    }
}
