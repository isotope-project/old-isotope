use clap::{App, Arg};
use isotope_parser::utils::ws;
use isotope_repl::*;
use log::*;
use nom::combinator::*;
use nom::sequence::*;
use rustyline::{error::ReadlineError, Editor};

fn main() {
    // Handle command line arguments
    let matches = App::new("isotope-repl")
        .version("0.0.0")
        .author("Jad Ghalayini")
        .about("A REPL for interacting and experimenting with the isotope API")
        .arg(
            Arg::with_name("history")
                .short("h")
                .long("history")
                .value_name("FILE")
                .help("Sets a file to load history from")
                .takes_value(true),
        )
        .get_matches();
    //TODO: check if is a tty
    println!("isotope-repl 0.0.0");

    // Initialize logging
    pretty_env_logger::init_timed();

    // Initialize editor and helper
    let history = matches.value_of("history");
    let mut editor = Editor::<IsotopeHelper>::new();
    if let Some(history) = history {
        if let Err(err) = editor.load_history(history) {
            info!("No history loaded from {}: {}", history, err)
        } else {
            info!("Successfully loaded history from {}", history)
        }
    } else {
        info!("No history loaded")
    }
    let helper = IsotopeHelper::new();
    editor.set_helper(Some(helper));

    // Initialize repl state
    let mut repl = Repl::new(ReplConfig::default());

    // Read-Evaluate-Print Loop
    loop {
        let line = editor.readline(">> ");
        match line {
            Ok(line) => {
                let mut line = line.as_str();
                while let Ok((rest, (command_str, command))) =
                    preceded(opt(ws), consumed(command))(line)
                {
                    if let Err(err) = repl.handle(&command) {
                        //TODO: terminate if not a tty?
                        error!(
                            "Error handling command:\nCOMMAND: {:#?}\nERROR: {}",
                            command, err
                        );
                        break;
                    } else {
                        editor.add_history_entry(command_str);
                    }
                    line = rest;
                }
            }
            Err(ReadlineError::Interrupted) | Err(ReadlineError::Eof) => break,
            Err(err) => {
                println!("{}", err);
                break;
            }
        }
    }
}
