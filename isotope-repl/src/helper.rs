use super::*;

/// A helper for `isotope` input code.
#[derive(Completer, Helper, Highlighter)]
pub struct IsotopeHelper {
    hinter: HistoryHinter,
}

impl IsotopeHelper {
    pub fn new() -> IsotopeHelper {
        IsotopeHelper {
            hinter: HistoryHinter {},
        }
    }
}

impl Validator for IsotopeHelper {
    fn validate(&self, ctx: &mut ValidationContext) -> Result<ValidationResult, ReadlineError> {
        match terminated(many0_count(preceded(opt(ws), command)), opt(ws))(ctx.input()) {
            Ok(("", _n)) => Ok(ValidationResult::Valid(None)),
            Ok((_rest, _n)) => Ok(ValidationResult::Incomplete),
            Err(err) => Ok(ValidationResult::Invalid(Some(format!(
                "Parse error: {:#?}",
                err
            )))),
        }
    }
}

impl Hinter for IsotopeHelper {
    type Hint = String;
    fn hint(&self, line: &str, pos: usize, ctx: &Context<'_>) -> Option<String> {
        self.hinter.hint(line, pos, ctx)
    }
}
