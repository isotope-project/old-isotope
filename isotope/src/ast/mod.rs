/*!
Convert `isotope` terms to abstract syntax trees
*/
use crate::{term::AnnotationLike, Error, Term, TermId, Untyped, Value};
pub use isotope_parser::ast::{self, *};
use smol_str::SmolStr;

/// A context for converting terms to ASTs
pub trait ToAstCtx {
    /// Annotate a term in this context
    fn annotate(&self, term: &TermId) -> Result<Option<Expr>, Error>;
    /// Print a variable index, given an annotation
    fn var(&self, ix: u32, annot: Option<&TermId>) -> Result<Expr, Error>;
    /// Whether to merge applications
    fn merge_app(&self) -> bool;
    /// Whether to print the parameter type of lambda functions
    fn param_ty_lambda(&self, result: &TermId) -> bool;
    /// Try to convert a term to an AST in this context
    #[inline]
    fn try_to_ast_in(&self, _term: &TermId) -> Option<Result<Expr, Error>> {
        None
    }
    /// Cache the result of converting a term to an AST in this context
    #[inline]
    fn cache(&self, _term: &TermId, _ast: &ast::Expr) -> Result<(), Error> {
        Ok(())
    }
    /// Get the parameter name for a given result
    #[inline]
    fn param_name(&self, _result: &TermId) -> Result<Option<SmolStr>, Error> {
        Ok(None)
    }
}

/// A term which can be convered to an AST in a given context
pub trait ToAst {
    /// Recursively convert this term to an AST in a given context, *without* it's annotation
    fn to_raw_ast_in(&self, ctx: &mut (impl ToAstCtx + ?Sized)) -> Result<Expr, Error>;
}

/// Convert a term to it's un-annotated prettyprinted string
pub fn to_raw_pretty(
    term: &impl ToAst,
    width: usize,
    ctx: &mut (impl ToAstCtx + ?Sized),
) -> Result<String, Error> {
    let ast = term.to_raw_ast_in(ctx)?;
    Ok(ast.to_pretty_string(width))
}

impl TermId {
    /// Convert this `TermId` to an abstract syntax tree in the given context with it's annotation
    #[inline]
    pub fn to_ast_in(&self, ctx: &mut (impl ToAstCtx + ?Sized)) -> Result<Expr, Error> {
        if let Some(result) = ctx.try_to_ast_in(self) {
            return result;
        }
        let raw_ast = self.to_raw_ast_in(ctx)?;
        let result = if let Some(annotation) = ctx.annotate(self)? {
            Expr::Annotated(Annotated {
                term: raw_ast.into(),
                ty: annotation.into(),
            })
        } else {
            raw_ast
        };
        ctx.cache(self, &result)?;
        Ok(result)
    }
    /// Convert a term to it's prettyprinted string
    pub fn to_pretty_str(
        &self,
        width: usize,
        ctx: &mut (impl ToAstCtx + ?Sized),
    ) -> Result<String, Error> {
        let ast = self.to_ast_in(ctx)?;
        Ok(ast.to_pretty_string(width))
    }
    /// Convert a term to it's prettyprinted string for debugging
    pub fn dbg_pretty_str(&self) -> String {
        self.to_pretty_str(80, &mut PartiallyTyped)
            .unwrap_or_else(|err| format!("Error formatting term: {}", err))
    }
}

impl Term {
    /// Convert a term to it's prettyprinted string
    pub fn to_pretty_str(
        &self,
        width: usize,
        ctx: &mut (impl ToAstCtx + ?Sized),
    ) -> Result<String, Error> {
        let ast = self.to_raw_ast_in(ctx)?;
        Ok(ast.to_pretty_string(width))
    }
    /// Convert a term to it's prettyprinted string for debugging
    pub fn dbg_pretty_str(&self) -> String {
        self.to_pretty_str(80, &mut PartiallyTyped)
            .unwrap_or_else(|err| format!("Error formatting term: {}", err))
    }
}

impl ToAst for TermId {
    #[inline]
    fn to_raw_ast_in(&self, ctx: &mut (impl ToAstCtx + ?Sized)) -> Result<Expr, Error> {
        (**self).to_raw_ast_in(ctx)
    }
}

mod impl_to_ast {
    use super::{Expr, ToAst, ToAstCtx};
    use crate::*;

    impl ToAst for Term {
        #[inline]
        fn to_raw_ast_in(&self, ctx: &mut (impl ToAstCtx + ?Sized)) -> Result<Expr, Error> {
            for_term!(self; t => t.to_raw_ast_in(ctx))
        }
    }

    impl ToAst for App {
        fn to_raw_ast_in(&self, ctx: &mut (impl ToAstCtx + ?Sized)) -> Result<Expr, Error> {
            let left = self.left().to_ast_in(ctx)?;
            let right = self.right().to_ast_in(ctx)?;
            let result = match (ctx.merge_app(), left) {
                (true, Expr::App(mut a)) => {
                    a.0.push(right.into());
                    Expr::App(a)
                }
                (_, left) => Expr::App(ast::App(smallvec![left.into(), right.into()])),
            };
            Ok(result)
        }
    }

    impl ToAst for Lambda {
        fn to_raw_ast_in(&self, ctx: &mut (impl ToAstCtx + ?Sized)) -> Result<Expr, Error> {
            let result = self.result();
            let param_name = ctx.param_name(result)?;
            let param_ty = if ctx.param_ty_lambda(result) {
                self.param_ty()
                    .map(|param_ty| param_ty.to_ast_in(ctx))
                    .transpose()?
            } else {
                None
            };
            let result = result.to_ast_in(ctx)?;
            Ok(Expr::Lambda(ast::Lambda {
                param_name,
                param_ty: param_ty.map(Arc::new),
                result: result.into(),
            }))
        }
    }

    impl ToAst for Pi {
        fn to_raw_ast_in(&self, ctx: &mut (impl ToAstCtx + ?Sized)) -> Result<Expr, Error> {
            let result = self.result();
            let param_name = ctx.param_name(result)?;
            let param_ty = self.param_ty().to_ast_in(ctx)?;
            let result = result.to_ast_in(ctx)?;
            Ok(Expr::Pi(ast::Pi {
                param_name: Some(param_name),
                param_ty: param_ty.into(),
                result: result.into(),
            }))
        }
    }

    impl ToAst for Var {
        #[inline]
        fn to_raw_ast_in(&self, ctx: &mut (impl ToAstCtx + ?Sized)) -> Result<Expr, Error> {
            ctx.var(self.ix(), self.get_ty())
        }
    }

    impl ToAst for UniverseTerm {
        #[inline]
        fn to_raw_ast_in(&self, ctx: &mut (impl ToAstCtx + ?Sized)) -> Result<Expr, Error> {
            self.as_universe().to_raw_ast_in(ctx)
        }
    }

    impl ToAst for Universe {
        #[inline]
        fn to_raw_ast_in(&self, _ctx: &mut (impl ToAstCtx + ?Sized)) -> Result<Expr, Error> {
            //TODO: optimize?
            Ok(Expr::Universe(
                ast::Universe::try_new(self.level(), self.is_var())
                    .expect("Valid universes can never lead to level overflow"),
            ))
        }
    }

    impl ToAst for Refl {
        fn to_raw_ast_in(&self, _ctx: &mut (impl ToAstCtx + ?Sized)) -> Result<Expr, Error> {
            Err(Error::NotImplemented)
        }
    }

    impl ToAst for Id {
        fn to_raw_ast_in(&self, _ctx: &mut (impl ToAstCtx + ?Sized)) -> Result<Expr, Error> {
            Err(Error::NotImplemented)
        }
    }

    impl ToAst for Bool {
        #[inline]
        fn to_raw_ast_in(&self, _ctx: &mut (impl ToAstCtx + ?Sized)) -> Result<Expr, Error> {
            Ok(Expr::Bool)
        }
    }

    impl ToAst for Boolean {
        #[inline]
        fn to_raw_ast_in(&self, _ctx: &mut (impl ToAstCtx + ?Sized)) -> Result<Expr, Error> {
            Ok(Expr::Boolean(self.as_bool()))
        }
    }

    impl ToAst for DynamicTerm {
        fn to_raw_ast_in(&self, _ctx: &mut (impl ToAstCtx + ?Sized)) -> Result<Expr, Error> {
            Err(Error::NotImplemented)
        }
    }

    impl ToAst for Enum {
        fn to_raw_ast_in(&self, _ctx: &mut (impl ToAstCtx + ?Sized)) -> Result<Expr, Error> {
            Ok(Expr::Enum(ast::Enum(
                self.variants().iter().map(SmolStr::new).collect(),
            )))
        }
    }

    impl ToAst for Variant {
        fn to_raw_ast_in(&self, _ctx: &mut (impl ToAstCtx + ?Sized)) -> Result<Expr, Error> {
            Ok(Expr::Variant(self.name().into()))
        }
    }

    impl ToAst for Case {
        fn to_raw_ast_in(&self, ctx: &mut (impl ToAstCtx + ?Sized)) -> Result<Expr, Error> {
            let matches = self.matches();
            let branches: Vec<_> = self
                .cases()
                .iter()
                .enumerate()
                .map(|(ix, result)| {
                    let pattern = Case::make_match(ix, matches)?;
                    let result = result.to_raw_ast_in(ctx)?.into();
                    let pattern = pattern.to_raw_ast_in(ctx)?.into();
                    Ok(ast::Branch { result, pattern })
                })
                .collect::<Result<_, _>>()?;
            Ok(Expr::Case(ast::Case(branches)))
        }
    }
}

impl ToAstCtx for Untyped {
    #[inline]
    fn annotate(&self, _term: &TermId) -> Result<Option<Expr>, Error> {
        Ok(None)
    }

    #[inline]
    fn var(&self, ix: u32, _annot: Option<&TermId>) -> Result<Expr, Error> {
        Ok(Expr::Var(ix))
    }

    #[inline]
    fn merge_app(&self) -> bool {
        true
    }

    #[inline]
    fn param_ty_lambda(&self, _result: &TermId) -> bool {
        false
    }
}

/// Print out transported annotations and parameter types
#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash, PartialOrd, Ord)]

pub struct PartiallyTyped;

impl ToAstCtx for PartiallyTyped {
    fn annotate(&self, term: &TermId) -> Result<Option<Expr>, Error> {
        if let Some(annot) = term.annot() {
            if let Some(ty) = annot.diff_ty() {
                ty.to_ast_in(&mut Untyped).map(Some)
            } else {
                Ok(None)
            }
        } else {
            Ok(None)
        }
    }

    #[inline]
    fn var(&self, ix: u32, _annot: Option<&TermId>) -> Result<Expr, Error> {
        Ok(Expr::Var(ix))
    }

    #[inline]
    fn merge_app(&self) -> bool {
        true
    }

    #[inline]
    fn param_ty_lambda(&self, _result: &TermId) -> bool {
        true
    }
}

/// Print out all annotations for non-universe terms
#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash, PartialOrd, Ord)]

pub struct FullyAnnotated;

impl ToAstCtx for FullyAnnotated {
    fn annotate(&self, term: &TermId) -> Result<Option<Expr>, Error> {
        if let Some(ty) = term.ty() {
            ty.to_raw_ast_in(&mut FullyAnnotated).map(Some)
        } else {
            Ok(None)
        }
    }

    #[inline]
    fn var(&self, ix: u32, _annot: Option<&TermId>) -> Result<Expr, Error> {
        Ok(Expr::Var(ix))
    }

    #[inline]
    fn merge_app(&self) -> bool {
        true
    }

    #[inline]
    fn param_ty_lambda(&self, _result: &TermId) -> bool {
        true
    }
}
