use super::*;

/// A simple vector of substitutions
#[derive(Debug, Clone, PartialEq, Eq, Hash, Default)]
pub struct SubstVec<T = ()> {
    /// A vector of substitutions
    substs: Vec<TermOrShift>,
    /// The current number of substitutions
    no_subst: u32,
    /// The current lowest variable substituted
    base: u32,
    /// The typing context for this substitution vector
    ctx: T,
}

impl<T: TyCtxMut> SubstVec<T> {
    /// Create a new substitution vector with the given context
    #[inline]
    pub fn new(ctx: T) -> SubstVec<T> {
        SubstVec {
            substs: Vec::new(),
            no_subst: 0,
            base: 0,
            ctx,
        }
    }
}

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
enum TermOrShift {
    Term(TermId),
    Shift(u32),
}

impl<T: TyCtxMut> SubstCtx for SubstVec<T> {
    type Ctx = T::MaxDeref;

    fn subst_var(&mut self, ix: u32, annot: Option<&TermId>) -> Result<Option<TermId>, Error> {
        if let Some(subst_ix) = ix.checked_sub(self.base) {
            let subst = if (subst_ix as usize) < self.substs.len() {
                self.substs[self.substs.len() - 1 - subst_ix as usize].clone()
            } else {
                TermOrShift::Shift(0)
            };
            match subst {
                TermOrShift::Term(term) => {
                    //TODO: transport
                    Ok(Some(term))
                }
                TermOrShift::Shift(shift) => {
                    let ty = annot.subst_rec(self)?;
                    let new_ix = subst_ix + shift - self.no_subst;
                    if ty.is_none() && new_ix == subst_ix {
                        return Ok(None);
                    }
                    let ty = ty
                        .unwrap_or_else(|| annot.consed(self.ctx.cons_ctx()))
                        .map(|ty| ty.into_shallow_cons(self.ctx.cons_ctx()));
                    let term = Var::new_unchecked(new_ix, ty).into_id_with(self.ctx.cons_ctx());
                    Ok(Some(term))
                }
            }
        } else {
            Ok(None)
        }
    }

    #[inline]
    fn push_param(&mut self, param_ty: Option<&TermId>) -> Result<Option<TermId>, Error> {
        let subst = if let Some(param_ty) = param_ty {
            param_ty.subst_rec(self)?
        } else {
            None
        };
        self.ctx.push_param(subst.as_ref().or(param_ty))?;
        self.base += 1;
        Ok(subst)
    }

    #[inline]
    fn intersects(&self, filter: VarFilter, _code: Code, _form: Form) -> bool {
        filter.fvb() >= self.base
    }

    #[inline]
    fn ctx(&mut self) -> &mut Self::Ctx {
        self.ctx.ctx()
    }

    #[inline]
    fn pop_param(&mut self) -> Result<(), Error> {
        self.ctx.pop_param()?;
        match self.substs.last() {
            Some(TermOrShift::Shift(_)) => {
                //TODO: check shift is base...
                self.substs.pop();
            }
            _ if self.base == 0 => return Err(Error::ParameterUnderflow),
            _ => self.base -= 1,
        }
        Ok(())
    }

    #[inline]
    fn is_var_null(&self) -> bool {
        self.substs.is_empty()
    }
}

impl<T: TyCtxMut> EvalCtx for SubstVec<T> {
    fn push_subst(&mut self, subst: TermId) -> Result<(), Error> {
        self.substs.reserve(self.base as usize + 1);
        while self.base > 0 {
            self.substs.push(TermOrShift::Shift(self.no_subst));
            self.base -= 1;
        }
        self.substs.push(TermOrShift::Term(subst));
        self.no_subst += 1;
        Ok(())
    }

    fn pop_subst(&mut self) -> Result<(), Error> {
        if self.base != 0 {
            return Err(Error::ParameterUnderflow);
        }
        match self.substs.last() {
            Some(TermOrShift::Term(_)) => {
                self.substs.pop();
                self.no_subst -= 1;
            }
            _ => {
                return Err(Error::ParameterUnderflow);
            }
        }
        while let Some(TermOrShift::Shift(s)) = self.substs.last() {
            if *s == self.no_subst {
                self.substs.pop();
                self.base += 1;
            } else {
                break;
            }
        }
        Ok(())
    }
}
