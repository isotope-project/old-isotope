/*!
Contexts for creating, normalizing, and type-checking `isotope` terms
*/
use crate::*;
use hashers::null::PassThroughHasher;
use indexmap::IndexMap;

pub mod cons;
pub mod eq;
pub mod eval;
pub mod subst;
pub mod ty;

/// A standard typing context
pub type StandardCtx = MapTyCtx<DisjointSetCtx>;
