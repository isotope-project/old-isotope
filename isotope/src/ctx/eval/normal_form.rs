/*!
Reduction to normal form
*/
use super::*;

/// A reduction configuration to keep going until reaching a given form for up to `n` compound reductions.
#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash, PartialOrd, Ord)]
pub struct NormalCfg {
    /// The maximum number of steps which can be taken.
    pub max_steps: u64,
    /// Whether to perform eta reductions
    pub eta: bool,
    /// Whether to perform head beta reductions
    pub head: bool,
    /// Whether to perform non-head beta reductions
    pub sub: bool,
}

impl NormalCfg {
    /// Create a new normal configuration targeting a form with a given number of maximum steps
    pub fn to_form(form: Form, max_steps: u64) -> NormalCfg {
        NormalCfg {
            max_steps,
            eta: form.is_enf(),
            head: form.is_hnf(),
            sub: form.is_bnf() || form.is_enf(),
        }
    }
}

impl ReductionConfig for NormalCfg {
    type AsRef = NormalCfg;

    #[inline]
    fn register_push_subst(&mut self, _subst: &TermId) -> Result<(), Error> {
        Ok(())
    }

    #[inline]
    fn register_pop_subst(&mut self) -> Result<(), Error> {
        Ok(())
    }

    #[inline]
    fn register_beta(&mut self) -> Result<(), Error> {
        if self.max_steps == 0 {
            return Err(Error::StopReduction);
        }
        self.max_steps -= 1;
        Ok(())
    }

    #[inline]
    fn register_eta(&mut self) -> Result<(), Error> {
        if self.max_steps == 0 {
            return Err(Error::StopReduction);
        }
        self.max_steps -= 1;
        Ok(())
    }

    #[inline]
    fn eta(&self, _term: &Term, _ctx: &mut (impl TyCtxMut + ?Sized)) -> Result<bool, Error> {
        if self.max_steps == 0 {
            return Err(Error::StopReduction);
        } else {
            Ok(self.eta)
        }
    }

    #[inline]
    fn sub(&self, _term: &Term, _ctx: &mut (impl TyCtxMut + ?Sized)) -> Result<bool, Error> {
        if self.max_steps == 0 {
            return Err(Error::StopReduction);
        } else {
            Ok(self.sub)
        }
    }

    #[inline]
    fn head(&self, _term: &Term, _ctx: &mut (impl TyCtxMut + ?Sized)) -> Result<bool, Error> {
        if self.max_steps == 0 {
            return Err(Error::StopReduction);
        } else {
            Ok(self.head)
        }
    }

    #[inline]
    fn intersects(&self, _filter: VarFilter, _code: Code, form: Form) -> bool {
        (self.head && !form.is_hnf())
            || (self.sub && !((self.head && form.is_bnf()) && (self.eta && form.is_enf())))
            || (self.eta && !form.is_enf())
    }

    #[inline]
    fn as_ref_mut(&mut self) -> &mut Self::AsRef {
        self
    }
}
