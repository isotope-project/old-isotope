/*!
Contexts for type-checking terms
*/
use fxhash::FxHashMap;

use super::*;

mod map;
pub use map::*;

/// A context for mutably type-checking terms
pub trait TyCtxMut {
    /// Get this context's underlying consing context, if any
    type ConsCtx: ConsCtx + ?Sized;

    /// Get this context's underlying equality checking context, if any
    type TermEqCtx: TermEqCtxMut;

    /// Get this context's maximally dereferenced type, if any
    type MaxDeref: TyCtxMut;

    /// Attempt to infer the type of an index
    fn infer(&mut self, ix: u32) -> Option<TermId>;

    /// Constrain a variable to have a given annotation. Return whether this constraint typechecks
    fn constrain(&mut self, ix: u32, annot: &TermId) -> Result<Option<bool>, Error>;

    /// Check whether a variable is compatible with a given annotation.
    fn check(&mut self, ix: u32, annot: &TermId) -> Result<Option<bool>, Error>;

    /// Push a parameter onto this context with an optional annotation
    fn push_param(&mut self, param_ty: Option<&TermId>) -> Result<(), Error>;

    /// Pop a parameter from this context.
    fn pop_param(&mut self) -> Result<(), Error>;

    /// Get whether this context is universal for true/false *global* type-checking values given a filter
    fn global_tyck_mask(&self, filter: VarFilter) -> L4;

    /// Get whether this context is universal for true/false *variable* type-checking values given a filter
    fn var_tyck_mask(&self, filter: VarFilter) -> L4;

    /// Get whether this context is universal for true/false *path* type-checking values given a filter
    fn annot_tyck_mask(&self, filter: VarFilter) -> L4;

    /// Get whether this context assumes a value for type-checking given flags
    fn approx_tyck(&self, flags: TyckFlags, filter: VarFilter) -> Option<bool>;

    /// Get whether this context assumes a value for *globally* type-checking given flags
    fn approx_global_tyck(&self, flags: TyckFlags, filter: VarFilter) -> Option<bool>;

    /// Get whether this context assumes a value for *variable* type-checking given flags
    fn approx_var_tyck(&self, flags: TyckFlags, filter: VarFilter) -> Option<bool>;

    /// Get whether this context assumes a value for *path* type-checking given flags
    fn approx_annot_tyck(&self, flags: TyckFlags, filter: VarFilter) -> Option<bool>;

    /// Get the base index of this context
    fn ty_ctx_base(&self) -> u32;

    /// Reset this context's unbound variable constraints
    fn reset_unbound(&mut self) -> Result<(), Error>;

    /// Get this context's underlying consing context
    fn cons_ctx(&mut self) -> &mut Self::ConsCtx;

    /// Get this context's underlying equality checking context
    fn eq_ctx(&mut self) -> &mut Self::TermEqCtx;

    /// Get this context's underlying type
    fn ctx(&mut self) -> &mut Self::MaxDeref;
}

/// A simple typing context, which combines a `ConsCtx` and a `TermEqCtx`
#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash, PartialOrd, Ord, Default)]
pub struct BinaryCtx<C, T> {
    /// The underlying consing context
    pub cons: C,
    /// The underlying equality context
    pub eq: T,
}

/// A trivial typing context
pub type Trivial = BinaryCtx<(), Untyped>;

/// The trivial typing context
pub const TRIVIAL: Trivial = Trivial {
    cons: (),
    eq: Untyped,
};

/// Wrap a typing context into a trivial one
pub type TrivialCons<C> = BinaryCtx<C, Untyped>;

impl<C> TrivialCons<C>
where
    C: ConsCtx,
{
    /// Create a new trivial typing context with the given consing context
    #[inline]
    pub fn with_cons(cons: C) -> TrivialCons<C> {
        TrivialCons { cons, eq: Untyped }
    }
}

impl<C, T> TyCtxMut for BinaryCtx<C, T>
where
    C: ConsCtx,
    T: TermEqCtxMut,
{
    type ConsCtx = C;
    type TermEqCtx = T;
    type MaxDeref = Self;

    #[inline]
    fn infer(&mut self, _ix: u32) -> Option<TermId> {
        None
    }

    #[inline]
    fn constrain(&mut self, _ix: u32, _annot: &TermId) -> Result<Option<bool>, Error> {
        Ok(None)
    }

    #[inline]
    fn check(&mut self, _ix: u32, _annot: &TermId) -> Result<Option<bool>, Error> {
        Ok(None)
    }

    #[inline]
    fn push_param(&mut self, _param_ty: Option<&TermId>) -> Result<(), Error> {
        Ok(())
    }

    #[inline]
    fn pop_param(&mut self) -> Result<(), Error> {
        Ok(())
    }

    #[inline]
    fn global_tyck_mask(&self, _filter: VarFilter) -> L4 {
        self.eq.untyped_mask()
    }

    #[inline]
    fn var_tyck_mask(&self, _filter: VarFilter) -> L4 {
        self.eq.untyped_mask().intersection(L4::False)
    }

    #[inline]
    fn annot_tyck_mask(&self, _filter: VarFilter) -> L4 {
        self.eq.untyped_mask()
    }

    #[inline]
    fn approx_tyck(&self, flags: TyckFlags, _filter: VarFilter) -> Option<bool> {
        //TODO: think about this...
        self.eq
            .untyped_mask()
            .intersection_opt(flags.get_tyck().truth_value())
    }

    fn approx_global_tyck(&self, flags: TyckFlags, _filter: VarFilter) -> Option<bool> {
        //TODO: think about this...
        self.eq
            .untyped_mask()
            .intersection_opt(flags.get_flag(GlobalTyck).truth_value())
    }

    fn approx_var_tyck(&self, flags: TyckFlags, _filter: VarFilter) -> Option<bool> {
        //TODO: think about this...
        self.eq
            .untyped_mask()
            .intersection_opt(flags.get_flag(GlobalTyck).truth_value())
    }

    #[inline]
    fn approx_annot_tyck(&self, flags: TyckFlags, _filter: VarFilter) -> Option<bool> {
        //TODO: fix this
        flags.get_flag(AnnotTyck).truth_value()
    }

    #[inline]
    fn ty_ctx_base(&self) -> u32 {
        //TODO: think about this. Remove?
        0
    }

    #[inline]
    fn reset_unbound(&mut self) -> Result<(), Error> {
        Ok(())
    }

    #[inline]
    fn cons_ctx(&mut self) -> &mut Self::ConsCtx {
        &mut self.cons
    }

    #[inline]
    fn eq_ctx(&mut self) -> &mut Self::TermEqCtx {
        &mut self.eq
    }

    #[inline]
    fn ctx(&mut self) -> &mut Self::MaxDeref {
        self
    }
}

impl TyCtxMut for DisjointSetCtx {
    type ConsCtx = Self;
    type TermEqCtx = Self;
    type MaxDeref = Self;

    #[inline]
    fn infer(&mut self, _ix: u32) -> Option<TermId> {
        None
    }

    #[inline]
    fn constrain(&mut self, _ix: u32, _annot: &TermId) -> Result<Option<bool>, Error> {
        Ok(Some(true))
    }

    #[inline]
    fn check(&mut self, _ix: u32, _annot: &TermId) -> Result<Option<bool>, Error> {
        Ok(None)
    }

    #[inline]
    fn push_param(&mut self, _param_ty: Option<&TermId>) -> Result<(), Error> {
        Ok(())
    }

    #[inline]
    fn pop_param(&mut self) -> Result<(), Error> {
        Ok(())
    }

    #[inline]
    fn global_tyck_mask(&self, _filter: VarFilter) -> L4 {
        L4::True
    }

    #[inline]
    fn var_tyck_mask(&self, _filter: VarFilter) -> L4 {
        L4::Unknown
    }

    #[inline]
    fn annot_tyck_mask(&self, _filter: VarFilter) -> L4 {
        L4::True
    }

    #[inline]
    fn approx_tyck(&self, flags: TyckFlags, _filter: VarFilter) -> Option<bool> {
        flags.get_tyck().truth_value()
    }

    #[inline]
    fn approx_global_tyck(&self, flags: TyckFlags, _filter: VarFilter) -> Option<bool> {
        flags.get_flag(GlobalTyck).truth_value()
    }

    #[inline]
    fn approx_var_tyck(&self, flags: TyckFlags, _filter: VarFilter) -> Option<bool> {
        flags.get_flag(VarTyck).truth_value()
    }

    #[inline]
    fn approx_annot_tyck(&self, flags: TyckFlags, _filter: VarFilter) -> Option<bool> {
        flags.get_flag(AnnotTyck).truth_value()
    }

    #[inline]
    fn ty_ctx_base(&self) -> u32 {
        0
    }

    #[inline]
    fn reset_unbound(&mut self) -> Result<(), Error> {
        Ok(())
    }

    #[inline]
    fn cons_ctx(&mut self) -> &mut Self::ConsCtx {
        self
    }

    #[inline]
    fn eq_ctx(&mut self) -> &mut Self::TermEqCtx {
        self
    }

    #[inline]
    fn ctx(&mut self) -> &mut Self::MaxDeref {
        self
    }
}

impl<T: TyCtxMut + ?Sized> TyCtxMut for &mut T {
    type ConsCtx = T::ConsCtx;
    type TermEqCtx = T::TermEqCtx;
    type MaxDeref = T::MaxDeref;

    #[inline]
    fn infer(&mut self, ix: u32) -> Option<TermId> {
        (**self).infer(ix)
    }

    #[inline]
    fn constrain(&mut self, ix: u32, annot: &TermId) -> Result<Option<bool>, Error> {
        (**self).constrain(ix, annot)
    }

    #[inline]
    fn check(&mut self, ix: u32, annot: &TermId) -> Result<Option<bool>, Error> {
        (**self).check(ix, annot)
    }

    #[inline]
    fn push_param(&mut self, param_ty: Option<&TermId>) -> Result<(), Error> {
        (**self).push_param(param_ty)
    }

    #[inline]
    fn pop_param(&mut self) -> Result<(), Error> {
        (**self).pop_param()
    }

    #[inline]
    fn global_tyck_mask(&self, filter: VarFilter) -> L4 {
        (**self).global_tyck_mask(filter)
    }

    #[inline]
    fn var_tyck_mask(&self, filter: VarFilter) -> L4 {
        (**self).var_tyck_mask(filter)
    }

    #[inline]
    fn annot_tyck_mask(&self, filter: VarFilter) -> L4 {
        (**self).annot_tyck_mask(filter)
    }

    #[inline]
    fn approx_tyck(&self, flags: TyckFlags, filter: VarFilter) -> Option<bool> {
        (**self).approx_tyck(flags, filter)
    }

    #[inline]
    fn approx_global_tyck(&self, flags: TyckFlags, filter: VarFilter) -> Option<bool> {
        (**self).approx_global_tyck(flags, filter)
    }

    #[inline]
    fn approx_var_tyck(&self, flags: TyckFlags, filter: VarFilter) -> Option<bool> {
        (**self).approx_var_tyck(flags, filter)
    }

    #[inline]
    fn approx_annot_tyck(&self, flags: TyckFlags, filter: VarFilter) -> Option<bool> {
        (**self).approx_annot_tyck(flags, filter)
    }

    #[inline]
    fn ty_ctx_base(&self) -> u32 {
        (**self).ty_ctx_base()
    }

    #[inline]
    fn reset_unbound(&mut self) -> Result<(), Error> {
        (**self).reset_unbound()
    }

    #[inline]
    fn cons_ctx(&mut self) -> &mut Self::ConsCtx {
        (**self).cons_ctx()
    }

    #[inline]
    fn eq_ctx(&mut self) -> &mut Self::TermEqCtx {
        (**self).eq_ctx()
    }

    #[inline]
    fn ctx(&mut self) -> &mut Self::MaxDeref {
        (**self).ctx()
    }
}
