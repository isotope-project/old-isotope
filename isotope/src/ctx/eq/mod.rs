/*!
Contexts for equality-checking terms
*/

use super::*;

mod dsf;
pub use dsf::*;

/// A type which may be used as a *mutable* context for checking `isotope` terms for equality
pub trait TermEqCtxMut: Debug {
    /// Check two terms are equivalent, potentially optimizing the underlying data structure
    /// Consing is optional
    fn term_eq_mut(&mut self, left: &Term, right: &Term) -> Option<bool>;

    /// Check two terms are equivalent, potentially optimizing the underlying data structure
    /// If this data structure conses, then this will shallow-cons both terms
    fn shallow_cons_term_eq(&mut self, left: &TermId, right: &TermId) -> Option<bool> {
        self.term_eq_mut(left, right)
    }

    /// Check two terms are equivalent, potentially optimizing the underlying data structure
    /// If this data structure conses, then this will deep-cons both terms
    fn deep_cons_term_eq(&mut self, left: &TermId, right: &TermId) -> Option<bool> {
        self.term_eq_mut(left, right)
    }

    /// Check whether this term is equivalent to a type in this context, potentially optimizing the underlying data structure
    fn is_ty_mut(&mut self, ty: &Term) -> Option<bool>;

    /// Check whether this term is equivalent to a type in this context, potentially optimizing the underlying data structure
    /// If this data structure conses, then this will shallow-cons both terms
    fn shallow_cons_is_ty(&mut self, ty: &TermId) -> Option<bool> {
        self.is_ty_mut(ty)
    }

    /// Check whether this term is equivalent to a type in this context, potentially optimizing the underlying data structure
    /// If this data structure conses, then this will deep-cons both terms
    fn deep_cons_is_ty(&mut self, ty: &TermId) -> Option<bool> {
        self.is_ty_mut(ty)
    }

    /// Get a term's root form in this context, potentially optimizing the underlying data structure
    fn root_mut(&mut self, ty: &Term) -> Option<TermId>;

    /// Get a term's universe in this context, potentially optimizing the underlying data structure
    /// If this data structure conses, then this will shallow-cons both terms
    fn shallow_cons_root(&mut self, ty: &TermId) -> Option<TermId> {
        self.root_mut(ty)
    }

    /// Get a term's root form in this context, potentially optimizing the underlying data structure
    /// If this data structure conses, then this will deep-cons both terms
    fn deep_cons_root(&mut self, ty: &TermId) -> Option<TermId> {
        self.root_mut(ty)
    }

    /// Get this term as a dynamic typing context
    fn as_dyn_eq_mut(&mut self) -> &mut dyn TermEqCtxMut;

    /// Quickly approximate equality between two terms: return `None` if unknown
    fn approx_term_eq_mut(&mut self, left: &Term, right: &Term) -> Option<Option<bool>> {
        self.term_eq_mut(left, right).map(Some)
    }

    /// Check whether it is possible for two terms to be equal by code/address alone
    fn approx_term_eq_code(
        &self,
        _left: (Code, Form),
        _right: (Code, Form),
    ) -> Option<Option<bool>> {
        None
    }

    /// Cache two equal terms.
    ///
    /// Unlike `TermEqEdit`'s `make_term_eq`, this is *not* guaranteed to have an effect.
    #[inline]
    fn cache_eq(&mut self, _left: &TermId, _right: &TermId) {}

    /// Cache two equal shallow-consed terms.
    ///
    /// Unlike `TermEqEdit`'s `shallow_cons_make_term_eq`, this is *not* guaranteed to have an effect.
    #[inline]
    fn shallow_cons_cache_eq(&mut self, left: &TermId, right: &TermId) {
        self.cache_eq(left, right)
    }

    /// Whether to compare general annotations
    fn cmp_annot(&self) -> bool;

    /// Whether to compare parameter annotations
    fn cmp_param(&self) -> bool;

    /// Whether to compare variable annotations
    fn cmp_var(&self) -> bool;

    /// Whether comparison is structural
    fn is_struct(&self) -> bool;

    /// Get the untyped equality mask for this context
    fn typed_mask(&self) -> L4;

    /// Get the untyped equality mask for this context
    fn untyped_mask(&self) -> L4;
}

impl<T: TermEqCtxMut + ?Sized> TermEqCtxMut for &mut T {
    #[inline]
    fn term_eq_mut(&mut self, left: &Term, right: &Term) -> Option<bool> {
        (**self).term_eq_mut(left, right)
    }

    #[inline]
    fn shallow_cons_term_eq(&mut self, left: &TermId, right: &TermId) -> Option<bool> {
        (**self).shallow_cons_term_eq(left, right)
    }

    #[inline]
    fn deep_cons_term_eq(&mut self, left: &TermId, right: &TermId) -> Option<bool> {
        (**self).deep_cons_term_eq(left, right)
    }

    #[inline]
    fn is_ty_mut(&mut self, ty: &Term) -> Option<bool> {
        (**self).is_ty_mut(ty)
    }

    #[inline]
    fn shallow_cons_is_ty(&mut self, ty: &TermId) -> Option<bool> {
        (**self).shallow_cons_is_ty(ty)
    }

    #[inline]
    fn deep_cons_is_ty(&mut self, ty: &TermId) -> Option<bool> {
        (**self).deep_cons_is_ty(ty)
    }

    #[inline]
    fn root_mut(&mut self, ty: &Term) -> Option<TermId> {
        (**self).root_mut(ty)
    }

    #[inline]
    fn shallow_cons_root(&mut self, ty: &TermId) -> Option<TermId> {
        (**self).shallow_cons_root(ty)
    }

    #[inline]
    fn deep_cons_root(&mut self, ty: &TermId) -> Option<TermId> {
        (**self).deep_cons_root(ty)
    }

    #[inline]
    fn as_dyn_eq_mut(&mut self) -> &mut dyn TermEqCtxMut {
        self
    }

    #[inline]
    fn approx_term_eq_mut(&mut self, left: &Term, right: &Term) -> Option<Option<bool>> {
        (**self).approx_term_eq_mut(left, right)
    }

    #[inline]
    fn approx_term_eq_code(&self, left: (Code, Form), right: (Code, Form)) -> Option<Option<bool>> {
        (**self).approx_term_eq_code(left, right)
    }

    #[inline]
    fn cache_eq(&mut self, left: &TermId, right: &TermId) {
        (**self).cache_eq(left, right)
    }

    #[inline]
    fn shallow_cons_cache_eq(&mut self, left: &TermId, right: &TermId) {
        (**self).shallow_cons_cache_eq(left, right)
    }

    #[inline]
    fn cmp_annot(&self) -> bool {
        (**self).cmp_annot()
    }

    #[inline]
    fn cmp_param(&self) -> bool {
        (**self).cmp_param()
    }

    #[inline]
    fn cmp_var(&self) -> bool {
        (**self).cmp_var()
    }

    #[inline]
    fn typed_mask(&self) -> L4 {
        (**self).typed_mask()
    }

    #[inline]
    fn untyped_mask(&self) -> L4 {
        (**self).untyped_mask()
    }

    #[inline]
    fn is_struct(&self) -> bool {
        (**self).is_struct()
    }
}

/// A type which may be used as a context for checking `isotope` terms for equality
pub trait TermEqCtx: TermEqCtxMut {
    /// Check two terms are equivalent within this structure
    fn term_eq(&self, left: &Term, right: &Term) -> Option<bool>;
    /// Check whether this term is equivalent to a type in this context
    fn is_ty(&self, ty: &Term) -> Option<bool>;
    /// Get a term's universe in this context
    fn universe(&self, ty: &Term) -> Option<Universe>;
}

/// A type which may be used as an *editable* context for checking `isotope` terms for equality
pub trait TermEqCtxEdit: TermEqCtxMut {
    /// Make two terms equivalent within this structure, returning whether they were already equivalent
    /// If this data structure conses, then this will *deep* cons both terms!
    fn make_term_eq(&mut self, left: &TermId, right: &TermId) -> bool;
    /// Make two terms equivalent within this structure, returning whether they were already equivalent
    /// If this data structure conses, then this will shallow-cons both terms
    fn shallow_cons_make_term_eq(&mut self, left: &TermId, right: &TermId) -> bool {
        self.make_term_eq(left, right)
    }
}

/// Untyped value comparison
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash, PartialOrd, Ord, Default)]
pub struct Untyped;

impl TermEqCtxMut for Untyped {
    #[inline]
    fn term_eq_mut(&mut self, left: &Term, right: &Term) -> Option<bool> {
        if left as *const _ == right as *const _ {
            Some(true)
        } else {
            self.approx_term_eq_code((left.code(), left.form()), (right.code(), right.form()))
                .flatten()
        }
    }

    #[inline]
    fn is_ty_mut(&mut self, ty: &Term) -> Option<bool> {
        ty.is_local_ty()
    }

    #[inline]
    fn root_mut(&mut self, _ty: &Term) -> Option<TermId> {
        None
    }

    #[inline]
    fn as_dyn_eq_mut(&mut self) -> &mut dyn TermEqCtxMut {
        self
    }

    #[inline]
    fn approx_term_eq_code(&self, left: (Code, Form), right: (Code, Form)) -> Option<Option<bool>> {
        if left.1 == Form::BetaEta && right.1 == Form::BetaEta && left.0.pure() != right.0.pure() {
            Some(Some(false))
        } else {
            None
        }
    }

    #[inline]
    fn cmp_annot(&self) -> bool {
        false
    }

    #[inline]
    fn cmp_param(&self) -> bool {
        false
    }

    #[inline]
    fn cmp_var(&self) -> bool {
        false
    }

    #[inline]
    fn typed_mask(&self) -> L4 {
        L4::False
    }

    #[inline]
    fn untyped_mask(&self) -> L4 {
        L4::Both
    }

    #[inline]
    fn is_struct(&self) -> bool {
        false
    }
}

impl TermEqCtx for Untyped {
    #[inline]
    fn term_eq(&self, left: &Term, right: &Term) -> Option<bool> {
        if left as *const _ == right as *const _ {
            Some(true)
        } else {
            self.approx_term_eq_code((left.code(), left.form()), (right.code(), right.form()))
                .flatten()
        }
    }

    #[inline]
    fn is_ty(&self, ty: &Term) -> Option<bool> {
        ty.is_local_ty()
    }

    #[inline]
    fn universe(&self, ty: &Term) -> Option<Universe> {
        ty.universe()
    }
}

/// Typed value comparison
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash, PartialOrd, Ord, Default)]
pub struct Typed;

impl TermEqCtxMut for Typed {
    #[inline]
    fn term_eq_mut(&mut self, left: &Term, right: &Term) -> Option<bool> {
        if left as *const _ == right as *const _ {
            Some(true)
        } else {
            self.approx_term_eq_code((left.code(), left.form()), (right.code(), right.form()))
                .flatten()
        }
    }

    #[inline]
    fn is_ty_mut(&mut self, ty: &Term) -> Option<bool> {
        ty.is_local_ty()
    }

    #[inline]
    fn root_mut(&mut self, _ty: &Term) -> Option<TermId> {
        None
    }

    #[inline]
    fn as_dyn_eq_mut(&mut self) -> &mut dyn TermEqCtxMut {
        self
    }

    #[inline]
    fn approx_term_eq_code(&self, left: (Code, Form), right: (Code, Form)) -> Option<Option<bool>> {
        if left.1 == Form::BetaEta && right.1 == Form::BetaEta && left.0 != right.0 {
            Some(Some(false))
        } else {
            None
        }
    }

    #[inline]
    fn cmp_annot(&self) -> bool {
        false
    }

    #[inline]
    fn cmp_param(&self) -> bool {
        true
    }

    #[inline]
    fn cmp_var(&self) -> bool {
        true
    }

    #[inline]
    fn typed_mask(&self) -> L4 {
        L4::Both
    }

    #[inline]
    fn untyped_mask(&self) -> L4 {
        L4::True
    }

    #[inline]
    fn is_struct(&self) -> bool {
        false
    }
}

impl TermEqCtx for Typed {
    #[inline]
    fn term_eq(&self, left: &Term, right: &Term) -> Option<bool> {
        if left as *const _ == right as *const _ {
            Some(true)
        } else {
            self.approx_term_eq_code((left.code(), left.form()), (right.code(), right.form()))
                .flatten()
        }
    }

    #[inline]
    fn is_ty(&self, ty: &Term) -> Option<bool> {
        ty.is_local_ty()
    }

    #[inline]
    fn universe(&self, ty: &Term) -> Option<Universe> {
        ty.universe()
    }
}

/// Structural comparison
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash, PartialOrd, Ord, Default)]
pub struct Structural;

impl TermEqCtxMut for Structural {
    #[inline]
    fn term_eq_mut(&mut self, left: &Term, right: &Term) -> Option<bool> {
        if left as *const _ == right as *const _ {
            Some(true)
        } else if left.code() != right.code() {
            Some(false)
        } else {
            None
        }
    }

    #[inline]
    fn is_ty_mut(&mut self, ty: &Term) -> Option<bool> {
        ty.is_local_ty()
    }

    #[inline]
    fn root_mut(&mut self, _ty: &Term) -> Option<TermId> {
        None
    }

    #[inline]
    fn as_dyn_eq_mut(&mut self) -> &mut dyn TermEqCtxMut {
        self
    }

    #[inline]
    fn approx_term_eq_code(&self, left: (Code, Form), right: (Code, Form)) -> Option<Option<bool>> {
        if left != right {
            Some(Some(false))
        } else {
            None
        }
    }

    #[inline]
    fn cmp_annot(&self) -> bool {
        true
    }

    #[inline]
    fn cmp_param(&self) -> bool {
        true
    }

    #[inline]
    fn cmp_var(&self) -> bool {
        true
    }

    #[inline]
    fn typed_mask(&self) -> L4 {
        L4::True
    }

    #[inline]
    fn untyped_mask(&self) -> L4 {
        L4::True
    }

    #[inline]
    fn is_struct(&self) -> bool {
        true
    }
}
