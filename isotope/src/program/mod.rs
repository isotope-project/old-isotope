/*!
`isotope` program graphs
*/

/// An index for a node in an `isotope` program graph
pub type NodeIx = std::num::NonZeroU64;
