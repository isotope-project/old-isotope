/*!
Datatypes for `isotope` values
*/

/// A datatype for `isotope` values
#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub enum Type {}
