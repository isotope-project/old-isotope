/*!
Utilities
*/
use super::*;
use num::traits::{CheckedShl, One, WrappingSub, Zero};

mod l4;
mod str_set;
pub use l4::*;
pub use str_set::*;

/// Borrow an optional value of a given type
pub trait BorrowOpt<T> {
    /// Borrow an optional value of type `T`
    fn borrow_opt(&self) -> Option<&T>;
}

impl<T> BorrowOpt<T> for T {
    #[inline]
    fn borrow_opt(&self) -> Option<&T> {
        Some(self)
    }
}

impl<T> BorrowOpt<T> for Option<T> {
    #[inline]
    fn borrow_opt(&self) -> Option<&T> {
        self.as_ref()
    }
}

/// Get an integer composed of `min(BITS, n)` ones followed by `BITS - min(BITS, n)` zeros, where `N` is an integer type with `BITS` bits.
#[inline]
pub fn n_ones<N>(n: u32) -> N
where
    N: Zero + One + CheckedShl + WrappingSub,
{
    N::one()
        .checked_shl(n)
        .unwrap_or(N::zero())
        .wrapping_sub(&N::one())
}

/// Get an integer composed of `min(BITS, n)` ones followed by `BITS - min(BITS, n)` zeros, where `N` is an integer type with `BITS` bits.
#[inline]
pub const fn n_ones_const_u32(n: u32) -> u32 {
    if let Some(result) = 1u32.checked_shl(n) {
        result - 1
    } else {
        u32::MAX
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use L4::*;

    #[test]
    fn basic_four_valued_logic_properties() {
        assert_eq!(L4::default(), Unknown);
        let truth_values = [None, Some(true), Some(false), None];
        assert_eq!(False as usize, 0b10);
        assert_eq!(True as usize, 0b01);
        assert_eq!(Unknown as usize, 0b00);
        assert_eq!(Both as usize, 0b11);
        assert_eq!(L4::from(0b10u8), False);
        assert_eq!(L4::from(0b01u8), True);
        assert_eq!(L4::from(0b00u8), Unknown);
        assert_eq!(L4::from(0b11u8), Both);
        for i in 0b00..=0b11 {
            let l = L4::from(i as u8);
            assert_eq!(l.truth_value(), truth_values[i]);
            assert_eq!(l.maybe_true(), truth_values[i].unwrap_or(true) == true);
            assert_eq!(l.maybe_false(), truth_values[i].unwrap_or(false) == false);
            assert_eq!(l.contains(false), l.maybe_false() && l != Unknown);
            assert_eq!(l.contains(true), l.maybe_true() && l != Unknown);
            assert_eq!(l.contains(false) || l.contains(true), l != Unknown);
            assert_eq!(l.contains(false) && l.contains(true), l == Both);
        }
    }

    #[test]
    fn four_valued_logic_and() {
        let truth_table = [
            (False, False, False),
            (False, True, False),
            (False, Unknown, False),
            (False, Both, False),
            (True, True, True),
            (True, Unknown, Unknown),
            (True, Both, Both),
            (Unknown, Unknown, Unknown),
            (Unknown, Both, False),
            (Both, Both, Both),
        ];
        for &(left, right, result) in &truth_table {
            assert_eq!(left & right, result);
            assert_eq!(right & left, result);
        }

        for flag in TyckFlag::into_enum_iter() {
            for left in L4::into_enum_iter() {
                for right in L4::into_enum_iter() {
                    assert_eq!(
                        TyckFlags::default()
                            .with_flag(flag, left)
                            .conjunction(TyckFlags::default().with_flag(flag, right)),
                        TyckFlags::default().with_flag(flag, left & right)
                    )
                }
            }
        }
    }

    #[test]
    fn four_valued_logic_or() {
        let truth_table = [
            (False, False, False),
            (False, True, True),
            (False, Unknown, Unknown),
            (False, Both, Both),
            (True, True, True),
            (True, Unknown, True),
            (True, Both, True),
            (Unknown, Unknown, Unknown),
            (Unknown, Both, True),
            (Both, Both, Both),
        ];
        for &(left, right, result) in &truth_table {
            assert_eq!(left | right, result);
            assert_eq!(right | left, result);
        }

        for flag in TyckFlag::into_enum_iter() {
            for left in L4::into_enum_iter() {
                for right in L4::into_enum_iter() {
                    assert_eq!(
                        TyckFlags::default()
                            .with_flag(flag, left)
                            .disjunction(TyckFlags::default().with_flag(flag, right)),
                        TyckFlags::default().with_flag(flag, left | right)
                    )
                }
            }
        }
    }

    #[test]
    fn n_ones_u32() {
        for i in 0..=256 {
            assert_eq!(n_ones::<u32>(i), n_ones_const_u32(i));
            assert_eq!(n_ones::<u32>(i).leading_zeros(), 32u32.saturating_sub(i));
            if i >= 32 {
                assert_eq!(n_ones::<u32>(i), u32::MAX)
            }
        }
    }

    #[test]
    fn n_ones_u64() {
        for i in 0..=256 {
            assert_eq!(n_ones::<u64>(i) as u32, n_ones_const_u32(i));
            assert_eq!(n_ones::<u64>(i).leading_zeros(), 64u32.saturating_sub(i));
            if i >= 64 {
                assert_eq!(n_ones::<u32>(i), u32::MAX)
            }
        }
    }
}
