/*!
IEEE 1364 standard 4-valued logic
*/
use super::*;

/// All even bits in a u8
pub const EVEN_MASK_U8: u8 = 0b01010101;

/// All odd bits in a u8
pub const ODD_MASK_U8: u8 = EVEN_MASK_U8 << 1;

/// IEEE 1364 standard 4-valued logic
#[derive(Copy, Clone, Eq, PartialEq, Hash, IntoEnumIterator)]
pub enum L4 {
    /// The value is known to be false
    False = 0b10,
    /// The value is known to be true
    True = 0b01,
    /// The value is unknown
    Unknown = 0b00,
    /// The value may be either true or false
    Both = 0b11,
}

impl L4 {
    /// Create a new value which has `f` for a maybe-false bit
    #[inline]
    pub const fn with_false(f: bool) -> L4 {
        if f {
            L4::False
        } else {
            L4::Unknown
        }
    }
    /// Create a new value which has `t` for a maybe-true bit
    #[inline]
    pub const fn with_true(t: bool) -> L4 {
        if t {
            L4::True
        } else {
            L4::Unknown
        }
    }
    /// Create an `L4` from a `u8` encoding
    #[inline]
    pub const fn from_u8(x: u8) -> L4 {
        use L4::*;
        match x {
            0b10 => False,
            0b01 => True,
            0b00 => Unknown,
            _ => Both,
        }
    }
    /// Create an `L4` from a boolean
    #[inline]
    pub const fn from_bool(x: bool) -> L4 {
        if x {
            L4::True
        } else {
            L4::False
        }
    }
    /// Create an `L4` from an optional boolean
    #[inline]
    pub const fn from_opt(x: Option<bool>) -> L4 {
        match x {
            None => L4::Unknown,
            Some(true) => L4::True,
            Some(false) => L4::False,
        }
    }
    /// Take the union of two L4 values, taken as subsets of {0, 1}
    #[inline]
    pub const fn union(self, other: L4) -> L4 {
        L4::from_u8(self as u8 | other as u8)
    }
    /// Take the intersection of two L4 values, taken as subsets of {0, 1}
    #[inline]
    pub const fn intersection(self, other: L4) -> L4 {
        L4::from_u8(self as u8 & other as u8)
    }
    /// Take the union of two L4 values, taken as subsets of {0, 1}
    #[inline]
    pub const fn union_bool(self, other: bool) -> Option<bool> {
        L4::from_u8(self as u8 | L4::from_bool(other) as u8).truth_value()
    }
    /// Take the intersection of two L4 values, taken as subsets of {0, 1}
    #[inline]
    pub const fn intersection_bool(self, other: bool) -> Option<bool> {
        L4::from_u8(self as u8 & L4::from_bool(other) as u8).truth_value()
    }
    /// Take the union of two L4 values, taken as subsets of {0, 1}
    #[inline]
    pub const fn union_opt(self, other: Option<bool>) -> Option<bool> {
        L4::from_u8(self as u8 | L4::from_opt(other) as u8).truth_value()
    }
    /// Take the intersection of two L4 values, taken as subsets of {0, 1}
    #[inline]
    pub const fn intersection_opt(self, other: Option<bool>) -> Option<bool> {
        L4::from_u8(self as u8 & L4::from_opt(other) as u8).truth_value()
    }
    /// Whether this value *might* be true
    #[inline]
    pub const fn maybe_true(self) -> bool {
        match self {
            L4::False => false,
            _ => true,
        }
    }
    /// Whether this value *might* be false
    #[inline]
    pub const fn maybe_false(self) -> bool {
        match self {
            L4::True => false,
            _ => true,
        }
    }
    /// Whether this value contains a given boolean value
    #[inline]
    pub const fn contains(self, b: bool) -> bool {
        self as u8 & (1 << !b as u8) != 0
    }
    /// The known truth value, if any
    #[inline]
    pub const fn truth_value(self) -> Option<bool> {
        use L4::*;
        match self {
            False => Some(false),
            True => Some(true),
            _ => None,
        }
    }
    /// Get a static reference to this value
    #[inline]
    pub const fn as_static(self) -> &'static L4 {
        use L4::*;
        match self {
            False => &False,
            True => &True,
            Unknown => &Unknown,
            Both => &Both,
        }
    }
    /// Take the conjunction of two values
    #[inline]
    pub const fn conjunction(self, other: L4) -> L4 {
        L4::from_u8(self as u8 & ODD_MASK_U8 | other as u8 & ODD_MASK_U8 | self as u8 & other as u8)
    }
    /// Take the disjunction of two values
    #[inline]
    pub const fn disjunction(self, other: L4) -> L4 {
        L4::from_u8(
            self as u8 & EVEN_MASK_U8 | other as u8 & EVEN_MASK_U8 | self as u8 & other as u8,
        )
    }
}

impl BitAnd for L4 {
    type Output = L4;

    #[inline]
    fn bitand(self, rhs: Self) -> Self::Output {
        self.conjunction(rhs)
    }
}

impl BitOr for L4 {
    type Output = L4;

    #[inline]
    fn bitor(self, rhs: Self) -> Self::Output {
        self.disjunction(rhs)
    }
}

impl Not for L4 {
    type Output = L4;

    fn not(self) -> Self::Output {
        L4::from(((self as u8 & EVEN_MASK_U8) << 1) | ((self as u8 & ODD_MASK_U8) >> 1))
    }
}

impl From<u8> for L4 {
    #[inline]
    fn from(x: u8) -> Self {
        L4::from_u8(x)
    }
}

impl From<bool> for L4 {
    #[inline]
    fn from(x: bool) -> Self {
        L4::from_bool(x)
    }
}

impl From<Option<bool>> for L4 {
    #[inline]
    fn from(x: Option<bool>) -> Self {
        L4::from_opt(x)
    }
}

impl Default for L4 {
    #[inline]
    fn default() -> Self {
        L4::Unknown
    }
}

impl Debug for L4 {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        let s = match self {
            L4::False => "0",
            L4::True => "1",
            L4::Unknown => "Z",
            L4::Both => "X",
        };
        Display::fmt(s, f)
    }
}