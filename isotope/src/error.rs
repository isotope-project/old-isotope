/*!
Error handling utilities
*/
use super::*;
use thiserror::Error;

/// An error which may occur manipulating `isotope` terms
#[derive(Debug, Copy, Clone, PartialEq, Eq, Error)]
pub enum Error {
    /// Attempted to substitute away a dependency
    #[error("Attempted to substitute away a dependency on a variable")]
    HasDependency,
    /// Expected an annotation
    #[error("Expected an annotation")]
    ExpectedAnnot,
    /// Inconsistent type-check flag
    #[error("An inconsistent type-check flag")]
    InconsistentTyck,
    /// A variable annotation mismatch
    #[error("An annotation mismatch on variable")]
    VarAnnotMismatch,
    /// A general annotation mismatch
    #[error("General annotation mismatch")]
    AnnotMismatch,
    /// Expected a term
    #[error("Expected a term")]
    ExpectedTerm,
    /// Expected a dependent function type
    #[error("Expected a dependent function type")]
    ExpectedPi,
    /// Expected a type
    #[error("Expected a type")]
    ExpectedType,
    /// Expected a universe
    #[error("Expected a universe")]
    ExpectedUniverse,
    /// Expected a function
    #[error("Expected a function")]
    ExpectedFunction,
    /// Expected a reflexivity instance
    #[error("Expected a reflexivity instance")]
    ExpectedRefl,
    /// Function took too many steps to evaluate
    #[error("Function took too many steps to evaluate")]
    OutOfGas,
    /// Stop the current reduction
    #[error("Stop the current reduction")]
    StopReduction,
    /// A type annotation is required
    #[error("A type annotation is required")]
    AnnotationRequired,
    /// Parameter underflow
    #[error("Parameter underflow")]
    ParameterUnderflow,
    /// Parameter overflow
    #[error("Parameter overflow")]
    ParameterOverflow,
    /// A type mismatch
    #[error("Type mismatch")]
    TypeMismatch,
    /// Failed to unify types
    #[error("Type unification failed")]
    TypeUnificationFailure,
    /// A subtype mismatch
    #[error("Subtype mismatch")]
    SubtypeMismatch,
    /// A term mismatch
    #[error("Term mismatch")]
    TermMismatch,
    /// Failed to unify terms
    #[error("Term unification failed")]
    TermUnificationFailure,
    /// An untyped parameter
    #[error("Untyped parameter for variable")]
    UntypedParameter,
    /// An untyped constraint
    #[error("Untyped parameter for variable")]
    UntypedConstraint,
    /// Cannot infer a value
    #[error("Cannot infer a value")]
    CannotInfer,
    /// An undefined symbol
    #[error("An undefined symbol")]
    UndefinedSymbol,
    /// Unimplemented functionality
    #[error("Not implemented")]
    NotImplemented,
    /// Universe out of bounds
    #[error("Universe out of bounds")]
    UniverseOutOfBounds,
    /// Attempted to coerce an untyped term
    #[error("Attempted to coerce an untyped term")]
    CoerceUntyped,
    /// A potentially recoverable inference failure.
    #[error("Potentially recoverable inference failure")]
    InferenceFailure,
    /// A type cannot be matched
    #[error("Cannot match type")]
    CannotMatch,
    /// An index out of bounds
    #[error("Index out of bounds")]
    IndexOutOfBounds,
}
