/*!
Weak references to `isotope` terms
*/
use super::*;

/// A weak reference to a term in `isotope`'s term language
#[derive(Debug, Clone, Default)]
pub struct WeakId(Weak<Term>);

impl TermId {
    /// Downgrade this `TermId` to a `WeakId`
    #[inline]
    pub fn downgrade(&self) -> WeakId {
        WeakId(Arc::downgrade(&self.0))
    }
}

impl WeakId {
    /// Check whether two `WeakId`s point to the same data
    #[inline]
    pub fn ptr_eq(&self, other: &WeakId) -> bool {
        self.0.ptr_eq(&other.0)
    }
    /// Get a pointer to the data underlying this `WeakId`
    #[inline]
    pub fn as_ptr(&self) -> *const Term {
        self.0.as_ptr()
    }
    /// Attempt to upgrade this `WeakId` into a `TermId`
    #[inline]
    pub fn upgrade(&self) -> Option<TermId> {
        self.0.upgrade().map(TermId)
    }
}

impl PartialEq for WeakId {
    #[inline]
    fn eq(&self, other: &WeakId) -> bool {
        self.0.ptr_eq(&other.0)
    }
}

impl PartialEq<TermId> for WeakId {
    #[inline]
    fn eq(&self, other: &TermId) -> bool {
        self.0.as_ptr() == other.as_ptr()
    }
}

impl PartialEq<WeakId> for TermId {
    #[inline]
    fn eq(&self, other: &WeakId) -> bool {
        self.as_ptr() == other.0.as_ptr()
    }
}

impl Eq for WeakId {}

impl Hash for WeakId {
    #[inline]
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.0.as_ptr().hash(state)
    }
}