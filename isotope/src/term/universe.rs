/*!
Typing universes
*/
use super::*;

/// A term containing a typing universe
#[derive(Debug, Clone)]
pub struct UniverseTerm {
    /// The universe underlying this term
    universe: Universe,
    /// The annotation of this term, if any
    annot: Option<Annotation>,
    /// The code of this term
    code: Code,
    /// The variable filter of this term
    filter: VarFilter,
    /// The pre-computed flags of this term
    flags: AtomicTyckFlags,
}

impl From<Universe> for UniverseTerm {
    #[inline]
    fn from(universe: Universe) -> Self {
        Self::new_unchecked(universe, None)
    }
}

impl UniverseTerm {
    /// Create a new universe term without any type-checking
    #[inline]
    pub fn new_unchecked(universe: Universe, annot: Option<Annotation>) -> UniverseTerm {
        let code = Self::compute_code(&universe, annot.as_ref());
        let filter = Self::compute_filter(&universe, annot.as_ref());
        let flags = Self::compute_flags(&universe, annot.as_ref()).into();
        UniverseTerm {
            universe,
            annot,
            code,
            filter,
            flags,
        }
    }

    /// Compute the code for a universe term with a given optional annotation
    #[inline]
    pub fn compute_code(universe: &Universe, annot: Option<&Annotation>) -> Code {
        let universe_ = 0x756e697665727365; // "universe" in ASCII
        let mut hasher = AHasher::new_with_keys(universe_, 0x0);
        universe.hash(&mut hasher);
        let pre = hasher.finish();
        if let Some(ty) = annot {
            ty.hash(&mut hasher)
        };
        let post = hasher.finish();
        Code::new(pre, post)
    }

    /// Compute the filter for a universe term with a given optional annotation
    #[inline]
    pub fn compute_filter(_universe: &Universe, annot: Option<&Annotation>) -> VarFilter {
        annot.get_filter()
    }

    /// Compute the flags for a universe term with a given optional annotation
    #[inline]
    pub fn compute_flags(_universe: &Universe, annot: Option<&Annotation>) -> TyckFlags {
        TyckFlags::default()
            .with_flag(AnnotTyck, L4::with_true(annot.is_none()))
            .with_flag(LocalTyck, L4::with_true(annot.is_none()))
            .with_flag(GlobalTyck, L4::with_true(annot.is_none()))
    }

    /// Get this term as a universe
    #[inline]
    pub fn as_universe(&self) -> &Universe {
        &self.universe
    }
}

impl Cons for UniverseTerm {
    type Consed = UniverseTerm;

    fn cons(&self, ctx: &mut (impl ConsCtx + ?Sized)) -> Option<Self> {
        if let Some(consed_ty) = self.annot.cons(ctx).flatten() {
            let code = Self::compute_code(&self.universe, Some(&consed_ty));
            Some(UniverseTerm {
                universe: self.universe.clone(),
                annot: Some(consed_ty),
                code,
                filter: self.filter,
                flags: self.flags.clone(),
            })
        } else {
            None
        }
    }

    #[inline]
    fn to_consed_ty(&self) -> Self::Consed {
        self.clone()
    }
}

impl Substitute for UniverseTerm {
    type Substituted = UniverseTerm;

    fn subst_rec(&self, ctx: &mut (impl SubstCtx + ?Sized)) -> Result<Option<UniverseTerm>, Error> {
        if !ctx.intersects(self.filter, self.code, Form::BetaEta) {
            return Ok(None);
        }
        let ty = self.annot.subst_rec(ctx)?;
        if let Some(ty) = ty.flatten() {
            let code = UniverseTerm::compute_code(&self.universe, Some(&ty));
            let filter = UniverseTerm::compute_filter(&self.universe, Some(&ty));
            let flags = UniverseTerm::compute_flags(&self.universe, Some(&ty)).into();
            let term = UniverseTerm {
                annot: Some(ty),
                universe: self.universe.clone(),
                code,
                filter,
                flags,
            };
            Ok(Some(term))
        } else {
            Ok(None)
        }
    }

    #[inline]
    fn from_consed(this: Self::Consed, _ctx: &mut (impl ConsCtx + ?Sized)) -> Self::Substituted {
        this
    }
}

impl Typecheck for UniverseTerm {
    fn do_local_tyck(&self, _ctx: &mut (impl ConsCtx + ?Sized)) -> bool {
        true
    }

    fn do_global_tyck(&self, _ctx: &mut (impl TyCtxMut + ?Sized)) -> Option<bool> {
        Some(true)
    }

    #[inline]
    fn do_annot_tyck(&self, ctx: &mut (impl TyCtxMut + ?Sized)) -> Option<bool> {
        if let Some(annot) = &self.annot {
            if let Ok(succ) = self.as_universe().succ() {
                Some(succ.is_subtype_in(&*annot.base(), &mut Structural)? && annot.tyck(ctx)?)
            } else {
                Some(false)
            }
        } else {
            Some(true)
        }
    }

    #[inline]
    fn load_flags(&self) -> TyckFlags {
        self.flags.load_flags()
    }

    #[inline]
    fn set_flags(&self, flags: TyckFlags) {
        self.flags.set_flags(flags);
    }
}

impl HasDependencies for UniverseTerm {
    #[inline]
    fn get_filter(&self) -> VarFilter {
        self.filter
    }

    #[inline]
    fn has_var_dep(&self, ix: u32, equiv: bool) -> bool {
        self.filter.contains(ix) && (self.filter.exact() || self.annot.has_var_dep(ix, equiv))
    }

    fn has_dep_below(&self, ix: u32, base: u32) -> bool {
        if let Some(contains) = self.filter.contains_below(ix, base) {
            contains
        } else {
            self.annot.has_dep_below(ix, base)
        }
    }
}

impl TermEq for UniverseTerm {
    #[inline]
    fn eq_in(&self, other: &Self, ctx: &mut (impl TermEqCtxMut + ?Sized)) -> Option<bool> {
        if let Some(result) =
            ctx.approx_term_eq_code((self.code, Form::BetaEta), (other.code, Form::BetaEta))
        {
            return result;
        }
        Some(
            self.universe == other.universe
                && (!ctx.cmp_annot() || self.annot.eq_in(&other.annot, ctx)?)
                && (!ctx.cmp_var() || self.annot.is_some() == other.annot.is_some()),
        )
    }
}

impl Value for UniverseTerm {
    type ValueConsed = UniverseTerm;
    type ValueSubstituted = UniverseTerm;

    #[inline]
    fn into_term_direct(self) -> Term {
        Term::Universe(self)
    }

    #[inline]
    fn annot(&self) -> Option<AnnotationRef> {
        if let Some(ty) = &self.annot {
            Some(ty.borrow_annot())
        } else {
            self.universe.annot()
        }
    }

    #[inline]
    fn id(&self) -> Option<NodeIx> {
        None
    }

    #[inline]
    fn is_local_ty(&self) -> Option<bool> {
        Some(true)
    }

    #[inline]
    fn is_local_universe(&self) -> Option<bool> {
        Some(true)
    }

    #[inline]
    fn is_local_function(&self) -> Option<bool> {
        Some(false)
    }

    #[inline]
    fn is_local_pi(&self) -> Option<bool> {
        Some(false)
    }

    #[inline]
    fn is_root(&self) -> bool {
        true
    }

    #[inline]
    fn coerce(
        &self,
        ty: Option<TermId>,
        ctx: &mut (impl TyCtxMut + ?Sized),
    ) -> Result<Option<UniverseTerm>, Error> {
        if let Some(annot) = &self.annot {
            if let Some(ty) = ty {
                if let Some(annot) = annot.coerce_ty(&ty, ctx)? {
                    return self.annotate_unchecked(annot, ctx.cons_ctx());
                }
            }
            Ok(None)
        } else {
            self.universe.coerce(ty, ctx)
        }
    }

    #[inline]
    fn annotate_unchecked(
        &self,
        annot: Annotation,
        ctx: &mut (impl ConsCtx + ?Sized),
    ) -> Result<Option<UniverseTerm>, Error> {
        if let Some(curr_annot) = &self.annot {
            if curr_annot.is_sub_annot(&annot) {
                Ok(None)
            } else {
                Ok(Some(UniverseTerm::new_unchecked(
                    self.universe.clone(),
                    Some(annot),
                )))
            }
        } else {
            self.universe.annotate_unchecked(annot, ctx)
        }
    }

    #[inline]
    fn is_subtype_in(&self, other: &Term, ctx: &mut (impl TermEqCtxMut + ?Sized)) -> Option<bool> {
        self.universe.is_subtype_in(other, ctx)
    }

    #[inline]
    fn universe(&self) -> Option<Universe> {
        self.universe.succ().ok()
    }

    #[inline]
    fn code(&self) -> Code {
        self.code
    }

    #[inline]
    fn untyped_code(&self) -> Code {
        self.universe.code()
    }

    #[inline]
    fn eq_term_in(&self, other: &Term, ctx: &mut (impl TermEqCtxMut + ?Sized)) -> Option<bool> {
        //TODO: optimize
        match other {
            Term::Universe(other) => self.eq_in(other, ctx),
            other if other.is_root() => Some(false),
            _ => None,
        }
    }

    #[inline]
    fn form(&self) -> Form {
        Form::BetaEta
    }
}

/// A typing universe
#[derive(Clone, PartialEq, Eq, Hash, PartialOrd, Ord)]
pub struct Universe(u32);

impl Universe {
    /// Try to create a new universe given a level and variable flag
    #[inline]
    pub fn try_new(level: u32, is_var: bool) -> Result<Universe, Error> {
        level
            .checked_mul(2)
            .ok_or(Error::UniverseOutOfBounds)?
            .checked_add(is_var as u32)
            .ok_or(Error::UniverseOutOfBounds)
            .map(Universe)
    }

    /// Get the universe of sets
    #[inline]
    pub fn set() -> Universe {
        Universe(2)
    }

    /// Get a universe containing this one
    #[inline]
    pub fn succ(&self) -> Result<Universe, Error> {
        self.0
            .checked_add(2)
            .ok_or(Error::UniverseOutOfBounds)
            .map(Universe)
    }

    /// Get a universe containing both input universes
    #[inline]
    pub fn join(&self, other: &Universe) -> Universe {
        let level = (self.0 / 2).max(other.0 / 2);
        let is_var = (self.0 % 2).max(other.0 % 2);
        Universe((level * 2) | is_var)
    }

    /// Get the level of this universe
    #[inline]
    pub fn level(&self) -> u32 {
        self.0 / 2
    }

    /// Get whether this universe is a variable
    #[inline]
    pub fn is_var(&self) -> bool {
        self.0 % 2 == 1
    }
}

impl Cons for Universe {
    type Consed = Universe;

    #[inline]
    fn cons(&self, _ctx: &mut (impl ConsCtx + ?Sized)) -> Option<Self> {
        None
    }

    #[inline]
    fn to_consed_ty(&self) -> Self::Consed {
        self.clone()
    }
}

impl Substitute for Universe {
    type Substituted = Universe;

    #[inline]
    fn subst_rec(
        &self,
        _ctx: &mut (impl SubstCtx + ?Sized),
    ) -> Result<Option<Self::Substituted>, Error> {
        Ok(None)
    }

    #[inline]
    fn from_consed(this: Self::Consed, _ctx: &mut (impl ConsCtx + ?Sized)) -> Self::Substituted {
        this
    }
}

impl Typecheck for Universe {
    fn do_local_tyck(&self, _ctx: &mut (impl ConsCtx + ?Sized)) -> bool {
        true
    }

    fn do_global_tyck(&self, _ctx: &mut (impl TyCtxMut + ?Sized)) -> Option<bool> {
        Some(true)
    }

    #[inline]
    fn do_annot_tyck(&self, _ctx: &mut (impl TyCtxMut + ?Sized)) -> Option<bool> {
        Some(true)
    }

    #[inline]
    fn tyck(&self, _ctx: &mut (impl TyCtxMut + ?Sized)) -> Option<bool> {
        Some(true)
    }

    #[inline]
    fn load_flags(&self) -> TyckFlags {
        TyckFlags::ALL_TRUE
    }

    #[inline]
    fn set_flags(&self, _flags: TyckFlags) {
        //TODO: check flags are all compatible with ALL_TRUE
    }
}

impl HasDependencies for Universe {
    #[inline]
    fn has_var_dep(&self, _ix: u32, _equiv: bool) -> bool {
        false
    }

    #[inline]
    fn has_dep_below(&self, _ix: u32, _base: u32) -> bool {
        false
    }

    #[inline]
    fn get_filter(&self) -> VarFilter {
        VarFilter::EMPTY
    }
}

impl TermEq for Universe {
    #[inline]
    fn eq_in(&self, other: &Self, _ctx: &mut (impl TermEqCtxMut + ?Sized)) -> Option<bool> {
        Some(self.eq(other))
    }
}

impl Value for Universe {
    type ValueConsed = UniverseTerm;
    type ValueSubstituted = UniverseTerm;

    #[inline]
    fn into_term_direct(self) -> Term {
        Term::Universe(self.into())
    }

    #[inline]
    fn annot(&self) -> Option<AnnotationRef> {
        self.succ().ok().map(AnnotationRef::Universe)
    }

    #[inline]
    fn id(&self) -> Option<NodeIx> {
        None
    }

    #[inline]
    fn is_local_ty(&self) -> Option<bool> {
        // Universes are always types
        Some(true)
    }

    #[inline]
    fn is_local_universe(&self) -> Option<bool> {
        Some(true)
    }

    #[inline]
    fn is_local_function(&self) -> Option<bool> {
        Some(false)
    }

    #[inline]
    fn is_local_pi(&self) -> Option<bool> {
        Some(false)
    }

    #[inline]
    fn is_root(&self) -> bool {
        true
    }

    #[inline]
    fn coerce(
        &self,
        ty: Option<TermId>,
        ctx: &mut (impl TyCtxMut + ?Sized),
    ) -> Result<Option<UniverseTerm>, Error> {
        if let Some(ty) = ty {
            if let Some(annot) = self.succ()?.coerce_annot_ty(&ty, ctx)? {
                self.annotate_unchecked(annot, ctx.cons_ctx())
            } else {
                Ok(None)
            }
        } else {
            Ok(None)
        }
    }

    #[inline]
    fn annotate_unchecked(
        &self,
        annot: Annotation,
        _ctx: &mut (impl ConsCtx + ?Sized),
    ) -> Result<Option<UniverseTerm>, Error> {
        if let Some(true) = self.succ()?.is_subtype(&*annot.ty()) {
            Ok(None)
        } else {
            Ok(Some(UniverseTerm::new_unchecked(self.clone(), Some(annot))))
        }
    }

    #[inline]
    fn is_subtype_in(&self, other: &Term, ctx: &mut (impl TermEqCtxMut + ?Sized)) -> Option<bool> {
        //TODO: optimize
        let rooted = ctx.root_mut(other);
        match (other, rooted.as_deref()) {
            (_, Some(Term::Universe(other))) | (Term::Universe(other), _) => {
                Some(self <= other.as_universe())
            }
            _ if other.is_root() => Some(false),
            _ => None,
        }
    }

    #[inline]
    fn universe(&self) -> Option<Universe> {
        self.succ().ok()
    }

    #[inline]
    fn code(&self) -> Code {
        let universe = 0x756e697665727365; // "universe" in ASCII
        let mut hasher = AHasher::new_with_keys(universe, 0x0);
        self.0.hash(&mut hasher);
        Code(hasher.finish())
    }

    #[inline]
    fn untyped_code(&self) -> Code {
        self.code()
    }

    #[inline]
    fn eq_term_in(&self, other: &Term, ctx: &mut (impl TermEqCtxMut + ?Sized)) -> Option<bool> {
        match other {
            Term::Universe(other) => UniverseTerm::from(self.clone()).eq_in(other, ctx),
            _ if other.is_root() => Some(false),
            _ => None,
        }
    }

    #[inline]
    fn form(&self) -> Form {
        Form::BetaEta
    }
}

impl Eq for UniverseTerm {}

impl PartialEq for UniverseTerm {
    #[inline]
    fn eq(&self, other: &Self) -> bool {
        self.code == other.code && self.universe == other.universe && self.annot == other.annot
    }
}

impl Hash for UniverseTerm {
    #[inline]
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.universe.hash(state);
        self.annot.hash(state)
    }
}

impl PartialEq<Term> for UniverseTerm {
    #[inline]
    fn eq(&self, other: &Term) -> bool {
        match other {
            Term::Universe(other) => self.eq(other),
            _ => false,
        }
    }
}

impl PartialEq<TermId> for UniverseTerm {
    #[inline]
    fn eq(&self, other: &TermId) -> bool {
        self.eq(&**other)
    }
}

impl PartialEq<Universe> for UniverseTerm {
    fn eq(&self, other: &Universe) -> bool {
        self.annot.is_none() && self.universe.eq(other)
    }
}

impl PartialEq<UniverseTerm> for Universe {
    fn eq(&self, other: &UniverseTerm) -> bool {
        other.annot.is_none() && other.universe.eq(self)
    }
}

impl PartialEq<Term> for Universe {
    #[inline]
    fn eq(&self, other: &Term) -> bool {
        match other {
            Term::Universe(other) => self.eq(other),
            _ => false,
        }
    }
}

impl PartialEq<TermId> for Universe {
    #[inline]
    fn eq(&self, other: &TermId) -> bool {
        self.eq(&**other)
    }
}

impl Debug for Universe {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        if self.is_var() {
            write!(f, "UniverseVar({})", self.level())
        } else {
            write!(f, "Universe({})", self.level())
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;
    fn test_set_properties(ctx: &mut impl TyCtxMut) {
        let set = Universe::set();

        assert_eq!(set.cons_id(ctx.cons_ctx()), None);
        super::value_test_utils::test_value_invariants(&set, ctx.cons_ctx());
        super::value_test_utils::test_value_invariants(
            &set.clone_into_id_with(ctx.cons_ctx()),
            ctx.cons_ctx(),
        );
        let type_ = set.succ().unwrap();
        assert_eq!(type_.cons_id(ctx.cons_ctx()), None);
        assert_eq!(set.annot(), Some(AnnotationRef::Universe(type_.clone())));
        super::value_test_utils::test_value_invariants(&type_, ctx.cons_ctx());
        super::value_test_utils::test_value_invariants(
            &type_.clone_into_id_with(ctx.cons_ctx()),
            ctx.cons_ctx(),
        );

        let set_type = UniverseTerm::new_unchecked(
            set.clone(),
            Annotation::from_ty(type_.into_id_with(ctx.cons_ctx())).ok(),
        );
        assert_eq!(set_type.tyck(ctx), Some(true));
        let set_cycle = UniverseTerm::new_unchecked(
            set.clone(),
            Annotation::from_ty(set.into_id_with(ctx.cons_ctx())).ok(),
        );
        assert_eq!(set_cycle.tyck(ctx), Some(false));
    }

    #[test]
    fn set_properties() {
        test_set_properties(&mut Trivial::default());
        test_set_properties(&mut DisjointSetCtx::default());
    }
}
