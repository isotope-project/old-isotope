/*!
Identity types
*/
use super::*;

/// The reflexivity axiom: asserts that the left hand side and right hand side are judgementally equal, and therefore propositionally equal
#[derive(Debug, Clone)]
pub struct Refl {
    /// The left hand side of the equality
    left: TermId,
    /// The right hand side of the equality
    right: TermId,
    /// The type of this proof
    annot: Option<Annotation>,
    /// This proof's code
    code: Code,
    /// This proof's filter
    filter: VarFilter,
    /// This proof's flags
    flags: AtomicTyckFlags,
}

impl Refl {
    /// Create a new, unchecked instance of the reflexivity axiom with a given annotation
    #[inline]
    pub fn new_unchecked_with(left: TermId, right: TermId, annot: Option<Annotation>) -> Refl {
        let code = Self::compute_code(&left, &right, annot.as_ref());
        let filter = Self::compute_filter(&left, &right, annot.as_ref());
        let flags = Self::compute_flags(&left, &right, annot.as_ref()).into();
        Refl {
            left,
            right,
            annot,
            code,
            filter,
            flags,
        }
    }

    /// Compute the most basic identity type for a given instance of the reflexivity axiom
    #[inline]
    pub fn compute_ty(
        left: &TermId,
        right: &TermId,
        ctx: &mut (impl ConsCtx + ?Sized),
    ) -> Option<Id> {
        if let Some(universe) = Id::compute_ty(left, right) {
            Some(Id::new_unchecked_with(
                left.clone_into_id_with(ctx),
                right.clone_into_id_with(ctx),
                Annotation::from_ty(universe.into_id_with(ctx)).ok(),
            ))
        } else {
            None
        }
    }

    /// Create a new, unchecked instance of the reflexivity axiom, inferring the most basic possible annotation
    #[inline]
    pub fn new_unchecked(left: TermId, right: TermId, ctx: &mut (impl ConsCtx + ?Sized)) -> Refl {
        let annot = if let Some(ty) = Self::compute_ty(&left, &right, ctx) {
            Annotation::from_ty(ty.into_id_with(ctx)).ok()
        } else {
            None
        };
        Self::new_unchecked_with(left, right, annot)
    }

    /// Compute the code of a reflexivity instance with a given type annotation
    #[inline]
    pub fn compute_code(left: &TermId, right: &TermId, annot: Option<&Annotation>) -> Code {
        let mut hasher = AHasher::new_with_keys(0x362, 0x6423);
        let left = left.code();
        let right = right.code();
        left.pure().hash(&mut hasher);
        right.pure().hash(&mut hasher);
        let pre = hasher.finish();
        annot.hash(&mut hasher);
        let post = hasher.finish();
        Code::new(pre, post)
    }

    /// Compute the filter of a reflexivity instance with a given type annotation
    #[inline]
    pub fn compute_filter(left: &TermId, right: &TermId, annot: Option<&Annotation>) -> VarFilter {
        left.get_filter()
            .union(right.get_filter())
            .union(annot.get_filter())
    }

    /// Compute the flags of a reflexivity instance with a given type annotation
    #[inline]
    pub fn compute_flags(left: &TermId, right: &TermId, annot: Option<&Annotation>) -> TyckFlags {
        TyckFlags::default().with_flag(
            TyckFlag::GlobalTyck,
            L4::with_false(!annot.maybe_tyck())
                & L4::from(left.maybe_tyck())
                & L4::from(right.maybe_tyck()),
        )
    }

    /// Compute whether a dependent function type locally type-checks given an annotation
    pub fn compute_local_tyck(left: &TermId, right: &TermId, annot: AnnotationRef) -> bool {
        if let Term::Id(id) = &*annot.base() {
            Self::compute_local_tyck_id(left, right, id)
        } else {
            false
        }
    }

    /// Compute whether a dependent function type locally type-checks given an identity type
    pub fn compute_local_tyck_id(left: &TermId, right: &TermId, id: &Id) -> bool {
        left.eq_in(id.left(), &mut Untyped).unwrap_or(false)
            && right.eq_in(id.right(), &mut Untyped).unwrap_or(false)
    }

    /// Get the left-hand-side, i.e. source, of this path
    pub fn left(&self) -> &TermId {
        &self.left
    }

    /// Get the right hand side, i.e. target, of this path
    pub fn right(&self) -> &TermId {
        &self.right
    }

    /// Get the left and right hand side of this path if *equivalent*
    pub fn as_equal(&self) -> Option<&TermId> {
        //TODO: looser definition of equivalence?
        if self.left == self.right {
            Some(&self.left)
        } else {
            None
        }
    }

    /// Get the left and side of this path if different from the right hand side
    pub fn diff_right(&self) -> Option<&TermId> {
        if self.left != self.right {
            Some(&self.right)
        } else {
            None
        }
    }

    /// If both sides of this equality are universes, return one.
    /// Return `None` if neither side is a universe, or if both sides are different universes (implying this instance is invalid!)
    pub fn as_universe(&self) -> Option<Universe> {
        match (&*self.left, &*self.right) {
            (Term::Universe(left), Term::Universe(right)) => {
                let left = left.as_universe();
                let right = right.as_universe();
                if left == right {
                    Some(left.clone())
                } else {
                    None
                }
            }
            (Term::Universe(u), _) | (_, Term::Universe(u)) => Some(u.as_universe().clone()),
            _ => None,
        }
    }

    /// If the right side of this equality is a pi type, return it.
    pub fn as_pi(&self) -> Option<&Pi> {
        if let Term::Pi(pi) = &*self.right {
            Some(pi)
        } else {
            None
        }
    }

    /// Attempt to transport the right side of this path in a given typing context.
    pub fn transport_right(
        &self,
        target: &TermId,
        ctx: &mut (impl TyCtxMut + ?Sized),
    ) -> Result<Option<Refl>, Error> {
        if *target == self.right {
            return Ok(None);
        }
        self.transported_right(target, ctx).map(Some)
    }

    /// Get this path transported right in a given typing context
    pub fn transported_right(
        &self,
        target: &TermId,
        ctx: &mut (impl TyCtxMut + ?Sized),
    ) -> Result<Refl, Error> {
        let right_eq = self.right.eq_in(target, ctx.eq_ctx());
        match right_eq {
            Some(true) | None => Ok(Refl::new_unchecked(
                self.left.clone(),
                target.clone(),
                ctx.cons_ctx(),
            )),
            Some(false) => Err(Error::TermMismatch),
        }
    }

    /// Get this instance of reflexivity as a coerced annotation
    pub fn coerce_right(
        &self,
        target: &TermId,
        ctx: &mut (impl TyCtxMut + ?Sized),
    ) -> Result<Option<Annotation>, Error> {
        if *target == self.right {
            return Ok(None);
        }
        let result = if let Some(true) = self.left.is_subtype(target) {
            Annotation::from_ty(target.clone())?
        } else {
            Annotation::from_path(
                self.transported_right(target, ctx)?
                    .into_id_with(ctx.cons_ctx()),
            )?
        };
        Ok(Some(result))
    }

    /// Convert this annotation into it's equal sides, or error if unequal
    pub fn into_equal(self) -> Result<TermId, Refl> {
        if self.left == self.right {
            Ok(self.left)
        } else {
            Err(self)
        }
    }
}

/// The identity type, representing proofs that two terms are equal
#[derive(Debug, Clone)]
pub struct Id {
    /// The left hand side of the (potential) equality
    left: TermId,
    /// The right hand side of the (potential) equality
    right: TermId,
    /// The type of this type
    annot: Option<Annotation>,
    /// This type's code
    code: Code,
    /// This type's filter
    filter: VarFilter,
    /// This type's flags
    flags: AtomicTyckFlags,
}

impl Id {
    /// Create a new, unchecked instance of an identity type with a given annotation
    #[inline]
    pub fn new_unchecked_with(left: TermId, right: TermId, annot: Option<Annotation>) -> Id {
        let code = Self::compute_code(&left, &right, annot.as_ref());
        let filter = Self::compute_filter(&left, &right, annot.as_ref());
        let flags = Self::compute_flags(&left, &right, annot.as_ref()).into();
        Id {
            left,
            right,
            annot,
            code,
            filter,
            flags,
        }
    }

    /// Compute the most basic universe for an identity type
    #[inline]
    pub fn compute_ty(left: &TermId, right: &TermId) -> Option<Universe> {
        if let (Some(left), Some(right)) = (left.annot(), right.annot()) {
            if let (Some(left), Some(right)) = (left.as_universe(), right.as_universe()) {
                return Some(left.join(&right));
            }
        }
        None
    }

    /// Create a new, unchecked instance of the reflexivity axiom, inferring the most basic possible annotation
    #[inline]
    pub fn new_unchecked(left: TermId, right: TermId, ctx: &mut (impl ConsCtx + ?Sized)) -> Id {
        let annot = if let Some(ty) = Self::compute_ty(&left, &right) {
            Annotation::from_ty(ty.into_id_with(ctx)).ok()
        } else {
            None
        };
        Self::new_unchecked_with(left, right, annot)
    }

    /// Compute the code of an identity type with a given type annotation
    #[inline]
    pub fn compute_code(left: &TermId, right: &TermId, annot: Option<&Annotation>) -> Code {
        let mut hasher = AHasher::new_with_keys(0x5322, 0x7532);
        let left = left.code();
        let right = right.code();
        left.pure().hash(&mut hasher);
        right.pure().hash(&mut hasher);
        let pre = hasher.finish();
        left.hash(&mut hasher);
        right.hash(&mut hasher);
        annot.hash(&mut hasher);
        let post = hasher.finish();
        Code::new(pre, post)
    }

    /// Compute the filter of a reflexivity instance with a given type annotation
    #[inline]
    pub fn compute_filter(left: &TermId, right: &TermId, annot: Option<&Annotation>) -> VarFilter {
        left.get_filter()
            .union(right.get_filter())
            .union(annot.get_filter())
    }

    /// Compute the flags of a reflexivity instance with a given type annotation
    #[inline]
    pub fn compute_flags(left: &TermId, right: &TermId, annot: Option<&Annotation>) -> TyckFlags {
        TyckFlags::default().with_flag(
            TyckFlag::GlobalTyck,
            L4::with_false(!annot.maybe_tyck())
                & L4::from(left.maybe_tyck())
                & L4::from(right.maybe_tyck()),
        )
    }

    /// Compute whether a dependent function type locally type-checks given an annotation
    pub fn compute_local_tyck(left: &TermId, right: &TermId, annot: AnnotationRef) -> bool {
        if let Term::Universe(base) = &*annot.base() {
            Self::compute_local_tyck_universe(left, right, base.as_universe())
        } else {
            false
        }
    }

    /// Compute whether a dependent function type locally type-checks given a universe
    pub fn compute_local_tyck_universe(left: &TermId, right: &TermId, base: &Universe) -> bool {
        if let (Some(left), Some(right)) = (left.annot(), right.annot()) {
            if let (Some(left), Some(right)) = (left.as_universe(), right.as_universe()) {
                return left <= *base && right <= *base;
            }
        }
        false
    }

    /// Get the left-hand-side, i.e. source, of this path
    pub fn left(&self) -> &TermId {
        &self.left
    }

    /// Get the right hand side, i.e. target, of this path
    pub fn right(&self) -> &TermId {
        &self.right
    }

    /// If both sides of this equality are types, get them as a universe.
    /// Return `None` if neither side is a universe, or if both sides are different universes (implying this instance is invalid!)
    pub fn as_universe(&self) -> Option<Universe> {
        match (&*self.left, &*self.right) {
            (Term::Universe(left), Term::Universe(right)) => {
                let left = left.as_universe();
                let right = right.as_universe();
                if left == right {
                    Some(left.clone())
                } else {
                    None
                }
            }
            (Term::Universe(u), _) | (_, Term::Universe(u)) => Some(u.as_universe().clone()),
            _ => None,
        }
    }
}

impl Cons for Refl {
    type Consed = Refl;

    fn cons(&self, ctx: &mut (impl ConsCtx + ?Sized)) -> Option<Self::Consed> {
        let left = self.left.cons(ctx);
        let right = self.right.cons(ctx);
        let annot = self.annot.cons(ctx);
        if left.is_none() && right.is_none() && annot.is_none() {
            None
        } else {
            let left = left.unwrap_or_else(|| self.left.clone());
            let right = right.unwrap_or_else(|| self.right.clone());
            let annot = annot.unwrap_or_else(|| self.annot.clone());
            Some(Refl {
                left,
                right,
                annot,
                code: self.code,
                filter: self.filter,
                flags: self.flags.clone(),
            })
        }
    }

    #[inline]
    fn to_consed_ty(&self) -> Self::Consed {
        self.clone()
    }
}

impl Substitute for Refl {
    type Substituted = Refl;

    fn subst_rec(&self, ctx: &mut (impl SubstCtx + ?Sized)) -> Result<Option<Refl>, Error> {
        let left = self.left.subst_rec(ctx)?;
        let right = self.right.subst_rec(ctx)?;
        let annot = self.annot.cons(ctx.cons_ctx());
        if left.is_none() && right.is_none() && annot.is_none() {
            Ok(None)
        } else {
            let left = left.unwrap_or_else(|| self.left.clone());
            let right = right.unwrap_or_else(|| self.right.clone());
            let annot = annot.unwrap_or_else(|| self.annot.clone());
            let code = Self::compute_code(&left, &right, annot.as_ref());
            let filter = Self::compute_filter(&left, &right, annot.as_ref());
            let flags = Self::compute_flags(&left, &right, annot.as_ref()).into();
            Ok(Some(Refl {
                left,
                right,
                annot,
                code,
                filter,
                flags,
            }))
        }
    }

    #[inline]
    fn from_consed(this: Self::Consed, _ctx: &mut (impl ConsCtx + ?Sized)) -> Self::Substituted {
        this
    }
}

impl HasDependencies for Refl {
    fn has_var_dep(&self, ix: u32, equiv: bool) -> bool {
        self.filter.contains(ix)
            && (self.filter.exact()
                || self.left.has_var_dep(ix, equiv)
                || self.right.has_var_dep(ix, equiv)
                || self.annot.has_var_dep(ix, equiv))
    }

    fn has_dep_below(&self, ix: u32, base: u32) -> bool {
        if let Some(contains) = self.filter.contains_below(ix, base) {
            contains
        } else {
            self.left.has_dep_below(ix, base)
                || self.right.has_dep_below(ix + 1, base + 1)
                || self.annot.has_dep_below(ix, base)
        }
    }

    #[inline]
    fn get_filter(&self) -> VarFilter {
        self.filter
    }
}

impl TermEq for Refl {
    fn eq_in(&self, other: &Self, ctx: &mut (impl TermEqCtxMut + ?Sized)) -> Option<bool> {
        if let Some(result) =
            ctx.approx_term_eq_code((self.code, Form::BetaEta), (other.code, Form::BetaEta))
        {
            return result;
        }
        let annot = if ctx.cmp_annot() {
            self.annot.eq_in(&other.annot, ctx)
        } else if ctx.cmp_var() {
            Some(self.annot.is_some() == other.annot.is_some())
        } else {
            Some(true)
        };
        if let Some(false) = annot {
            return Some(false);
        }
        let left = self.left.eq_in(&other.left, ctx);
        if let Some(false) = left {
            return Some(false);
        }
        let right = self.right.eq_in(&other.right, ctx);
        if let Some(false) = right {
            return Some(false);
        }
        Some(annot? && left? && right?)
    }
}

impl Typecheck for Refl {
    fn do_local_tyck(&self, _ctx: &mut (impl ConsCtx + ?Sized)) -> bool {
        if let Some(annot) = &self.annot {
            Self::compute_local_tyck(&self.left, &self.right, annot.borrow_annot())
        } else {
            false
        }
    }

    fn do_global_tyck(&self, ctx: &mut (impl TyCtxMut + ?Sized)) -> Option<bool> {
        Some(
            self.left.tyck(ctx)?
                && self.right.tyck(ctx)?
                && self.left.eq_in(&self.right, ctx.eq_ctx())?,
        )
    }

    fn do_annot_tyck(&self, ctx: &mut (impl TyCtxMut + ?Sized)) -> Option<bool> {
        self.annot.tyck(ctx)
    }

    fn load_flags(&self) -> TyckFlags {
        self.flags.load_flags()
    }

    fn set_flags(&self, flags: TyckFlags) {
        self.flags.set_flags(flags);
    }
}

impl Value for Refl {
    type ValueConsed = Refl;
    type ValueSubstituted = Refl;

    #[inline]
    fn into_term_direct(self) -> Term {
        Term::Refl(self)
    }

    #[inline]
    fn annot(&self) -> Option<AnnotationRef> {
        self.annot.as_ref().map(Annotation::borrow_annot)
    }

    #[inline]
    fn is_local_ty(&self) -> Option<bool> {
        Some(false)
    }

    #[inline]
    fn is_local_universe(&self) -> Option<bool> {
        Some(false)
    }

    #[inline]
    fn is_local_function(&self) -> Option<bool> {
        Some(false)
    }

    #[inline]
    fn is_local_pi(&self) -> Option<bool> {
        Some(false)
    }

    #[inline]
    fn is_root(&self) -> bool {
        true
    }

    #[inline]
    fn coerce(
        &self,
        _ty: Option<TermId>,
        _ctx: &mut (impl TyCtxMut + ?Sized),
    ) -> Result<Option<Refl>, Error> {
        //TODO: this
        Err(Error::NotImplemented)
    }

    fn annotate_unchecked(
        &self,
        _annot: Annotation,
        _ctx: &mut (impl ConsCtx + ?Sized),
    ) -> Result<Option<Refl>, Error> {
        //TODO: this
        Err(Error::NotImplemented)
    }

    #[inline]
    fn is_subtype_in(
        &self,
        _other: &Term,
        _ctx: &mut (impl TermEqCtxMut + ?Sized),
    ) -> Option<bool> {
        // refl instances are never types!
        Some(false)
    }

    #[inline]
    fn universe(&self) -> Option<Universe> {
        None
    }

    #[inline]
    fn code(&self) -> Code {
        self.code
    }

    #[inline]
    fn untyped_code(&self) -> Code {
        Self::compute_code(&self.left, &self.right, None)
    }

    #[inline]
    fn eq_term_in(&self, other: &Term, ctx: &mut (impl TermEqCtxMut + ?Sized)) -> Option<bool> {
        match other {
            Term::Refl(other) => self.eq_in(other, ctx),
            _ => Some(false),
        }
    }

    #[inline]
    fn form(&self) -> Form {
        //TODO: lhs normalization?
        Form::BetaEta
    }
}

impl Eq for Refl {}

impl PartialEq for Refl {
    fn eq(&self, other: &Self) -> bool {
        self.eq_in(other, &mut Structural).unwrap_or(false)
    }
}

impl Cons for Id {
    type Consed = Id;

    fn cons(&self, ctx: &mut (impl ConsCtx + ?Sized)) -> Option<Self::Consed> {
        let left = self.left.cons(ctx);
        let right = self.right.cons(ctx);
        let annot = self.annot.cons(ctx);
        if left.is_none() && right.is_none() && annot.is_none() {
            None
        } else {
            let left = left.unwrap_or_else(|| self.left.clone());
            let right = right.unwrap_or_else(|| self.right.clone());
            let annot = annot.unwrap_or_else(|| self.annot.clone());
            Some(Id {
                left,
                right,
                annot,
                code: self.code,
                filter: self.filter,
                flags: self.flags.clone(),
            })
        }
    }

    #[inline]
    fn to_consed_ty(&self) -> Self::Consed {
        self.clone()
    }
}

impl Substitute for Id {
    type Substituted = Id;

    fn subst_rec(&self, ctx: &mut (impl SubstCtx + ?Sized)) -> Result<Option<Id>, Error> {
        let left = self.left.subst_rec(ctx)?;
        let right = self.right.subst_rec(ctx)?;
        let annot = self.annot.cons(ctx.cons_ctx());
        if left.is_none() && right.is_none() && annot.is_none() {
            Ok(None)
        } else {
            let left = left.unwrap_or_else(|| self.left.clone());
            let right = right.unwrap_or_else(|| self.right.clone());
            let annot = annot.unwrap_or_else(|| self.annot.clone());
            let code = Self::compute_code(&left, &right, annot.as_ref());
            let filter = Self::compute_filter(&left, &right, annot.as_ref());
            let flags = Self::compute_flags(&left, &right, annot.as_ref()).into();
            Ok(Some(Id {
                left,
                right,
                annot,
                code,
                filter,
                flags,
            }))
        }
    }

    #[inline]
    fn from_consed(this: Self::Consed, _ctx: &mut (impl ConsCtx + ?Sized)) -> Self::Substituted {
        this
    }
}

impl HasDependencies for Id {
    fn has_var_dep(&self, ix: u32, equiv: bool) -> bool {
        self.filter.contains(ix)
            && (self.filter.exact()
                || self.left.has_var_dep(ix, equiv)
                || self.right.has_var_dep(ix, equiv)
                || self.annot.has_var_dep(ix, equiv))
    }

    fn has_dep_below(&self, ix: u32, base: u32) -> bool {
        if let Some(contains) = self.filter.contains_below(ix, base) {
            contains
        } else {
            self.left.has_dep_below(ix, base)
                || self.right.has_dep_below(ix, base)
                || self.annot.has_dep_below(ix, base)
        }
    }

    #[inline]
    fn get_filter(&self) -> VarFilter {
        self.filter
    }
}

impl TermEq for Id {
    fn eq_in(&self, other: &Self, ctx: &mut (impl TermEqCtxMut + ?Sized)) -> Option<bool> {
        if let Some(result) =
            ctx.approx_term_eq_code((self.code, Form::BetaEta), (other.code, Form::BetaEta))
        {
            return result;
        }
        let annot = if ctx.cmp_annot() {
            self.annot.eq_in(&other.annot, ctx)
        } else if ctx.cmp_var() {
            Some(self.annot.is_some() == other.annot.is_some())
        } else {
            Some(true)
        };
        if let Some(false) = annot {
            return Some(false);
        }
        let left = self.left.eq_in(&other.left, ctx);
        if let Some(false) = left {
            return Some(false);
        }
        let right = self.right.eq_in(&other.right, ctx);
        if let Some(false) = right {
            return Some(false);
        }
        Some(annot? && left? && right?)
    }
}

impl Typecheck for Id {
    fn do_local_tyck(&self, _ctx: &mut (impl ConsCtx + ?Sized)) -> bool {
        if let Some(annot) = &self.annot {
            Self::compute_local_tyck(&self.left, &self.right, annot.borrow_annot())
        } else {
            false
        }
    }

    fn do_global_tyck(&self, ctx: &mut (impl TyCtxMut + ?Sized)) -> Option<bool> {
        Some(self.left.tyck(ctx)? && self.right.tyck(ctx)?)
    }

    fn do_annot_tyck(&self, ctx: &mut (impl TyCtxMut + ?Sized)) -> Option<bool> {
        self.annot.tyck(ctx)
    }

    fn load_flags(&self) -> TyckFlags {
        self.flags.load_flags()
    }

    fn set_flags(&self, flags: TyckFlags) {
        self.flags.set_flags(flags);
    }
}

impl Value for Id {
    type ValueConsed = Id;
    type ValueSubstituted = Id;

    #[inline]
    fn into_term_direct(self) -> Term {
        Term::Id(self)
    }

    #[inline]
    fn annot(&self) -> Option<AnnotationRef> {
        self.annot.as_ref().map(Annotation::borrow_annot)
    }

    #[inline]
    fn is_local_ty(&self) -> Option<bool> {
        Some(true)
    }

    #[inline]
    fn is_local_universe(&self) -> Option<bool> {
        Some(false)
    }

    #[inline]
    fn is_local_function(&self) -> Option<bool> {
        Some(false)
    }

    #[inline]
    fn is_local_pi(&self) -> Option<bool> {
        Some(false)
    }

    #[inline]
    fn is_root(&self) -> bool {
        true
    }

    #[inline]
    fn coerce(
        &self,
        _ty: Option<TermId>,
        _ctx: &mut (impl TyCtxMut + ?Sized),
    ) -> Result<Option<Id>, Error> {
        //TODO: this
        Err(Error::NotImplemented)
    }

    fn annotate_unchecked(
        &self,
        _annot: Annotation,
        _ctx: &mut (impl ConsCtx + ?Sized),
    ) -> Result<Option<Id>, Error> {
        //TODO: this
        Err(Error::NotImplemented)
    }

    #[inline]
    fn is_subtype_in(&self, other: &Term, ctx: &mut (impl TermEqCtxMut + ?Sized)) -> Option<bool> {
        if let Term::Id(other) = other {
            Some(self.left.eq_in(&other.left, ctx)? && self.right.eq_in(&other.right, ctx)?)
        } else {
            Some(false)
        }
    }

    #[inline]
    fn code(&self) -> Code {
        self.code
    }

    #[inline]
    fn untyped_code(&self) -> Code {
        Self::compute_code(&self.left, &self.right, None)
    }

    #[inline]
    fn eq_term_in(&self, other: &Term, ctx: &mut (impl TermEqCtxMut + ?Sized)) -> Option<bool> {
        match other {
            Term::Id(other) => self.eq_in(other, ctx),
            _ => Some(false),
        }
    }

    #[inline]
    fn form(&self) -> Form {
        //TODO: lhs normalization?
        Form::BetaEta
    }
}

impl Eq for Id {}

impl PartialEq for Id {
    fn eq(&self, other: &Self) -> bool {
        self.eq_in(other, &mut Structural).unwrap_or(false)
    }
}
