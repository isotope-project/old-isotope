/*!
Function application
*/
use super::*;

/// A function application, with an optional type annotation
#[derive(Debug, Clone)]
pub struct App {
    /// The function being applied
    left: TermId,
    /// The argument to the function
    right: TermId,
    /// The type annotation of this application
    annot: Option<Annotation>,
    /// The code of this application
    code: Code,
    /// The filter of this application
    filter: VarFilter,
    /// The cached flags of this application
    flags: AtomicTyckFlags,
    /// This application's form
    form: Form,
}

impl App {
    /// Create a new application with an *unchecked* type annotation
    #[inline]
    pub fn new_unchecked(left: TermId, right: TermId, annot: Option<Annotation>) -> App {
        let code = Self::compute_code(&left, &right, annot.as_ref());
        let filter = Self::compute_filter(&left, &right, annot.as_ref());
        let flags = Self::compute_flags(&left, &right, annot.as_ref()).into();
        let form = Self::compute_form(&left, &right, annot.as_ref());
        App {
            left,
            right,
            annot,
            code,
            filter,
            flags,
            form,
        }
    }

    /// Create a new application, inferring a *direct* application with a given (optional) transport
    #[inline]
    pub fn new_direct(
        left: TermId,
        right: TermId,
        ty: Option<TermId>,
        ctx: &mut (impl TyCtxMut + ?Sized),
    ) -> App {
        let annot = Self::compute_annot(&left, &right, ty, ctx).ok();
        let typed = annot.is_some();
        let result = Self::new_unchecked(left, right, annot);
        result.flags.set_flag(LocalTyck, typed.into());
        result
    }

    /// Create a new application, inferring a *direct* application
    #[inline]
    pub fn with_ty(
        left: TermId,
        right: TermId,
        ty: TermId,
        ctx: &mut (impl TyCtxMut + ?Sized),
    ) -> App {
        Self::new_direct(left, right, Some(ty), ctx)
    }

    /// Create a new application, inferring a *direct* application with a given transport
    #[inline]
    pub fn new(left: TermId, right: TermId, ctx: &mut (impl TyCtxMut + ?Sized)) -> App {
        Self::new_direct(left, right, None, ctx)
    }

    /// Compute the form of an application
    #[inline]
    pub fn compute_form(left: &TermId, right: &TermId, _annot: Option<&Annotation>) -> Form {
        //TODO: applicable method?
        //TODO: eta normalcy
        left.form()
            .meet(right.form().join(Form::Head))
            .meet(if let Term::Lambda(_) = &**left {
                Form::Eta
            } else {
                Form::BetaEta
            })
    }

    /// Compute the code of an application with a given type annotation
    #[inline]
    pub fn compute_code(left: &TermId, right: &TermId, annot: Option<&Annotation>) -> Code {
        let mut hasher = AHasher::new_with_keys(0x54, 0x7842);
        let left_code = left.code();
        let right_code = right.code();
        left_code.pure().hash(&mut hasher);
        right_code.pure().hash(&mut hasher);
        let pre = hasher.finish();
        annot.hash(&mut hasher);
        let post = hasher.finish();
        Code::new(pre, post)
    }

    /// Compute the filter of an application with a given type annotation
    #[inline]
    pub fn compute_filter(left: &TermId, right: &TermId, annot: Option<&Annotation>) -> VarFilter {
        left.get_filter()
            .union(right.get_filter())
            .union(annot.get_filter())
    }

    /// Compute the flags of an application with a given type annotation
    #[inline]
    pub fn compute_flags(left: &TermId, right: &TermId, annot: Option<&Annotation>) -> TyckFlags {
        TyckFlags::default().with_flag(
            TyckFlag::GlobalTyck,
            L4::with_false(!annot.maybe_tyck())
                & L4::from(left.maybe_tyck())
                & L4::from(right.maybe_tyck()),
        )
    }

    /// Compute whether a dependent function type locally type-checks given an annotation
    ///
    /// TODO: consider whether passing in a context here could be helpful...
    pub fn compute_local_tyck(left: &TermId, right: &TermId, annot: AnnotationRef) -> bool {
        if let Ok(ty) = Self::compute_ty(left, right, &mut Trivial::default()) {
            annot.base().eq_id_in(&ty, &mut Structural).unwrap_or(false)
        } else {
            false
        }
    }

    /// Compute the type of a *direct* application
    pub fn compute_ty(
        left: &TermId,
        right: &TermId,
        ctx: &mut (impl TyCtxMut + ?Sized),
    ) -> Result<TermId, Error> {
        if right.annot().is_none() {
            return Err(Error::AnnotationRequired);
        }
        let left_ty = left.ty().ok_or(Error::AnnotationRequired)?;
        let mut args = smallvec![right.clone()];
        let result = left_ty.apply_ty(&mut args, &mut SubstVec::new(ctx))?;
        if result.is_none() {
            debug_assert_eq!(args.len(), 1);
        } else {
            debug_assert_eq!(args.len(), 0);
        }
        result.ok_or(Error::ExpectedPi)
    }

    /// Compute the annotation of a *direct* application
    pub fn compute_annot(
        left: &TermId,
        right: &TermId,
        ty: Option<TermId>,
        ctx: &mut (impl TyCtxMut + ?Sized),
    ) -> Result<Annotation, Error> {
        let mut base_annot = Annotation::from_ty(Self::compute_ty(left, right, ctx)?)?;
        if let Some(ty) = ty {
            if let Some(coercion) = base_annot.coerce_ty(&ty, ctx)? {
                base_annot = coercion
            }
        }
        Ok(base_annot)
    }

    /// Get the left hand side of this application
    #[inline]
    pub fn left(&self) -> &TermId {
        &self.left
    }

    /// Get the right hand side of this application
    #[inline]
    pub fn right(&self) -> &TermId {
        &self.right
    }
}

impl Cons for App {
    type Consed = App;

    #[inline]
    fn cons(&self, ctx: &mut (impl ConsCtx + ?Sized)) -> Option<Self::Consed> {
        let left = self.left.cons_id(ctx);
        let right = self.right.cons_id(ctx);
        let ty = self.annot.cons(ctx).flatten();
        if left.is_none() && right.is_none() && ty.is_none() {
            return None;
        }
        //NOTE: we call `into_id_direct` first to save us the spurious `uncons` call
        let result = App {
            left: left.unwrap_or_else(|| self.left.clone()),
            right: right.unwrap_or_else(|| self.right.clone()),
            annot: ty.or_else(|| self.annot.clone()),
            code: self.code,
            filter: self.filter,
            flags: self.flags.clone(),
            form: self.form,
        };
        Some(result)
    }

    #[inline]
    fn to_consed_ty(&self) -> Self::Consed {
        self.clone()
    }
}

impl Substitute for App {
    type Substituted = App;

    fn subst_rec(&self, ctx: &mut (impl SubstCtx + ?Sized)) -> Result<Option<App>, Error> {
        if !ctx.intersects(self.filter, self.code, self.form) {
            return Ok(None);
        }
        let left = self.left.subst_id(ctx)?;
        let right = self.right.subst_id(ctx)?;
        if left.is_none() && right.is_none() {
            return Ok(None);
        }
        let ty = self.annot.subst_rec(ctx)?;
        let left = left.unwrap_or_else(|| self.left.clone_into_id_with(ctx.cons_ctx()));
        let right = right.unwrap_or_else(|| self.right.clone_into_id_with(ctx.cons_ctx()));
        let annot = ty.unwrap_or_else(|| self.annot.consed(ctx.cons_ctx()));
        let filter = Self::compute_filter(&left, &right, annot.as_ref());
        let code = Self::compute_code(&left, &right, annot.as_ref());
        //TODO: term flag optimization
        let flags = Self::compute_flags(&left, &right, annot.as_ref()).into();
        let form = Self::compute_form(&left, &right, annot.as_ref());
        let term = App {
            left,
            right,
            annot,
            filter,
            flags,
            code,
            form,
        };
        Ok(Some(term))
    }

    #[inline]
    fn from_consed(this: Self::Consed, _ctx: &mut (impl ConsCtx + ?Sized)) -> Self::Substituted {
        this
    }
}

impl Typecheck for App {
    fn do_local_tyck(&self, _ctx: &mut (impl ConsCtx + ?Sized)) -> bool {
        if let Some(annot) = &self.annot {
            Self::compute_local_tyck(&self.left, &self.right, annot.borrow_annot())
        } else {
            false
        }
    }

    fn do_global_tyck(&self, ctx: &mut (impl TyCtxMut + ?Sized)) -> Option<bool> {
        Some(self.left.tyck(ctx)? && self.right.tyck(ctx)?)
    }

    #[inline]
    fn do_annot_tyck(&self, ctx: &mut (impl TyCtxMut + ?Sized)) -> Option<bool> {
        self.annot.tyck(ctx)
    }

    #[inline]
    fn load_flags(&self) -> TyckFlags {
        let flags = self.flags.load_flags();
        flags
    }

    #[inline]
    fn set_flags(&self, flags: TyckFlags) {
        self.flags.set_flags(flags);
    }
}

impl HasDependencies for App {
    #[inline]
    fn has_var_dep(&self, ix: u32, equiv: bool) -> bool {
        self.filter.contains(ix)
            && (self.filter.exact()
                || self.left.has_var_dep(ix, equiv)
                || self.right.has_var_dep(ix, equiv)
                || self.annot.has_var_dep(ix, equiv))
    }

    fn has_dep_below(&self, ix: u32, base: u32) -> bool {
        if let Some(contains) = self.filter.contains_below(ix, base) {
            contains
        } else {
            self.left.has_dep_below(ix, base)
                || self.right.has_dep_below(ix, base)
                || self.annot.has_dep_below(ix, base)
        }
    }

    #[inline]
    fn get_filter(&self) -> VarFilter {
        self.filter
    }
}

impl TermEq for App {
    #[inline]
    fn eq_in(&self, other: &Self, ctx: &mut (impl TermEqCtxMut + ?Sized)) -> Option<bool> {
        if let Some(result) =
            ctx.approx_term_eq_code((self.code, self.form), (other.code, other.form))
        {
            return result;
        }
        let annot = if ctx.cmp_annot() {
            self.annot.eq_in(&other.annot, ctx)
        } else if ctx.cmp_var() {
            Some(self.annot.is_some() == other.annot.is_some())
        } else {
            Some(true)
        };
        if let Some(false) = annot {
            return Some(false);
        }
        let left = self.left.eq_in(&other.left, ctx);
        if let Some(false) = left {
            return Some(false);
        }
        let right = self.right.eq_in(&other.right, ctx);
        if let Some(false) = right {
            return Some(false);
        }
        Some(annot? && left? && right?)
    }
}

impl Value for App {
    type ValueConsed = App;
    type ValueSubstituted = App;

    #[inline]
    fn into_term_direct(self) -> Term {
        Term::App(self)
    }

    #[inline]
    fn annot(&self) -> Option<AnnotationRef> {
        self.annot.as_ref().map(Annotation::borrow_annot)
    }

    #[inline]
    fn id(&self) -> Option<NodeIx> {
        None
    }

    #[inline]
    fn is_local_ty(&self) -> Option<bool> {
        self.ty()?.is_local_universe()
    }

    #[inline]
    fn is_local_universe(&self) -> Option<bool> {
        None
    }

    #[inline]
    fn is_local_function(&self) -> Option<bool> {
        self.ty()?.is_local_pi()
    }

    #[inline]
    fn is_local_pi(&self) -> Option<bool> {
        None
    }

    #[inline]
    fn is_root(&self) -> bool {
        false
    }

    #[inline]
    fn coerce(
        &self,
        ty: Option<TermId>,
        ctx: &mut (impl TyCtxMut + ?Sized),
    ) -> Result<Option<App>, Error> {
        if let Some(annot) = &self.annot {
            //TODO: deal with locally-bad annotations?
            if let Some(ty) = ty {
                if let Some(annot) = annot.coerce_ty(&ty, ctx)? {
                    return self.annotate_unchecked(annot, ctx.cons_ctx());
                }
            }
            Ok(None)
        } else {
            let left = self.left.coerced(None, ctx)?;
            //TODO: think about the error code here
            //TODO: root for pi via equality context?
            let left_param = left
                .annot()
                .ok_or(Error::AnnotationRequired)?
                .as_pi()
                .ok_or(Error::ExpectedPi)?
                .param_ty();
            let right = self.right.coerced(Some(left_param.clone()), ctx)?;
            //TODO: error on failed construction?
            let result = Self::new_direct(left, right, ty, ctx);
            debug_assert_eq!(result.cons(ctx.cons_ctx()), None);
            Ok(Some(result))
        }
    }

    #[inline]
    fn annotate_unchecked(
        &self,
        annot: Annotation,
        _ctx: &mut (impl ConsCtx + ?Sized),
    ) -> Result<Option<App>, Error> {
        if let Some(curr_annot) = &self.annot {
            if curr_annot.is_sub_annot(&annot) {
                return Ok(None);
            }
        }
        Ok(Some(App::new_unchecked(
            self.left.clone(),
            self.right.clone(),
            Some(annot),
        )))
    }

    #[inline]
    fn is_subtype_in(&self, other: &Term, ctx: &mut (impl TermEqCtxMut + ?Sized)) -> Option<bool> {
        self.eq_term_in(other, ctx)
    }

    #[inline]
    fn universe(&self) -> Option<Universe> {
        //TODO: this
        None
    }

    #[inline]
    fn code(&self) -> Code {
        self.code
    }

    #[inline]
    fn untyped_code(&self) -> Code {
        Self::compute_code(&self.left, &self.right, None)
    }

    #[inline]
    fn eq_term_in(&self, other: &Term, ctx: &mut (impl TermEqCtxMut + ?Sized)) -> Option<bool> {
        //TODO: optimize
        match other {
            Term::App(other) => self.eq_in(other, ctx),
            _ => None,
        }
    }

    #[inline]
    fn apply(
        &self,
        args: &mut SmallVec<[TermId; 2]>,
        ctx: &mut (impl EvalCtx + ?Sized),
    ) -> Result<Option<TermId>, Error> {
        let no_args = args.len();
        args.push(self.right.clone());
        let left = self.left.apply(args, ctx)?;
        debug_assert!(args.len() <= no_args + 1);
        if args.len() == no_args + 1 {
            args.pop();
            if let Some(left) = left {
                let result = App::new_unchecked(left, self.right.clone(), self.annot.clone());
                Ok(Some(result.into_id_with(ctx.cons_ctx())))
            } else {
                self.subst_id(ctx)
            }
        } else {
            debug_assert_ne!(left, None);
            Ok(left)
        }
    }

    #[inline]
    fn form(&self) -> Form {
        self.form
    }
}

impl Eq for App {}

impl PartialEq for App {
    #[inline]
    fn eq(&self, other: &Self) -> bool {
        self.eq_in(other, &mut Structural).unwrap_or(false)
    }
}

impl Hash for App {
    #[inline]
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.left.hash(state);
        self.right.hash(state);
        self.annot.hash(state);
    }
}

impl PartialEq<Term> for App {
    #[inline]
    fn eq(&self, other: &Term) -> bool {
        match other {
            Term::App(other) => self.eq(other),
            _ => false,
        }
    }
}

impl PartialEq<TermId> for App {
    #[inline]
    fn eq(&self, other: &TermId) -> bool {
        self.eq(&**other)
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn basic_subst() {
        let mut ctx = DisjointSetCtx::default();
        let vars = [
            Var::with_ix(0).into_id_with(&mut ctx),
            Var::with_ix(1).into_id_with(&mut ctx),
            Var::with_ix(2).into_id_with(&mut ctx),
        ];
        let apps = [
            [
                App::new_unchecked(vars[0].clone(), vars[0].clone(), None).into_id_with(&mut ctx),
                App::new_unchecked(vars[0].clone(), vars[1].clone(), None).into_id_with(&mut ctx),
                App::new_unchecked(vars[0].clone(), vars[2].clone(), None).into_id_with(&mut ctx),
            ],
            [
                App::new_unchecked(vars[1].clone(), vars[0].clone(), None).into_id_with(&mut ctx),
                App::new_unchecked(vars[1].clone(), vars[1].clone(), None).into_id_with(&mut ctx),
                App::new_unchecked(vars[1].clone(), vars[2].clone(), None).into_id_with(&mut ctx),
            ],
            [
                App::new_unchecked(vars[2].clone(), vars[0].clone(), None).into_id_with(&mut ctx),
                App::new_unchecked(vars[2].clone(), vars[1].clone(), None).into_id_with(&mut ctx),
                App::new_unchecked(vars[2].clone(), vars[2].clone(), None).into_id_with(&mut ctx),
            ],
        ];
        let mut subst = Shift::new(-1, 1, &mut ctx);
        for (i, a) in apps.iter().enumerate() {
            for (j, app) in a.iter().enumerate() {
                let subst = app.subst_id(&mut subst);
                if i == 1 || j == 1 {
                    assert_eq!(subst, Err(Error::HasDependency))
                } else if i == 0 && j == 0 {
                    assert_eq!(subst, Ok(None))
                } else {
                    let subst = subst.unwrap().unwrap();
                    assert_eq!(subst, apps[i.saturating_sub(1)][j.saturating_sub(1)]);
                }
            }
        }
        for (i, v) in vars.iter().enumerate() {
            let vs = [v];
            let mut subst = SubstSlice::new_with(&vs[..], 1, &mut ctx).unwrap();
            for (j, v) in apps.iter().enumerate() {
                for (k, app) in v.iter().enumerate() {
                    let subst = app.subst_id(&mut subst);
                    let ji = if j == 1 { i } else { j.saturating_sub(1) };
                    let ki = if k == 1 { i } else { k.saturating_sub(1) };
                    if j == 0 && k == 0 {
                        assert_eq!(subst, Ok(None))
                    } else {
                        //NOTE: we may still have j == ji && k == ki, as we don't do a check for this
                        let subst = subst.unwrap().unwrap();
                        assert_eq!(subst, apps[ji][ki]);
                        //TODO: assert_eq!(subst.flags, SubstFlags::ID);
                    }
                }
            }
        }
    }
}
