/*!
A variable
*/
use std::u32;

use super::*;

/// A variable, represented by a de-Bruijn index
#[derive(Debug, Clone)]
pub struct Var {
    /// The index of this variable
    ix: u32,
    /// This variable's dependent variable filter
    filter: VarFilter,
    /// This variable's type annotation, if any
    ty: Option<TermId>,
    /// This variable's code
    code: Code,
    /// This variable's pre-computed flags
    flags: AtomicTyckFlags,
}

impl Var {
    /// Create a new variable with a given index having a given type annotation
    #[inline]
    pub fn new_unchecked(ix: u32, ty: Option<TermId>) -> Var {
        let code = Self::compute_code(ix, ty.as_ref());
        let filter = Self::compute_filter(ix, ty.as_ref());
        let flags = Self::compute_flags(ix, ty.as_ref()).into();
        Var {
            ix,
            filter,
            ty,
            code,
            flags,
        }
    }

    /// Create a new variable with a given index having no type annotation
    #[inline]
    pub fn with_ix(ix: u32) -> Var {
        Var::new_unchecked(ix, None)
    }

    /// Create a new variable with a given index and type annotation
    #[inline]
    pub fn with_ty(ix: u32, ty: TermId) -> Var {
        Var::new_unchecked(ix, Some(ty))
    }

    /// Compute the code of a variable with a given index and type
    #[inline]
    pub fn compute_code(ix: u32, ty: Option<&TermId>) -> Code {
        // "variable encoding" in ASCII
        let mut hasher = AHasher::new_with_keys(0x7661726961626c65, 0x656e636f64696e67);
        ix.hash(&mut hasher);
        let pre = hasher.finish();
        ty.hash(&mut hasher);
        let post = hasher.finish();
        Code::new(pre, post)
    }

    /// Compute the filter for a variable with a given index and type
    pub fn compute_filter(ix: u32, ty: Option<&TermId>) -> VarFilter {
        ty.map(TermId::get_filter)
            .unwrap_or(VarFilter::EMPTY)
            .with_index(ix)
    }

    /// Compute the flags for a variable with a given index and type
    pub fn compute_flags(_ix: u32, ty: Option<&TermId>) -> TyckFlags {
        if ty.is_some() {
            TyckFlags::EMPTY
        } else {
            TyckFlags::NIL
        }
    }

    /// Get the type of this variable
    #[inline]
    pub fn get_ty(&self) -> Option<&TermId> {
        self.ty.as_ref()
    }

    /// Get this variable's index
    #[inline]
    pub fn ix(&self) -> u32 {
        self.ix
    }

    /// Check coercion of this variable to a type
    #[inline]
    pub fn check_coercion(
        &self,
        ty: Option<&TermId>,
        ctx: &mut (impl TyCtxMut + ?Sized),
    ) -> Result<(), Error> {
        if let Some(ty) = &self.ty {
            if !ctx.constrain(self.ix, ty)?.unwrap_or(false) {
                return Err(Error::VarAnnotMismatch);
            }
        }
        if let Some(ty) = ty {
            if !ctx.constrain(self.ix, ty)?.unwrap_or(false) {
                return Err(Error::TypeMismatch);
            }
        }
        Ok(())
    }
}

impl Cons for Var {
    type Consed = Var;

    #[inline]
    fn cons(&self, ctx: &mut (impl ConsCtx + ?Sized)) -> Option<Self::Consed> {
        if let Some(consed_ty) = self.ty.cons(ctx).flatten() {
            //NOTE: we call `into_id_direct` first to save us the spurious `uncons` call
            return Some(Var::with_ty(self.ix, consed_ty));
        } else {
            None
        }
    }

    #[inline]
    fn to_consed_ty(&self) -> Self::Consed {
        self.clone()
    }
}

impl Substitute for Var {
    type Substituted = TermId;

    #[inline]
    fn subst_rec(&self, ctx: &mut (impl SubstCtx + ?Sized)) -> Result<Option<TermId>, Error> {
        ctx.subst_constrain(self.ix, self.ty.as_ref())
    }

    #[inline]
    fn from_consed(this: Self::Consed, ctx: &mut (impl ConsCtx + ?Sized)) -> Self::Substituted {
        this.into_id_with(ctx)
    }
}

impl Typecheck for Var {
    fn do_local_tyck(&self, _ctx: &mut (impl ConsCtx + ?Sized)) -> bool {
        if let Some(ty) = &self.ty {
            !ty.has_dep_below(self.ix + 1, 0)
        } else {
            false
        }
    }

    fn do_global_tyck(&self, ctx: &mut (impl TyCtxMut + ?Sized)) -> Option<bool> {
        if let Some(ty) = &self.ty {
            if ty.tyck(ctx)? {
                let result = ctx.constrain(self.ix, ty).ok().flatten();
                result
            } else {
                Some(false)
            }
        } else {
            Some(false)
        }
    }

    #[inline]
    fn do_annot_tyck(&self, ctx: &mut (impl TyCtxMut + ?Sized)) -> Option<bool> {
        self.annot().tyck(ctx)
    }

    #[inline]
    fn load_flags(&self) -> TyckFlags {
        self.flags.load_flags()
    }

    #[inline]
    fn set_flags(&self, flags: TyckFlags) {
        self.flags.set_flags(flags);
    }
}

impl HasDependencies for Var {
    #[inline]
    fn get_filter(&self) -> VarFilter {
        self.filter
    }

    #[inline]
    fn has_var_dep(&self, ix: u32, equiv: bool) -> bool {
        if !self.filter.contains(ix) {
            return false;
        }
        if self.filter.exact() || VarFilter::equiv(ix, self.ix, equiv) {
            return true;
        }
        debug_assert_ne!(self.ty, None);
        self.ty.has_var_dep(ix, equiv)
    }

    #[inline]
    fn has_dep_below(&self, ix: u32, base: u32) -> bool {
        if let Some(contains) = self.filter.contains_below(ix, base) {
            contains
        } else if self.ix == ix {
            true
        } else if self.local_tyck(&mut ()) {
            // If this term is locally type-checked, then the type of this variable can never depend on lower variables
            // so we may safely raise the base as an optimization
            self.ty.has_dep_below(ix, base.max(self.ix + 1))
        } else {
            self.ty.has_dep_below(ix, base)
        }
    }
}

impl TermEq for Var {
    #[inline]
    fn eq_in(&self, other: &Self, ctx: &mut (impl TermEqCtxMut + ?Sized)) -> Option<bool> {
        if let Some(result) =
            ctx.approx_term_eq_code((self.code, Form::BetaEta), (other.code, Form::BetaEta))
        {
            return result;
        }
        Some(self.ix == other.ix && (!ctx.cmp_var() || self.ty.eq_in(&other.ty, ctx)?))
    }
}

impl Value for Var {
    type ValueConsed = Var;
    type ValueSubstituted = TermId;

    #[inline]
    fn into_term_direct(self) -> Term {
        Term::Var(self)
    }

    #[inline]
    fn annot(&self) -> Option<AnnotationRef> {
        self.ty.as_ref().map(AnnotationRef::Type)
    }

    #[inline]
    fn id(&self) -> Option<NodeIx> {
        None
    }

    #[inline]
    fn is_local_ty(&self) -> Option<bool> {
        if let Some(ty) = &self.ty {
            ty.is_local_universe()
        } else {
            None
        }
    }

    #[inline]
    fn is_local_universe(&self) -> Option<bool> {
        Some(false)
    }

    #[inline]
    fn is_local_function(&self) -> Option<bool> {
        self.ty()?.is_local_pi()
    }

    #[inline]
    fn is_local_pi(&self) -> Option<bool> {
        Some(false)
    }

    #[inline]
    fn is_root(&self) -> bool {
        true
    }

    #[inline]
    fn coerce(
        &self,
        ty: Option<TermId>,
        ctx: &mut (impl TyCtxMut + ?Sized),
    ) -> Result<Option<Var>, Error> {
        self.check_coercion(ty.as_ref(), ctx)?;
        let ty = if let Some(ty) = ty {
            if Some(&ty) == self.ty.as_ref() {
                return Ok(None);
            } else {
                ty
            }
        } else if self.ty.is_none() {
            if let Some(infer) = ctx.infer(self.ix) {
                infer
            } else {
                return Err(Error::InferenceFailure);
            }
        } else {
            return Ok(None);
        };
        Ok(Some(Self::new_unchecked(self.ix, Some(ty))))
    }

    #[inline]
    fn annotate_unchecked(
        &self,
        annot: Annotation,
        _ctx: &mut (impl ConsCtx + ?Sized),
    ) -> Result<Option<Var>, Error> {
        if let Some(ty) = &self.ty {
            if let Some(true) = ty.is_subtype(&*annot.ty()) {
                return Ok(None);
            }
        }
        if let Some(false) = annot.ty().is_local_ty() {
            Err(Error::ExpectedType)
        } else {
            Ok(Some(Self::with_ty(self.ix, annot.into_ty())))
        }
    }

    #[inline]
    fn is_subtype_in(&self, other: &Term, ctx: &mut (impl TermEqCtxMut + ?Sized)) -> Option<bool> {
        if self.is_local_ty() == Some(false) || other.is_local_ty() == Some(false) {
            return Some(false);
        }
        self.eq_term_in(other, ctx)
    }

    #[inline]
    fn universe(&self) -> Option<Universe> {
        if let Some(ty) = &self.ty {
            match &**ty {
                Term::Universe(u) => Some(u.as_universe().clone()),
                _ => None,
            }
        } else {
            None
        }
    }

    #[inline]
    fn code(&self) -> Code {
        self.code
    }

    #[inline]
    fn untyped_code(&self) -> Code {
        let result = Self::compute_code(self.ix, None);
        debug_assert!(self.ty.is_some() || result == self.code);
        result
    }

    #[inline]
    fn eq_term_in(&self, other: &Term, ctx: &mut (impl TermEqCtxMut + ?Sized)) -> Option<bool> {
        match other {
            Term::Var(other) => self.eq_in(other, ctx),
            _ => None,
        }
    }

    #[inline]
    fn form(&self) -> Form {
        Form::BetaEta
    }
}

impl Eq for Var {}

impl PartialEq for Var {
    #[inline]
    fn eq(&self, other: &Self) -> bool {
        self.eq_in(other, &mut Structural).unwrap_or(false)
    }
}

impl Hash for Var {
    #[inline]
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.ix.hash(state);
        self.ty.hash(state);
    }
}

impl PartialEq<TermId> for Var {
    #[inline]
    fn eq(&self, other: &TermId) -> bool {
        self.eq(&**other)
    }
}

impl PartialEq<Term> for Var {
    #[inline]
    fn eq(&self, other: &Term) -> bool {
        match other {
            Term::Var(other) => self.eq(other),
            _ => false,
        }
    }
}

/// A filter for variables
//TODO: maximum universe level in filter?
//TODO: move normalization flags from code to filter?
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash, Default)]
pub struct VarFilter {
    /// This filter's free variable bound
    fvb: u32,
    /// The indices encountered by this filter
    indices: u32,
}

impl VarFilter {
    /// A filter with no indices set
    pub const EMPTY: VarFilter = VarFilter { fvb: 0, indices: 0 };

    /// The index to check when shifting down
    ///
    /// Rather than implementing this manually, you should use `shift_down_value`.
    pub const SHIFT_DOWN_CHECK_IX: u32 = 32;

    /// The index to set when a dependency check performed while shifting down returns `true`
    ///
    /// Rather than implementing this manually, you should use `shift_down_value`.
    pub const SHIFT_DOWN_SET_IX: u32 = Self::SHIFT_DOWN_CHECK_IX - 1;

    /// The number of bits in an FVB filter
    const BITS: u32 = 32;

    /// Get whether two indices are equivalent or equal
    #[inline]
    pub const fn equiv(left: u32, right: u32, equiv: bool) -> bool {
        if equiv {
            left % Self::BITS == right % Self::BITS
        } else {
            left == right
        }
    }

    /// Get this filter with an index set as a dependency
    #[inline]
    pub const fn with_index(self, ix: u32) -> VarFilter {
        VarFilter {
            fvb: if self.fvb > ix { self.fvb } else { ix },
            indices: self.indices | (1 << (ix % Self::BITS)),
        }
    }

    /// Get a filter with a single index set
    #[inline]
    pub const fn singleton(ix: u32) -> VarFilter {
        Self::EMPTY.with_index(ix)
    }

    /// Get an index bitset with all indices strictly below `ix` and above `base` set
    #[inline]
    const fn range_bitset(ix: u32, base: u32) -> u32 {
        n_ones_const_u32((ix + 1).saturating_sub(base)).rotate_left(base)
    }

    /// Get a filter with all indices below `ix` and above `base` set
    #[inline]
    pub const fn range(ix: u32, base: u32) -> VarFilter {
        VarFilter {
            fvb: if ix > base { ix } else { 0 },
            indices: Self::range_bitset(ix, base),
        }
    }

    /// Get whether this filter is exact
    #[inline]
    pub const fn exact(self) -> bool {
        self.fvb < Self::BITS
    }

    /// Get whether a filter contains an index in the given equivalence class, and an index greater than or equal to `ix`
    #[inline]
    pub const fn contains(self, ix: u32) -> bool {
        ix <= self.fvb && self.indices & (1 << (ix % Self::BITS)) != 0
    }

    /// Get whether a filter may contain an index `base <= index <= ix`
    #[inline]
    pub const fn contains_below(self, ix: u32, base: u32) -> Option<bool> {
        if base < self.fvb || ix >= base {
            Some(false)
        } else if self.fvb <= ix && self.fvb > 0 {
            Some(true)
        } else if self.indices & Self::range_bitset(ix, base) != 0 {
            if self.exact() {
                Some(true)
            } else {
                None
            }
        } else {
            Some(false)
        }
    }

    /// Get whether this filter may contain any of the `len` indices after `base`.
    #[inline]
    pub const fn contains_ix_range(self, base: u32, len: u32) -> bool {
        if base > self.fvb || len == 0 {
            return false;
        }
        let possible_range = self.fvb - base;
        let possible_len = if possible_range > len {
            len
        } else {
            possible_range
        };
        let mask = n_ones_const_u32(possible_len).rotate_left(base);
        self.indices & mask != 0
    }

    /// Get whether a filter contains an index in the given equivalence class
    #[inline]
    pub const fn contains_equiv(self, ix: u32) -> bool {
        self.indices & (1 << (ix % Self::BITS)) != 0
    }

    /// Set an index as a dependency
    #[inline]
    pub fn set_index(&mut self, ix: u32) {
        debug_assert!(
            (self.fvb == 0 && self.indices == 0) || self.fvb >= (31 - self.indices.leading_zeros()),
            "FVB {} is less than highest bit of index set 0b{:32b} ({})",
            self.fvb,
            self.indices,
            32 - self.indices.leading_zeros()
        );
        self.fvb = self.fvb.max(ix);
        self.indices |= 1 << (ix % Self::BITS);
        debug_assert!(
            self.fvb >= (Self::BITS - 1 - self.indices.leading_zeros()),
            "FVB {} is less than highest bit of index set {:#032b} ({})",
            self.fvb,
            self.indices,
            Self::BITS - self.indices.leading_zeros()
        );
        debug_assert!(self.contains(ix));
    }

    /// Get whether this filter is empty
    #[inline]
    pub const fn is_empty(self) -> bool {
        self.fvb == 0
    }

    /// Get the minimum number of indices in this filter
    #[inline]
    pub const fn min_len(self) -> u32 {
        self.indices.count_ones()
    }

    /// Shift this filter down one level, and return it along with whether the top index must be re-checked
    #[inline]
    pub const fn shift_down(self) -> (VarFilter, bool) {
        let result = VarFilter {
            fvb: self.fvb.saturating_sub(1),
            indices: self.indices.wrapping_shr(1),
        };
        if self.fvb > Self::BITS {
            (result, self.contains(0))
        } else if self.fvb == Self::BITS {
            (result.with_index(Self::BITS - 1), false)
        } else {
            (result, false)
        }
    }

    /// Shift this filter down one level, automatically performing necessary checks
    #[inline]
    pub fn shift_down_value(self, value: &impl Value) -> VarFilter {
        let (mut result, check) = self.shift_down();
        if check {
            if value.has_var_dep(Self::SHIFT_DOWN_CHECK_IX, true) {
                result.set_index(Self::SHIFT_DOWN_SET_IX)
            }
        }
        result
    }

    /// Get the union of this filter set with another
    #[inline]
    pub const fn union(self, other: VarFilter) -> VarFilter {
        VarFilter {
            fvb: if self.fvb > other.fvb {
                self.fvb
            } else {
                other.fvb
            },
            indices: self.indices | other.indices,
        }
    }

    /// Get the intersection of this filter set with another
    #[inline]
    pub const fn intersection(self, other: VarFilter) -> VarFilter {
        VarFilter {
            //TODO: exact filter case...
            fvb: if self.fvb < other.fvb {
                self.fvb
            } else {
                other.fvb
            },
            indices: self.indices & other.indices,
        }
    }

    /// Get the free variable bound of this filter
    #[inline]
    pub const fn fvb(&self) -> u32 {
        self.fvb
    }
}

impl BitAnd for VarFilter {
    type Output = VarFilter;

    #[inline]
    fn bitand(self, rhs: Self) -> Self::Output {
        self.intersection(rhs)
    }
}

impl BitAndAssign for VarFilter {
    #[inline]
    fn bitand_assign(&mut self, rhs: Self) {
        *self = self.intersection(rhs)
    }
}

impl BitOr for VarFilter {
    type Output = VarFilter;

    fn bitor(self, rhs: Self) -> Self::Output {
        self.union(rhs)
    }
}

impl BitOrAssign for VarFilter {
    #[inline]
    fn bitor_assign(&mut self, rhs: Self) {
        *self = self.union(rhs)
    }
}

impl Index<u32> for VarFilter {
    type Output = bool;

    #[inline]
    fn index(&self, index: u32) -> &Self::Output {
        if self.contains(index) {
            &true
        } else {
            &false
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn shift_up_no_annotation() {
        let var3 = Var::with_ix(3);
        let var5 = Var::with_ix(5);
        let var7 = Var::with_ix(7);
        let var9 = Var::with_ix(9);
        assert_eq!(None, var3.shift_id(2, 5, &mut ()).unwrap());
        assert_eq!(var7, var5.shift_id(2, 5, &mut ()).unwrap().unwrap());
        assert_eq!(var9, var7.shift_id(2, 5, &mut ()).unwrap().unwrap());
        assert_eq!(var5, var3.shift_id(2, 3, &mut ()).unwrap().unwrap());
        assert_eq!(var7, var5.shift_id(2, 3, &mut ()).unwrap().unwrap());
        assert_eq!(var9, var7.shift_id(2, 3, &mut ()).unwrap().unwrap());
        let mut ctx = DisjointSetCtx::default();
        assert_eq!(None, var3.shift_id(2, 5, &mut ctx).unwrap());
        assert_eq!(var7, var5.shift_id(2, 5, &mut ctx).unwrap().unwrap());
        assert_eq!(var9, var7.shift_id(2, 5, &mut ctx).unwrap().unwrap());
        assert_eq!(var5, var3.shift_id(2, 3, &mut ctx).unwrap().unwrap());
        assert_eq!(var7, var5.shift_id(2, 3, &mut ctx).unwrap().unwrap());
        assert_eq!(var9, var7.shift_id(2, 3, &mut ctx).unwrap().unwrap());
    }

    #[test]
    fn basic_var_properties() {
        let set = Universe::set().into_id_direct();
        let ixs = [0, 3, 7, 31, 32, 63, 64, 127, 128, 255, 256, 511, 512];
        for &ix in &ixs {
            let untyped_var = Var::with_ix(ix).into_id_direct();
            assert_eq!(untyped_var.annot(), None);
            let set_var = Var::with_ty(ix, set.clone()).into_id_direct();
            assert_eq!(*set_var.annot().unwrap().ty(), set);
            assert_eq!(*set_var.annot().unwrap().base(), set);
            let recursive_var = Var::with_ty(ix, untyped_var.clone()).into_id_direct();
            assert_eq!(*recursive_var.annot().unwrap().ty(), untyped_var);
            assert_eq!(*recursive_var.annot().unwrap().base(), untyped_var);
            for var in &[untyped_var, set_var, recursive_var] {
                term::value_test_utils::test_value_invariants(var, &mut ());
                term::value_test_utils::test_value_invariants(var, &mut DisjointSetCtx::default());
                assert!(var.has_var_dep(ix, false));
                assert!(!var.has_var_dep(ix + 1, false));
                assert_eq!(var.get_filter(), VarFilter::singleton(ix));
            }
            let shifted_var = Var::with_ty(ix + 1, set.clone()).into_id_direct();
            let meta_var = Var::with_ty(ix, shifted_var.clone());
            assert_eq!(*meta_var.annot().unwrap().ty(), shifted_var);
            assert_eq!(*meta_var.annot().unwrap().base(), shifted_var);
            term::value_test_utils::test_value_invariants(&meta_var, &mut ());
            term::value_test_utils::test_value_invariants(
                &meta_var,
                &mut DisjointSetCtx::default(),
            );
            assert!(meta_var.has_var_dep(ix, false));
            assert!(meta_var.has_var_dep(ix + 1, false));
            assert!(!meta_var.has_var_dep(ix + 2, false));
            assert_eq!(
                meta_var.get_filter(),
                VarFilter::singleton(ix).with_index(ix + 1)
            );
        }
    }

    #[test]
    fn null_subst() {
        let mut ctx = DisjointSetCtx::default();
        let var0 = Var::with_ix(0).into_id_with(&mut ctx);
        let var1 = Var::with_ix(1).into_id_with(&mut ctx);
        let var2 = Var::with_ix(2).into_id_with(&mut ctx);
        let mut subst = Shift::new(-1, 1, &mut ctx);
        assert_eq!(var0.subst_id(&mut subst), Ok(None));
        assert_eq!(var1.subst_id(&mut subst), Err(Error::HasDependency));
        assert_eq!(var2.subst_id(&mut subst).unwrap().unwrap(), var1);
    }

    #[test]
    fn trivial_subst() {
        let mut ctx = DisjointSetCtx::default();
        let var0 = Var::with_ix(0).into_id_with(&mut ctx);
        let var1 = Var::with_ix(1).into_id_with(&mut ctx);
        let mut subst = SubstVec::new(&mut ctx);
        assert_eq!(subst.push_subst(var1.clone()), Ok(()));
        assert_eq!(var0.subst_id(&mut subst), Ok(Some(var1)));
    }

    #[test]
    fn var_filter_properties() {
        let mut filter = VarFilter::default();
        assert_eq!(filter, VarFilter::EMPTY);
        assert_eq!(filter & VarFilter::EMPTY, VarFilter::EMPTY);
        assert_eq!(filter | VarFilter::EMPTY, filter);
        for i in 0..32 {
            assert!(!filter.contains(i));
        }
        filter.set_index(27);
        assert_eq!(filter, VarFilter::singleton(27));
        assert_ne!(filter, VarFilter::EMPTY);
        assert_eq!(filter & VarFilter::EMPTY, VarFilter::EMPTY);
        assert_eq!(filter | VarFilter::EMPTY, filter);
        for i in 0..32 {
            assert_eq!(filter.contains(i), i == 27);
            assert_eq!(filter[i], i == 27);
        }
        filter.set_index(31);
        assert_eq!(filter, VarFilter::singleton(27).with_index(31));
        assert_eq!(filter & VarFilter::EMPTY, VarFilter::EMPTY);
        assert_eq!(filter | VarFilter::EMPTY, filter);
        for i in 0..32 {
            assert_eq!(filter.contains(i), i == 27 || i == 31);
            assert_eq!(filter[i], i == 27 || i == 31);
        }
        let (shifted, recheck) = filter.shift_down();
        assert!(!recheck);
        for i in 0..32 {
            assert_eq!(shifted.contains(i), i == 26 || i == 30);
            assert_eq!(shifted[i], i == 26 || i == 30);
        }
        filter.set_index(32);
        assert_eq!(
            filter,
            VarFilter::singleton(27).with_index(31).with_index(32)
        );
        assert_eq!(filter & VarFilter::EMPTY, VarFilter::EMPTY);
        assert_eq!(filter | VarFilter::EMPTY, filter);
        for i in 0..32 {
            assert_eq!(filter.contains(i), i == 0 || i == 27 || i == 31);
            assert_eq!(filter[i], i == 0 || i == 27 || i == 31);
        }
        assert!(filter[32]);
        assert!(filter.contains(32));
        let (shifted, recheck) = filter.shift_down();
        assert!(!recheck);
        for i in 0..32 {
            assert_eq!(shifted.contains(i), i == 26 || i == 30 || i == 31);
            assert_eq!(shifted[i], i == 26 || i == 30 || i == 31);
        }
        assert!(!shifted[32]);
        assert!(!shifted.contains(32));
        filter.set_index(33);
        assert_eq!(
            filter,
            VarFilter::singleton(27)
                .with_index(31)
                .with_index(32)
                .with_index(33)
        );
        assert_eq!(filter & VarFilter::EMPTY, VarFilter::EMPTY);
        assert_eq!(filter | VarFilter::EMPTY, filter);
        for i in 0..32 {
            assert_eq!(filter.contains(i), i == 0 || i == 1 || i == 27 || i == 31);
            assert_eq!(filter[i], i == 0 || i == 1 || i == 27 || i == 31);
        }
        assert!(filter[32]);
        assert!(filter.contains(32));
        assert!(filter[33]);
        assert!(filter.contains(33));
        let (shifted, recheck) = filter.shift_down();
        assert!(recheck);
        for i in 0..32 {
            assert_eq!(shifted.contains(i), i == 0 || i == 26 || i == 30);
            assert_eq!(shifted[i], i == 0 || i == 26 || i == 30);
        }
        assert!(shifted[32]);
        assert!(shifted.contains(32));
        assert!(!shifted[33]);
        assert!(!shifted.contains(33));
    }

    #[test]
    fn range_bitset() {
        assert_eq!(VarFilter::range_bitset(4, 2), 0b11100);
        assert_eq!(VarFilter::range_bitset(39, 35), 0b11111000);
        assert_eq!(VarFilter::range_bitset(63, 32), u32::MAX);
    }
}
