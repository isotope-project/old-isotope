/*!
Case expressions
*/
use super::*;

/// A case expression, which switches on variants to return a value
#[derive(Debug, Clone)]
pub struct Case {
    /// This expression's cases, with the final branch a fallback
    cases: Arc<[TermId]>,
    /// This expression's matching type
    matches: TermId,
    /// This expression's annotation
    annot: Option<Annotation>,
    /// This expression's code
    code: Code,
    /// This expression's filter
    filter: VarFilter,
    /// This expression's flags
    flags: AtomicTyckFlags,
    /// This expression's form
    form: Form,
}

impl Case {
    /// Construct a new case expression without performing any checks
    pub fn new_unchecked(cases: Arc<[TermId]>, matches: TermId, annot: Option<Annotation>) -> Case {
        let code = Self::compute_code(&cases, &matches, annot.as_ref());
        let filter = Self::compute_filter(&cases, &matches, annot.as_ref());
        let flags = Self::compute_flags(&cases, &matches, annot.as_ref()).into();
        let form = Self::compute_form(&cases, &matches, annot.as_ref());
        Case {
            cases,
            matches,
            annot,
            code,
            filter,
            flags,
            form,
        }
    }

    /// Construct a new case expression with the minium possible type
    pub fn new_minimal(
        cases: Arc<[TermId]>,
        matches: TermId,
        ctx: &mut (impl TyCtxMut + ?Sized),
    ) -> Result<Case, Error> {
        let annot = Self::compute_minimal_ty(&*cases, &matches, ctx)?
            .map(Annotation::from_ty)
            .transpose()?;
        Ok(Self::new_unchecked(cases, matches, annot))
    }

    /// Compute the form of a case statement
    #[inline]
    pub fn compute_form(cases: &[TermId], _matches: &TermId, _annot: Option<&Annotation>) -> Form {
        //TODO: think about this...
        let mut form = Form::BetaEta;
        for case in cases {
            form = form.meet(case.form());
        }
        form.join(Form::Head)
    }

    /// Compute the code of a case statement
    #[inline]
    pub fn compute_code(cases: &[TermId], matches: &TermId, annot: Option<&Annotation>) -> Code {
        let mut hasher = AHasher::new_with_keys(0x194, 0x92561);
        for case in cases {
            case.code().pure().hash(&mut hasher)
        }
        matches.code().pure().hash(&mut hasher);
        let pre = hasher.finish();
        if let Some(annot) = annot {
            annot.hash(&mut hasher);
        }
        let post = hasher.finish();
        Code::new(pre, post)
    }

    /// Compute the filter of a case statement
    #[inline]
    pub fn compute_filter(
        cases: &[TermId],
        matches: &TermId,
        annot: Option<&Annotation>,
    ) -> VarFilter {
        cases
            .get_filter()
            .union(matches.get_filter())
            .union(annot.get_filter())
    }

    /// Compute the flags of a case statement
    #[inline]
    pub fn compute_flags(
        _cases: &[TermId],
        _matches: &TermId,
        _annot: Option<&Annotation>,
    ) -> TyckFlags {
        //TODO: this
        TyckFlags::default()
    }

    /// Compute the minimal type of a case expression
    #[inline]
    pub fn compute_minimal_ty(
        cases: &[TermId],
        matches: &TermId,
        ctx: &mut (impl TyCtxMut + ?Sized),
    ) -> Result<Option<TermId>, Error> {
        let param_ty = if let Some(ty) = Self::compute_minimal_param(matches, cases, ctx)? {
            ty
        } else {
            return Ok(None);
        };
        let result = if let Some(ty) = Self::compute_minimal_result(&param_ty, cases, matches, ctx)?
        {
            ty
        } else {
            return Ok(None);
        };
        let pi = Pi::new(param_ty, result, ctx.cons_ctx()).into_id_with(ctx.cons_ctx());
        Ok(Some(pi))
    }

    /// Compute the minimal parameter type of a case expression
    #[inline]
    pub fn compute_minimal_param(
        matches: &TermId,
        _cases: &[TermId],
        _ctx: &mut (impl TyCtxMut + ?Sized),
    ) -> Result<Option<TermId>, Error> {
        //TODO: fallback...
        Ok(Some(matches.clone()))
    }

    /// Compute the minimal result type of a case expression
    #[inline]
    pub fn compute_minimal_result(
        arg: &TermId,
        cases: &[TermId],
        matches: &TermId,
        ctx: &mut (impl TyCtxMut + ?Sized),
    ) -> Result<Option<TermId>, Error> {
        let (count, overflow) = Self::count_cases(matches)?;
        if cases.len() < count && cases.len() > count + overflow as usize {
            return Err(Error::IndexOutOfBounds);
        }
        if cases.len() != count {
            // Overflow is not implemented...
            return Err(Error::NotImplemented);
        }
        let first = if let Some(first) = cases.get(0).map(Value::annot).flatten() {
            first
        } else {
            return Ok(None);
        };
        let mut curr = first.ty();
        let mut all_compat = true;
        for next in &cases[1..] {
            if let Some(next) = next.ty() {
                if next.is_subtype(&*curr).unwrap_or(false) {
                    continue;
                } else if curr.is_subtype(&*next).unwrap_or(false) {
                    curr = next
                } else {
                    all_compat = false;
                    break;
                }
            } else {
                return Ok(None);
            }
        }
        if all_compat {
            Ok(Some(curr.clone_into_id_with(ctx.cons_ctx())))
        } else {
            let ty_cases: Vec<_> = cases
                .iter()
                .map(|case| case.ty().unwrap().clone_into_id_with(ctx.cons_ctx()))
                .collect();
            let ty_case = Case::new_minimal(ty_cases.into(), matches.clone(), ctx)?;
            let app = App::new_direct(
                ty_case.into_id_with(ctx.cons_ctx()),
                Var::with_ty(0, arg.clone()).into_id_with(ctx.cons_ctx()),
                None,
                ctx,
            )
            .into_id_with(ctx.cons_ctx());
            Ok(Some(app))
        }
    }

    /// Count the number of cases to match, given a set of matches, as well as whether fallback is possible
    #[inline]
    pub fn count_cases(matches: &Term) -> Result<(usize, bool), Error> {
        match matches {
            Term::Bool(_) => Ok((2, false)),
            Term::Enum(enum_) => Ok((enum_.variants().len(), true)),
            _ => Err(Error::CannotMatch),
        }
    }

    /// Create a match for the `ix`th case
    #[inline]
    pub fn make_match(ix: usize, matches: &Term) -> Result<TermId, Error> {
        match matches {
            Term::Bool(_) => match ix {
                0 => Ok(FALSE.clone()),
                1 => Ok(TRUE.clone()),
                _ => Err(Error::IndexOutOfBounds),
            },
            Term::Enum(_enum) => Err(Error::NotImplemented),
            _ => Err(Error::CannotMatch),
        }
    }

    /// Get the index an argument matches, if any
    #[inline]
    pub fn get_match(arg: &Term, matches: &Term) -> Result<Option<usize>, Error> {
        match (arg, matches) {
            (Term::Boolean(b), Term::Bool(_)) => Ok(Some(b.as_bool() as usize)),
            //TODO: enum, etc...
            _ => {
                if let Some(ty) = arg.ty() {
                    if matches.is_subtype(&*ty).unwrap_or(true) {
                        Ok(None)
                    } else {
                        Err(Error::TypeMismatch)
                    }
                } else {
                    Ok(None)
                }
            }
        }
    }

    /// Get this statement's cases
    pub fn cases(&self) -> &Arc<[TermId]> {
        &self.cases
    }

    /// Get this statement's match base
    pub fn matches(&self) -> &TermId {
        &self.matches
    }
}

impl Cons for Case {
    type Consed = Case;

    fn cons(&self, ctx: &mut (impl ConsCtx + ?Sized)) -> Option<Self::Consed> {
        let cases = self.cases.cons(ctx);
        let matches = self.matches.cons(ctx);
        let annot = self.annot.cons(ctx);
        if cases.is_none() && matches.is_none() && annot.is_none() {
            return None;
        }
        let cases = cases.unwrap_or_else(|| self.cases.clone());
        let matches = matches.unwrap_or_else(|| self.matches.clone());
        let annot = annot.unwrap_or_else(|| self.annot.clone());
        Some(Case {
            cases,
            matches,
            annot,
            code: self.code,
            filter: self.filter,
            flags: self.flags.clone(),
            form: self.form,
        })
    }

    #[inline]
    fn to_consed_ty(&self) -> Self::Consed {
        self.clone()
    }
}

impl Substitute for Case {
    type Substituted = Case;

    fn subst_rec(
        &self,
        ctx: &mut (impl SubstCtx + ?Sized),
    ) -> Result<Option<Self::Substituted>, Error> {
        if !ctx.intersects(self.filter, self.code, self.form) {
            return Ok(None);
        }
        let cases = self.cases.subst_rec(ctx)?;
        let matches = self.matches.subst_rec(ctx)?;
        let annot = self.annot.subst_rec(ctx)?;
        if cases.is_none() && matches.is_none() && annot.is_none() {
            return Ok(None);
        }
        let cases = cases.unwrap_or_else(|| self.cases.consed(ctx.cons_ctx()));
        let matches = matches.unwrap_or_else(|| self.matches.consed(ctx.cons_ctx()));
        let annot = annot.unwrap_or_else(|| self.annot.consed(ctx.cons_ctx()));
        Ok(Some(Case::new_unchecked(cases, matches, annot)))
    }

    #[inline]
    fn from_consed(this: Self::Consed, _ctx: &mut (impl ConsCtx + ?Sized)) -> Self::Substituted {
        this
    }
}

impl HasDependencies for Case {
    #[inline]
    fn has_var_dep(&self, ix: u32, equiv: bool) -> bool {
        self.filter.contains(ix)
            && (self.filter.exact()
                || self.cases.has_var_dep(ix, equiv)
                || self.matches.has_var_dep(ix, equiv)
                || self.annot.has_var_dep(ix, equiv))
    }

    #[inline]
    fn has_dep_below(&self, ix: u32, base: u32) -> bool {
        if let Some(contains) = self.filter.contains_below(ix, base) {
            contains
        } else {
            self.cases.has_dep_below(ix, base)
                || self.matches.has_dep_below(ix, base)
                || self.annot.has_dep_below(ix, base)
        }
    }

    #[inline]
    fn get_filter(&self) -> VarFilter {
        self.filter
    }
}

impl Typecheck for Case {
    fn do_local_tyck(&self, ctx: &mut (impl ConsCtx + ?Sized)) -> bool {
        let cow;
        let pi = if let Some(annot) = &self.annot {
            cow = annot.base();
            if let Term::Pi(pi) = &*cow {
                pi
            } else {
                return false;
            }
        } else {
            return false;
        };
        match &*self.matches {
            Term::Bool(_)
                if pi
                    .param_ty()
                    .eq_in(&self.matches, &mut Untyped)
                    .unwrap_or(false) => {}
            //TODO: enum, etc
            _ => return false,
        }
        for (ix, case) in self.cases.iter().enumerate() {
            let ty = if let Some(ty) = case.ty() {
                ty
            } else {
                return false;
            };
            if !ty.is_subtype(&self.matches).unwrap_or(false) {
                return false;
            }
            let case = if let Ok(case) = Self::make_match(ix, &self.matches) {
                case
            } else {
                return false;
            };
            let mut args = smallvec![case];
            let applied = if let Ok(Some(applied)) = pi.apply_ty(
                &mut args,
                &mut SubstVec::new(TrivialCons::with_cons(&mut *ctx)),
            ) {
                applied
            } else {
                return false;
            };
            if !ty.is_subtype(&applied).unwrap_or(false) {
                return false;
            }
        }
        true
    }

    fn do_global_tyck(&self, ctx: &mut (impl TyCtxMut + ?Sized)) -> Option<bool> {
        let mut is_none = false;
        for case in self.cases.iter().chain(std::iter::once(&self.matches)) {
            match case.tyck(ctx) {
                Some(true) => {}
                Some(false) => return Some(false),
                None => is_none = true,
            }
        }
        if is_none {
            None
        } else {
            Some(true)
        }
    }

    #[inline]
    fn do_annot_tyck(&self, ctx: &mut (impl TyCtxMut + ?Sized)) -> Option<bool> {
        self.annot.tyck(ctx)
    }

    fn load_flags(&self) -> TyckFlags {
        self.flags.load_flags()
    }

    fn set_flags(&self, flags: TyckFlags) {
        self.flags.set_flags(flags);
    }
}

impl TermEq for Case {
    fn eq_in(&self, other: &Self, ctx: &mut (impl TermEqCtxMut + ?Sized)) -> Option<bool> {
        let cases = self.cases.eq_in(&other.cases, ctx);
        if let Some(false) = cases {
            return Some(false);
        }
        let matches = self.matches.eq_in(&other.matches, ctx);
        if let Some(false) = matches {
            return Some(false);
        }
        let annot = if ctx.cmp_annot() {
            self.annot.eq_in(&other.annot, ctx)
        } else {
            Some(true)
        };
        if let Some(false) = annot {
            return Some(false);
        }
        Some(cases? && matches? && annot?)
    }
}

impl Value for Case {
    type ValueConsed = Case;

    type ValueSubstituted = Case;

    #[inline]
    fn into_term_direct(self) -> Term {
        Term::Case(self)
    }

    #[inline]
    fn annot(&self) -> Option<AnnotationRef> {
        self.annot.as_ref().map(Annotation::borrow_annot)
    }

    #[inline]
    fn is_local_ty(&self) -> Option<bool> {
        Some(false)
    }

    #[inline]
    fn is_local_universe(&self) -> Option<bool> {
        Some(false)
    }

    #[inline]
    fn is_local_function(&self) -> Option<bool> {
        Some(true)
    }

    #[inline]
    fn is_local_pi(&self) -> Option<bool> {
        Some(false)
    }

    #[inline]
    fn is_root(&self) -> bool {
        //TODO: think about this
        false
    }

    #[inline]
    fn annotate_unchecked(
        &self,
        annot: Annotation,
        _ctx: &mut (impl ConsCtx + ?Sized),
    ) -> Result<Option<Self::ValueConsed>, Error> {
        //TODO: think about this...
        if Some(&annot) == self.annot.as_ref() {
            Ok(None)
        } else {
            Ok(Some(Self::new_unchecked(
                self.cases.clone(),
                self.matches.clone(),
                Some(annot),
            )))
        }
    }

    #[inline]
    fn is_subtype_in(
        &self,
        _other: &Term,
        _ctx: &mut (impl TermEqCtxMut + ?Sized),
    ) -> Option<bool> {
        Some(false)
    }

    #[inline]
    fn code(&self) -> Code {
        self.code
    }

    #[inline]
    fn untyped_code(&self) -> Code {
        Self::compute_code(&self.cases, &self.matches, None)
    }

    #[inline]
    fn eq_term_in(&self, other: &Term, ctx: &mut (impl TermEqCtxMut + ?Sized)) -> Option<bool> {
        match other {
            Term::Case(other) => self.eq_in(other, ctx),
            _ if other.is_local_function() == Some(false) => Some(false),
            _ => None,
        }
    }

    #[inline]
    fn form(&self) -> Form {
        self.form
    }

    #[inline]
    fn coerce(
        &self,
        ty: Option<TermId>,
        ctx: &mut (impl TyCtxMut + ?Sized),
    ) -> Result<Option<Self::ValueConsed>, Error> {
        //TODO: this...
        if let Some(ty) = ty {
            if let Some(annot) = self.annot() {
                if let Some(annot) = annot.coerce_ty(&ty, ctx)? {
                    self.annotate_unchecked(annot, ctx.cons_ctx())
                } else {
                    Ok(None)
                }
            } else {
                self.annotate_unchecked(Annotation::from_ty(ty)?, ctx.cons_ctx())
            }
        } else {
            Ok(None)
        }
    }

    #[inline]
    fn apply(
        &self,
        args: &mut SmallVec<[TermId; 2]>,
        ctx: &mut (impl EvalCtx + ?Sized),
    ) -> Result<Option<TermId>, Error> {
        if let Some(param) = args.pop() {
            if let Some(ix) = Self::get_match(&param, &self.matches)? {
                return Ok(Some(
                    self.cases[ix]
                        .apply(args, ctx)?
                        .unwrap_or_else(|| self.cases[ix].consed(ctx.cons_ctx())),
                ));
            }
        }
        self.subst_id(ctx)
    }
}

impl PartialEq for Case {
    #[inline]
    fn eq(&self, other: &Self) -> bool {
        self.eq_in(other, &mut Structural).unwrap_or(false)
    }
}

impl Eq for Case {}

impl Hash for Case {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.cases.hash(state);
        self.matches.hash(state);
        self.annot.hash(state);
    }
}
