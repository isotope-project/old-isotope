/*!
Simple types consisting of a finite number of variants
*/

use super::*;

/// A type with a finite number of named variants
#[derive(Debug, Clone)]
pub struct Enum {
    /// The variants this type has, as a sorted array
    //TODO: embedded set?
    variants: StringSet,
    /// This type's annotation, if any
    annot: Option<Annotation>,
    /// This type's code
    code: Code,
    /// This type's flags
    flags: AtomicTyckFlags,
    //TODO: label?
}

impl Enum {
    /// Create a new enumeration from an array of variants
    pub fn new(variants: StringSet) -> Enum {
        let code = Self::compute_code(&variants, None);
        let flags = Self::compute_flags(&variants, None).into();
        Enum {
            variants,
            annot: None,
            code,
            flags,
        }
    }

    /// Compute this type's code
    pub fn compute_code(variants: &StringSet, annot: Option<&Annotation>) -> Code {
        let mut hasher = AHasher::new_with_keys(0x563453, 0x831);
        variants.hash(&mut hasher);
        let pre = hasher.finish();
        if let Some(annot) = annot {
            annot.hash(&mut hasher)
        };
        let post = hasher.finish();
        Code::new(pre, post)
    }

    /// Compute this type's flags
    pub fn compute_flags(_variants: &StringSet, _annot: Option<&Annotation>) -> TyckFlags {
        TyckFlags::default()
    }

    /// Get this enum's variants
    pub fn variants(&self) -> &StringSet {
        &self.variants
    }
}

/// A variant of an enumeration
#[derive(Debug, Clone)]
pub struct Variant {
    /// The name of this variant
    name: SmolStr,
    /// This variant's annotation
    annot: Annotation,
    /// This variant's code
    code: Code,
    /// This variant's flags
    flags: AtomicTyckFlags,
}

impl Variant {
    /// Create a new variant given an enum as it's *direct* type
    pub fn new_direct(name: impl Into<SmolStr>, ty: TermId) -> Result<Variant, Error> {
        //TODO: check annot is `Enum`, and check set membership
        let name = name.into();
        let annot = Annotation::from_ty(ty)?;
        let code = Self::compute_code(&*name, &annot);
        let flags = Self::compute_flags(&*name, &annot).into();
        Ok(Variant {
            name,
            annot,
            code,
            flags,
        })
    }

    /// Compute this variant's code
    pub fn compute_code(name: &str, annot: &Annotation) -> Code {
        let mut hasher = AHasher::new_with_keys(0x563453, 0x831);
        name.hash(&mut hasher);
        let pre = hasher.finish();
        annot.hash(&mut hasher);
        let post = hasher.finish();
        Code::new(pre, post)
    }

    /// Compute this variant's flags
    pub fn compute_flags(_name: &str, _annot: &Annotation) -> TyckFlags {
        TyckFlags::default()
    }

    /// Get this variant's name
    pub fn name(&self) -> &str {
        &self.name
    }
}

impl Cons for Enum {
    type Consed = Enum;

    #[inline]
    fn cons(&self, ctx: &mut (impl ConsCtx + ?Sized)) -> Option<Self::Consed> {
        let annot = self.annot.cons(ctx)?;
        Some(Enum {
            variants: self.variants.clone(),
            annot,
            code: self.code,
            flags: self.flags.clone(),
        })
    }

    #[inline]
    fn to_consed_ty(&self) -> Self::Consed {
        self.clone()
    }
}

impl Substitute for Enum {
    type Substituted = Enum;

    #[inline]
    fn subst_rec(
        &self,
        ctx: &mut (impl SubstCtx + ?Sized),
    ) -> Result<Option<Self::Substituted>, Error> {
        let annot = self.annot.subst_rec(ctx)?;
        if let Some(annot) = annot {
            let code = Self::compute_code(&self.variants, annot.as_ref());
            let flags = Self::compute_flags(&self.variants, annot.as_ref()).into();
            Ok(Some(Enum {
                variants: self.variants.clone(),
                annot,
                code,
                flags,
            }))
        } else {
            Ok(None)
        }
    }

    #[inline]
    fn from_consed(this: Self::Consed, _ctx: &mut (impl ConsCtx + ?Sized)) -> Self::Substituted {
        this
    }
}

impl Typecheck for Enum {
    #[inline]
    fn do_local_tyck(&self, _ctx: &mut (impl ConsCtx + ?Sized)) -> bool {
        if let Some(annot) = &self.annot {
            if let Some(universe) = annot.borrow_annot().as_universe() {
                universe >= Universe::set()
            } else {
                false
            }
        } else {
            true
        }
    }

    #[inline]
    fn do_global_tyck(&self, _ctx: &mut (impl TyCtxMut + ?Sized)) -> Option<bool> {
        Some(true)
    }

    #[inline]
    fn do_annot_tyck(&self, ctx: &mut (impl TyCtxMut + ?Sized)) -> Option<bool> {
        self.annot.tyck(ctx)
    }

    #[inline]
    fn load_flags(&self) -> TyckFlags {
        self.flags.load_flags()
    }

    #[inline]
    fn set_flags(&self, flags: TyckFlags) {
        self.flags.set_flags(flags);
    }
}

impl TermEq for Enum {
    fn eq_in(&self, other: &Self, ctx: &mut (impl TermEqCtxMut + ?Sized)) -> Option<bool> {
        if self.variants == other.variants {
            Some(!ctx.cmp_annot() || self.annot.eq_in(&other.annot, ctx)?)
        } else {
            None
        }
    }
}

impl HasDependencies for Enum {
    fn has_var_dep(&self, ix: u32, equiv: bool) -> bool {
        self.annot.has_var_dep(ix, equiv)
    }

    fn has_dep_below(&self, ix: u32, base: u32) -> bool {
        self.annot.has_dep_below(ix, base)
    }

    fn get_filter(&self) -> VarFilter {
        self.annot.get_filter()
    }
}

impl Value for Enum {
    type ValueConsed = Enum;

    type ValueSubstituted = Enum;

    #[inline]
    fn into_term_direct(self) -> Term {
        Term::Enum(self)
    }

    #[inline]
    fn annot(&self) -> Option<AnnotationRef> {
        if let Some(annot) = &self.annot {
            Some(annot.borrow_annot())
        } else {
            Some(AnnotationRef::Universe(Universe::set()))
        }
    }

    #[inline]
    fn is_local_ty(&self) -> Option<bool> {
        Some(true)
    }

    #[inline]
    fn is_local_universe(&self) -> Option<bool> {
        Some(false)
    }

    #[inline]
    fn is_local_function(&self) -> Option<bool> {
        Some(false)
    }

    #[inline]
    fn is_local_pi(&self) -> Option<bool> {
        Some(false)
    }

    #[inline]
    fn is_root(&self) -> bool {
        true
    }

    fn annotate_unchecked(
        &self,
        annot: Annotation,
        _ctx: &mut (impl ConsCtx + ?Sized),
    ) -> Result<Option<Self::ValueConsed>, Error> {
        if Some(&annot) == self.annot.as_ref() {
            return Ok(None);
        }
        let code = Self::compute_code(&self.variants, Some(&annot));
        let flags = Self::compute_flags(&self.variants, Some(&annot)).into();
        Ok(Some(Enum {
            variants: self.variants.clone(),
            annot: Some(annot),
            code,
            flags,
        }))
    }

    #[inline]
    fn is_subtype_in(&self, other: &Term, _ctx: &mut (impl TermEqCtxMut + ?Sized)) -> Option<bool> {
        match other {
            Term::Enum(other) => Some(self.variants.is_subset(&other.variants)),
            _ if other.is_root() || other.is_local_ty() == Some(false) => Some(false),
            _ => None,
        }
    }

    #[inline]
    fn code(&self) -> Code {
        self.code
    }

    #[inline]
    fn untyped_code(&self) -> Code {
        Self::compute_code(&self.variants, None)
    }

    #[inline]
    fn eq_term_in(&self, other: &Term, ctx: &mut (impl TermEqCtxMut + ?Sized)) -> Option<bool> {
        match other {
            Term::Enum(other) => self.eq_in(other, ctx),
            _ if other.is_root() || other.is_local_ty() == Some(false) => Some(false),
            _ => None,
        }
    }

    #[inline]
    fn form(&self) -> Form {
        Form::BetaEta
    }
}

impl Cons for Variant {
    type Consed = Variant;

    #[inline]
    fn cons(&self, ctx: &mut (impl ConsCtx + ?Sized)) -> Option<Self::Consed> {
        let annot = self.annot.cons(ctx)?;
        Some(Variant {
            name: self.name.clone(),
            annot,
            code: self.code,
            flags: self.flags.clone(),
        })
    }

    #[inline]
    fn to_consed_ty(&self) -> Self::Consed {
        self.clone()
    }
}

impl Substitute for Variant {
    type Substituted = Variant;

    #[inline]
    fn subst_rec(
        &self,
        ctx: &mut (impl SubstCtx + ?Sized),
    ) -> Result<Option<Self::Substituted>, Error> {
        let annot = self.annot.subst_rec(ctx)?;
        if let Some(annot) = annot {
            let code = Self::compute_code(&self.name, &annot);
            let flags = Self::compute_flags(&self.name, &annot).into();
            Ok(Some(Variant {
                name: self.name.clone(),
                annot,
                code,
                flags,
            }))
        } else {
            Ok(None)
        }
    }

    #[inline]
    fn from_consed(this: Self::Consed, _ctx: &mut (impl ConsCtx + ?Sized)) -> Self::Substituted {
        this
    }
}

impl Typecheck for Variant {
    fn do_local_tyck(&self, _ctx: &mut (impl ConsCtx + ?Sized)) -> bool {
        // Local type-checking happens at construction time
        true
    }

    fn do_global_tyck(&self, _ctx: &mut (impl TyCtxMut + ?Sized)) -> Option<bool> {
        Some(true)
    }

    fn do_annot_tyck(&self, ctx: &mut (impl TyCtxMut + ?Sized)) -> Option<bool> {
        self.annot.tyck(ctx)
    }

    fn load_flags(&self) -> TyckFlags {
        self.flags.load_flags()
    }

    fn set_flags(&self, flags: TyckFlags) {
        self.flags.set_flags(flags);
    }
}

impl HasDependencies for Variant {
    #[inline]
    fn has_var_dep(&self, ix: u32, equiv: bool) -> bool {
        self.annot.has_var_dep(ix, equiv)
    }

    #[inline]
    fn has_dep_below(&self, ix: u32, base: u32) -> bool {
        self.annot.has_dep_below(ix, base)
    }

    #[inline]
    fn get_filter(&self) -> VarFilter {
        self.annot.get_filter()
    }
}

impl TermEq for Variant {
    #[inline]
    fn eq_in(&self, other: &Self, ctx: &mut (impl TermEqCtxMut + ?Sized)) -> Option<bool> {
        if self.name == other.name {
            Some(!ctx.cmp_annot() || self.annot.eq_in(&other.annot, ctx)?)
        } else {
            Some(false)
        }
    }
}

impl Value for Variant {
    type ValueConsed = Variant;

    type ValueSubstituted = Variant;

    #[inline]
    fn into_term_direct(self) -> Term {
        Term::Variant(self)
    }

    #[inline]
    fn annot(&self) -> Option<AnnotationRef> {
        Some(self.annot.borrow_annot())
    }

    #[inline]
    fn is_local_ty(&self) -> Option<bool> {
        Some(false)
    }

    #[inline]
    fn is_local_universe(&self) -> Option<bool> {
        Some(false)
    }

    #[inline]
    fn is_local_function(&self) -> Option<bool> {
        Some(false)
    }

    #[inline]
    fn is_local_pi(&self) -> Option<bool> {
        Some(false)
    }

    #[inline]
    fn is_root(&self) -> bool {
        true
    }

    fn annotate_unchecked(
        &self,
        annot: Annotation,
        _ctx: &mut (impl ConsCtx + ?Sized),
    ) -> Result<Option<Self::ValueConsed>, Error> {
        if annot == self.annot {
            return Ok(None);
        }
        let code = Self::compute_code(&*self.name, &annot);
        let flags = Self::compute_flags(&*self.name, &annot).into();
        Ok(Some(Variant {
            name: self.name.clone(),
            annot,
            code,
            flags,
        }))
    }

    #[inline]
    fn is_subtype_in(
        &self,
        _other: &Term,
        _ctx: &mut (impl TermEqCtxMut + ?Sized),
    ) -> Option<bool> {
        Some(false)
    }

    #[inline]
    fn code(&self) -> Code {
        self.code
    }

    #[inline]
    fn untyped_code(&self) -> Code {
        Code::new(self.code.pure() as u64, (self.code.pure() as u64) << 32)
    }

    #[inline]
    fn eq_term_in(&self, other: &Term, ctx: &mut (impl TermEqCtxMut + ?Sized)) -> Option<bool> {
        match other {
            Term::Variant(other) => self.eq_in(other, ctx),
            _ if other.is_root() || other.is_local_ty() == Some(true) => Some(false),
            _ => None,
        }
    }

    #[inline]
    fn form(&self) -> Form {
        Form::BetaEta
    }
}

impl PartialEq for Enum {
    #[inline]
    fn eq(&self, other: &Self) -> bool {
        self.eq_in(other, &mut Structural).unwrap_or(false)
    }
}

impl PartialEq for Variant {
    #[inline]
    fn eq(&self, other: &Self) -> bool {
        self.eq_in(other, &mut Structural).unwrap_or(false)
    }
}

impl Eq for Enum {}

impl Eq for Variant {}

impl Hash for Enum {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.variants.hash(state);
        self.annot.hash(state);
    }
}

impl Hash for Variant {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.name.hash(state);
        self.annot.hash(state);
    }
}
