/*!
Dynamic terms, which allow run-time extension of `isotope`, mainly for testing and debugging purposes
*/
use super::*;

/// The interface which must be implemented by dynamic terms
///
/// Rather than implement this manually, it is usually preferable to implement
/// `Value`, which will implement this trait automatically assuming the type
/// also satisfies `Debug`, `Hash`, `PartialEq`, and `'static`.
pub trait DynamicValue: Debug + Any + DynHash {
    /// Cons this term within a given context. Return `None` if already consed.
    fn dyn_cons(&self, ctx: &mut dyn ConsCtx) -> Option<TermId>;
    /// Get the type annotation of this term
    fn dyn_ty(&self) -> Option<AnnotationRef>;
    /// Get the index of this term in an `isotope` program graph, if any
    fn dyn_ix(&self) -> Option<NodeIx>;
    /// Get the hash-code of this term
    fn dyn_code(&self) -> Code;
    /// Get whether this term is a type
    fn dyn_is_local_ty(&self) -> Option<bool>;
    /// Get whether this term is a universe
    fn dyn_is_local_universe(&self) -> Option<bool>;
    /// Get whether this term is in "root form"
    fn dyn_is_root(&self) -> bool;
    /// Get whether this term is a subtype of another term in a given context
    fn dyn_is_subtype_in(&self, other: &Term, ctx: &mut dyn TermEqCtxMut) -> Option<bool>;
    /// Get whether this term has a universe *in all contexts*
    fn dyn_universe(&self) -> Option<Universe>;
    /// Get the untyped hash-code of this term
    fn dyn_untyped_code(&self) -> Code;
    /// Get the variable filter of this term
    fn dyn_filter(&self) -> VarFilter;
    /// Shift this term's variables with index `>= base` up by `n` in a given context
    fn dyn_shift(&self, n: i32, base: u32, ctx: &mut dyn ConsCtx) -> Result<Option<TermId>, Error>;
    /// Whether this term has a given variable dependency
    fn dyn_has_var_dep(&self, ix: u32, equiv: bool) -> bool;
    /// Whether this term has a given variable dependency below `ix` and above `base`
    fn dyn_has_dep_below(&self, ix: u32, base: u32) -> bool;
    /// Load this term's flags
    fn dyn_load_flags(&self) -> TyckFlags;
    /// Set this term's flags
    fn dyn_set_flags(&self, flags: TyckFlags);
    /// Compare this term to another
    #[inline]
    fn dyn_eq_term_in(&self, other: &Term, ctx: &mut dyn TermEqCtxMut) -> Option<bool> {
        match other {
            Term::Dynamic(other) => self.dyn_eq_in(&*other.0, ctx),
            _ => Some(false),
        }
    }
    /// Compare this term to a term ID
    #[inline]
    fn dyn_eq_id_in(&self, other: &TermId, ctx: &mut dyn TermEqCtxMut) -> Option<bool> {
        self.dyn_eq_term_in(&**other, ctx)
    }
    /// Compare this term to another dynamic term
    fn dyn_eq_in(&self, other: &dyn DynamicValue, ctx: &mut dyn TermEqCtxMut) -> Option<bool>;
    /// Convert this term to an `Any`
    fn as_any(&self) -> &dyn Any;
}

impl<V: Value + PartialEq + Debug + Hash + 'static> DynamicValue for V {
    fn dyn_cons(&self, ctx: &mut dyn ConsCtx) -> Option<TermId> {
        self.cons_id(ctx)
    }

    fn dyn_ty(&self) -> Option<AnnotationRef> {
        self.annot()
    }

    fn dyn_ix(&self) -> Option<NodeIx> {
        self.id()
    }

    fn dyn_code(&self) -> Code {
        self.code()
    }

    fn dyn_is_local_ty(&self) -> Option<bool> {
        self.is_local_ty()
    }

    fn dyn_is_local_universe(&self) -> Option<bool> {
        self.is_local_universe()
    }

    fn dyn_is_root(&self) -> bool {
        self.is_root()
    }

    fn dyn_is_subtype_in(&self, other: &Term, ctx: &mut dyn TermEqCtxMut) -> Option<bool> {
        self.is_subtype_in(other, ctx)
    }

    fn dyn_universe(&self) -> Option<Universe> {
        self.universe()
    }

    fn dyn_untyped_code(&self) -> Code {
        self.untyped_code()
    }

    fn dyn_filter(&self) -> VarFilter {
        self.get_filter()
    }

    fn dyn_shift(&self, n: i32, base: u32, ctx: &mut dyn ConsCtx) -> Result<Option<TermId>, Error> {
        self.shift_id(n, base, ctx)
    }

    fn dyn_has_var_dep(&self, ix: u32, equiv: bool) -> bool {
        self.has_var_dep(ix, equiv)
    }

    fn dyn_has_dep_below(&self, ix: u32, base: u32) -> bool {
        self.has_dep_below(ix, base)
    }

    fn dyn_load_flags(&self) -> TyckFlags {
        self.load_flags()
    }

    fn dyn_set_flags(&self, flags: TyckFlags) {
        self.set_flags(flags)
    }

    fn dyn_eq_in(&self, other: &dyn DynamicValue, ctx: &mut dyn TermEqCtxMut) -> Option<bool> {
        if let Some(other) = other.as_any().downcast_ref::<V>() {
            self.eq_in(other, ctx)
        } else {
            Some(false)
        }
    }

    fn as_any(&self) -> &dyn Any {
        self
    }
}

/// A dynamic version of the hash trait
///
/// Implemented automatically for all `Hash` types.
pub trait DynHash {
    /// Hash this using a given hasher
    fn dyn_hash(&self, state: &mut dyn Hasher);
}

impl<H: Hash + ?Sized> DynHash for H {
    fn dyn_hash(&self, mut state: &mut dyn Hasher) {
        self.hash(&mut state);
    }
}

/// A dynamic term
#[derive(Debug, Clone)]
pub struct DynamicTerm(pub Arc<dyn DynamicValue + Send + Sync>);

impl Deref for DynamicTerm {
    type Target = dyn DynamicValue + Send + Sync;

    #[inline]
    fn deref(&self) -> &Self::Target {
        &*self.0
    }
}

impl Cons for DynamicTerm {
    type Consed = TermId;

    #[inline]
    fn cons(&self, ctx: &mut (impl ConsCtx + ?Sized)) -> Option<Self::Consed> {
        self.cons_id(ctx)
    }

    #[inline]
    fn to_consed_ty(&self) -> Self::Consed {
        //TODO: think about this...
        self.into_id_direct()
    }
}

impl Substitute for DynamicTerm {
    type Substituted = TermId;

    #[inline]
    fn subst_rec(&self, ctx: &mut (impl SubstCtx + ?Sized)) -> Result<Option<TermId>, Error> {
        self.subst_id(ctx)
    }

    #[inline]
    fn from_consed(this: Self::Consed, _ctx: &mut (impl ConsCtx + ?Sized)) -> Self::Substituted {
        this
    }
}

impl Typecheck for DynamicTerm {
    #[inline]
    fn do_local_tyck(&self, _ctx: &mut (impl ConsCtx + ?Sized)) -> bool {
        //TODO: this
        false
    }

    #[inline]
    fn do_global_tyck(&self, _ctx: &mut (impl TyCtxMut + ?Sized)) -> Option<bool> {
        //TODO: this
        Some(false)
    }

    #[inline]
    fn do_annot_tyck(&self, _ctx: &mut (impl TyCtxMut + ?Sized)) -> Option<bool> {
        //TODO: this
        Some(false)
    }

    #[inline]
    fn tyck(&self, _ctx: &mut (impl TyCtxMut + ?Sized)) -> Option<bool> {
        //TODO: this
        Some(false)
    }

    #[inline]
    fn load_flags(&self) -> TyckFlags {
        self.0.dyn_load_flags()
    }

    #[inline]
    fn set_flags(&self, flags: TyckFlags) {
        self.0.dyn_set_flags(flags)
    }
}

impl HasDependencies for DynamicTerm {
    #[inline]
    fn has_var_dep(&self, ix: u32, equiv: bool) -> bool {
        self.0.dyn_has_var_dep(ix, equiv)
    }

    #[inline]
    fn has_dep_below(&self, ix: u32, base: u32) -> bool {
        self.0.dyn_has_dep_below(ix, base)
    }

    #[inline]
    fn get_filter(&self) -> VarFilter {
        self.0.dyn_filter()
    }
}

impl TermEq for DynamicTerm {
    #[inline]
    fn eq_in(&self, other: &Self, ctx: &mut (impl TermEqCtxMut + ?Sized)) -> Option<bool> {
        self.0.dyn_eq_in(other, ctx.as_dyn_eq_mut())
    }
}

impl Value for DynamicTerm {
    type ValueConsed = TermId;
    type ValueSubstituted = TermId;

    #[inline]
    fn into_term_direct(self) -> Term {
        Term::Dynamic(self)
    }

    #[inline]
    fn annot(&self) -> Option<AnnotationRef> {
        self.0.dyn_ty()
    }

    #[inline]
    fn id(&self) -> Option<NodeIx> {
        self.0.dyn_ix()
    }

    #[inline]
    fn is_local_ty(&self) -> Option<bool> {
        self.0.dyn_is_local_ty()
    }

    #[inline]
    fn is_local_universe(&self) -> Option<bool> {
        self.0.dyn_is_local_universe()
    }

    #[inline]
    fn is_local_function(&self) -> Option<bool> {
        //TODO: this
        None
    }

    #[inline]
    fn is_local_pi(&self) -> Option<bool> {
        //TODO: this
        None
    }

    #[inline]
    fn is_root(&self) -> bool {
        self.0.dyn_is_root()
    }

    #[inline]
    fn coerce(
        &self,
        _ty: Option<TermId>,
        _ctx: &mut (impl TyCtxMut + ?Sized),
    ) -> Result<Option<TermId>, Error> {
        Err(Error::NotImplemented)
    }

    #[inline]
    fn annotate_unchecked(
        &self,
        _annot: Annotation,
        _ctx: &mut (impl ConsCtx + ?Sized),
    ) -> Result<Option<TermId>, Error> {
        Err(Error::NotImplemented)
    }

    #[inline]
    fn is_subtype_in(&self, other: &Term, ctx: &mut (impl TermEqCtxMut + ?Sized)) -> Option<bool> {
        self.0.dyn_is_subtype_in(other, ctx.as_dyn_eq_mut())
    }

    #[inline]
    fn universe(&self) -> Option<Universe> {
        self.0.dyn_universe()
    }

    #[inline]
    fn code(&self) -> Code {
        self.0.dyn_code()
    }

    #[inline]
    fn untyped_code(&self) -> Code {
        self.0.dyn_untyped_code()
    }

    #[inline]
    fn subst_id(&self, _ctx: &mut (impl SubstCtx + ?Sized)) -> Result<Option<TermId>, Error> {
        //TODO: this
        Err(Error::NotImplemented)
    }

    #[inline]
    fn shift_id(
        &self,
        n: i32,
        base: u32,
        ctx: &mut (impl ConsCtx + ?Sized),
    ) -> Result<Option<TermId>, Error> {
        self.0.dyn_shift(n, base, ctx.as_dyn_cons_mut())
    }

    #[inline]
    fn eq_term_in(&self, other: &Term, ctx: &mut (impl TermEqCtxMut + ?Sized)) -> Option<bool> {
        self.0.dyn_eq_term_in(other, ctx.as_dyn_eq_mut())
    }

    #[inline]
    fn eq_id_in(&self, other: &TermId, ctx: &mut (impl TermEqCtxMut + ?Sized)) -> Option<bool> {
        self.0.dyn_eq_id_in(other, ctx.as_dyn_eq_mut())
    }

    #[inline]
    fn cons_id(&self, ctx: &mut (impl ConsCtx + ?Sized)) -> Option<TermId> {
        self.0.dyn_cons(ctx.as_dyn_cons_mut())
    }

    #[inline]
    fn apply(
        &self,
        _args: &mut SmallVec<[TermId; 2]>,
        _ctx: &mut (impl EvalCtx + ?Sized),
    ) -> Result<Option<TermId>, Error> {
        Err(Error::NotImplemented)
    }

    //TODO: this...
    #[inline]
    fn form(&self) -> Form {
        Form::BetaEta
    }
}

impl PartialEq for DynamicTerm {
    fn eq(&self, other: &Self) -> bool {
        Arc::ptr_eq(&self.0, &other.0)
            || self
                .0
                .dyn_eq_in(&*other.0, &mut Structural)
                .unwrap_or(false)
    }
}

impl PartialEq<TermId> for DynamicTerm {
    fn eq(&self, other: &TermId) -> bool {
        self.0.dyn_eq_id_in(other, &mut Structural).unwrap_or(false)
    }
}

impl PartialEq<Term> for DynamicTerm {
    fn eq(&self, other: &Term) -> bool {
        self.0
            .dyn_eq_term_in(other, &mut Structural)
            .unwrap_or(false)
    }
}

impl Eq for DynamicTerm {}

impl Hash for DynamicTerm {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.0.dyn_hash(state)
    }
}

/// A debug value with an arbitrary code, type, and name, which dynamically compares by pointer
#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub struct DebugValue {
    /// The name of this debug value
    pub name: String,
    /// The code of this debug value
    pub code: Code,
    /// This value's flags
    pub flags: TyckFlags,
}

impl DebugValue {
    /// Create a new debug value with a given code
    #[inline]
    pub fn code(name: impl Into<String>, code: Code) -> DebugValue {
        DebugValue {
            name: name.into(),
            code,
            flags: TyckFlags::default(),
        }
    }
    /// Create a new debug value with a given name
    #[inline]
    pub fn name(name: impl Into<String>) -> DebugValue {
        let name = name.into();
        let code = Code(fxhash::hash64(&name));
        DebugValue {
            name,
            code,
            flags: TyckFlags::default(),
        }
    }
    /// Create a new debug value `TermId` with a given code
    #[inline]
    pub fn code_id(name: impl Into<String>, code: Code) -> TermId {
        Self::code(name, code).into_id()
    }
    /// Create a new debug value `TermId` with a given name
    #[inline]
    pub fn name_id(name: impl Into<String>) -> TermId {
        Self::name(name).into_id()
    }
    /// Convert this `DebugValue` to a `DynamicTerm`
    #[inline]
    pub fn into_dyn_term(self) -> DynamicTerm {
        DynamicTerm(Arc::new(self))
    }
    /// Convert this `DebugValue` to a `Term`
    #[inline]
    pub fn into_term(self) -> Term {
        Term::Dynamic(self.into_dyn_term())
    }
    /// Convert this `DebugValue` to a `TermId`
    #[inline]
    pub fn into_id(self) -> TermId {
        self.into_dyn_term().into_id_direct()
    }
}

impl Cons for DebugValue {
    type Consed = DebugValue;

    fn cons(&self, _ctx: &mut (impl ConsCtx + ?Sized)) -> Option<Self::Consed> {
        None
    }

    fn to_consed_ty(&self) -> Self::Consed {
        self.clone()
    }
}

impl Substitute for DebugValue {
    type Substituted = DebugValue;

    fn subst_rec(&self, _ctx: &mut (impl SubstCtx + ?Sized)) -> Result<Option<DebugValue>, Error> {
        Ok(None)
    }

    #[inline]
    fn from_consed(this: Self::Consed, _ctx: &mut (impl ConsCtx + ?Sized)) -> Self::Substituted {
        this
    }
}

impl Typecheck for DebugValue {
    fn do_local_tyck(&self, _ctx: &mut (impl ConsCtx + ?Sized)) -> bool {
        false
    }

    fn do_global_tyck(&self, _ctx: &mut (impl TyCtxMut + ?Sized)) -> Option<bool> {
        Some(false)
    }

    fn do_annot_tyck(&self, _ctx: &mut (impl TyCtxMut + ?Sized)) -> Option<bool> {
        Some(false)
    }

    fn tyck(&self, _ctx: &mut (impl TyCtxMut + ?Sized)) -> Option<bool> {
        Some(false)
    }

    fn load_flags(&self) -> TyckFlags {
        self.flags
    }

    fn set_flags(&self, _flags: TyckFlags) {}
}

impl HasDependencies for DebugValue {
    fn has_var_dep(&self, _ix: u32, _equiv: bool) -> bool {
        false
    }

    fn has_dep_below(&self, _ix: u32, _base: u32) -> bool {
        false
    }

    fn get_filter(&self) -> VarFilter {
        VarFilter::EMPTY
    }
}

impl TermEq for DebugValue {
    fn eq_in(&self, other: &Self, _ctx: &mut (impl TermEqCtxMut + ?Sized)) -> Option<bool> {
        if self.name == other.name {
            Some(true)
        } else {
            None
        }
    }
}

impl Value for DebugValue {
    type ValueConsed = DebugValue;

    type ValueSubstituted = DebugValue;

    fn into_term_direct(self) -> Term {
        Term::Dynamic(self.into_dyn_term())
    }

    fn annot(&self) -> Option<AnnotationRef> {
        None
    }

    fn id(&self) -> Option<NodeIx> {
        None
    }

    fn is_local_ty(&self) -> Option<bool> {
        Some(false)
    }

    fn is_local_universe(&self) -> Option<bool> {
        Some(false)
    }

    fn is_local_function(&self) -> Option<bool> {
        Some(false)
    }

    fn is_local_pi(&self) -> Option<bool> {
        Some(false)
    }

    fn is_root(&self) -> bool {
        true
    }

    fn coerce(
        &self,
        _ty: Option<TermId>,
        _ctx: &mut (impl TyCtxMut + ?Sized),
    ) -> Result<Option<DebugValue>, Error> {
        Err(Error::CoerceUntyped)
    }

    fn annotate_unchecked(
        &self,
        _annot: Annotation,
        _ctx: &mut (impl ConsCtx + ?Sized),
    ) -> Result<Option<DebugValue>, Error> {
        Err(Error::CoerceUntyped)
    }

    fn is_subtype_in(&self, other: &Term, ctx: &mut (impl TermEqCtxMut + ?Sized)) -> Option<bool> {
        self.eq_term_in(other, ctx)
    }

    fn universe(&self) -> Option<Universe> {
        None
    }

    fn code(&self) -> Code {
        self.code
    }

    fn untyped_code(&self) -> Code {
        self.code
    }

    fn eq_term_in(&self, other: &Term, ctx: &mut (impl TermEqCtxMut + ?Sized)) -> Option<bool> {
        match other {
            Term::Dynamic(other) => self.dyn_eq_in(other, ctx.as_dyn_eq_mut()),
            _ => Some(false),
        }
    }

    fn apply(
        &self,
        _args: &mut SmallVec<[TermId; 2]>,
        _ctx: &mut (impl EvalCtx + ?Sized),
    ) -> Result<Option<TermId>, Error> {
        Ok(None)
    }

    fn form(&self) -> Form {
        Form::BetaEta
    }
}

impl PartialEq<Term> for DebugValue {
    fn eq(&self, other: &Term) -> bool {
        self.eq_term_in(other, &mut Structural).unwrap_or(false)
    }
}

impl PartialEq<TermId> for DebugValue {
    fn eq(&self, other: &TermId) -> bool {
        self.eq(&**other)
    }
}
