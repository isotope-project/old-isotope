/*!
Implementations for `TermId` and `Term`, to avoid cluttering `mod.rs`
*/

use super::*;
use std::sync::atomic::AtomicU64;

/// The total number of hash collisions observed
pub(super) static TOTAL_HASH_COLLISIONS: AtomicU64 = AtomicU64::new(0);

impl Cons for TermId {
    type Consed = TermId;

    #[inline]
    fn cons(&self, ctx: &mut (impl ConsCtx + ?Sized)) -> Option<Self::Consed> {
        self.cons_id(ctx)
    }

    #[inline]
    fn to_consed_ty(&self) -> Self::Consed {
        self.clone()
    }
}

impl Substitute for TermId {
    type Substituted = TermId;

    fn subst_rec(&self, ctx: &mut (impl SubstCtx + ?Sized)) -> Result<Option<TermId>, Error> {
        self.0.subst_rec(ctx)
    }

    #[inline]
    fn from_consed(this: Self::Consed, _ctx: &mut (impl ConsCtx + ?Sized)) -> Self::Substituted {
        this
    }
}

impl HasDependencies for TermId {
    #[inline]
    fn has_var_dep(&self, ix: u32, equiv: bool) -> bool {
        self.0.has_var_dep(ix, equiv)
    }

    #[inline]
    fn has_dep_below(&self, ix: u32, base: u32) -> bool {
        self.0.has_dep_below(ix, base)
    }

    #[inline]
    fn get_filter(&self) -> VarFilter {
        self.0.get_filter()
    }
}

impl Typecheck for TermId {
    #[inline]
    fn do_local_tyck(&self, ctx: &mut (impl ConsCtx + ?Sized)) -> bool {
        self.0.do_local_tyck(ctx)
    }

    #[inline]
    fn do_global_tyck(&self, ctx: &mut (impl TyCtxMut + ?Sized)) -> Option<bool> {
        self.0.do_global_tyck(ctx)
    }

    #[inline]
    fn do_annot_tyck(&self, ctx: &mut (impl TyCtxMut + ?Sized)) -> Option<bool> {
        self.0.do_annot_tyck(ctx)
    }

    #[inline]
    fn local_tyck(&self, ctx: &mut (impl ConsCtx + ?Sized)) -> bool {
        self.0.local_tyck(ctx)
    }

    #[inline]
    fn global_tyck(&self, ctx: &mut (impl TyCtxMut + ?Sized)) -> Option<bool> {
        self.0.global_tyck(ctx)
    }

    #[inline]
    fn annot_tyck(&self, ctx: &mut (impl TyCtxMut + ?Sized)) -> Option<bool> {
        self.0.annot_tyck(ctx)
    }

    #[inline]
    fn tyck(&self, ctx: &mut (impl TyCtxMut + ?Sized)) -> Option<bool> {
        self.0.tyck(ctx)
    }

    #[inline]
    fn load_flags(&self) -> TyckFlags {
        self.0.load_flags()
    }

    #[inline]
    fn set_flags(&self, flags: TyckFlags) {
        self.0.set_flags(flags)
    }

    #[inline]
    fn tyck_var(&self, ctx: &mut (impl TyCtxMut + ConsCtx + ?Sized)) -> Option<bool> {
        self.0.tyck_var(ctx)
    }

    #[inline]
    fn maybe_tyck(&self) -> bool {
        self.0.maybe_tyck()
    }
}

impl TermEq for TermId {
    #[inline]
    fn eq_in(&self, other: &Self, ctx: &mut (impl TermEqCtxMut + ?Sized)) -> Option<bool> {
        self.eq_id_in(other, ctx)
    }
}

impl Value for TermId {
    type ValueConsed = TermId;
    type ValueSubstituted = TermId;

    #[inline]
    fn into_term_direct(self) -> Term {
        match Arc::try_unwrap(self.0) {
            Ok(term) => term,
            Err(arc) => (*arc).clone(),
        }
    }

    #[inline]
    fn annot(&self) -> Option<AnnotationRef> {
        self.0.annot()
    }

    #[inline]
    fn id(&self) -> Option<NodeIx> {
        self.0.id()
    }

    #[inline]
    fn is_local_ty(&self) -> Option<bool> {
        self.0.is_local_ty()
    }

    #[inline]
    fn is_local_universe(&self) -> Option<bool> {
        self.0.is_local_universe()
    }

    #[inline]
    fn is_local_function(&self) -> Option<bool> {
        self.0.is_local_function()
    }

    #[inline]
    fn is_local_pi(&self) -> Option<bool> {
        self.0.is_local_pi()
    }

    #[inline]
    fn is_root(&self) -> bool {
        (*self.0).is_root()
    }

    #[inline]
    fn coerce(
        &self,
        ty: Option<TermId>,
        ctx: &mut (impl TyCtxMut + ?Sized),
    ) -> Result<Option<TermId>, Error> {
        self.0.coerce_id(ty, ctx)
    }

    #[inline]
    fn coerce_id(
        &self,
        ty: Option<TermId>,
        ctx: &mut (impl TyCtxMut + ?Sized),
    ) -> Result<Option<TermId>, Error> {
        self.0.coerce_id(ty, ctx)
    }

    #[inline]
    fn annotate_unchecked(
        &self,
        annot: Annotation,
        ctx: &mut (impl ConsCtx + ?Sized),
    ) -> Result<Option<TermId>, Error> {
        self.0.annotate_unchecked_id(annot, ctx)
    }

    #[inline]
    fn annotate_unchecked_id(
        &self,
        annot: Annotation,
        ctx: &mut (impl ConsCtx + ?Sized),
    ) -> Result<Option<TermId>, Error> {
        self.0.annotate_unchecked_id(annot, ctx)
    }

    #[inline]
    fn is_const(&self) -> bool {
        self.0.is_const()
    }

    #[inline]
    fn is_subtype_in(&self, other: &Term, ctx: &mut (impl TermEqCtxMut + ?Sized)) -> Option<bool> {
        self.0.is_subtype_in(other, ctx)
    }

    #[inline]
    fn universe(&self) -> Option<Universe> {
        self.0.universe()
    }

    #[inline]
    fn code(&self) -> Code {
        self.0.code()
    }

    #[inline]
    fn untyped_code(&self) -> Code {
        self.0.untyped_code()
    }

    #[inline]
    fn subst_rec_id(&self, ctx: &mut (impl SubstCtx + ?Sized)) -> Result<Option<TermId>, Error> {
        self.0.subst_rec_id(ctx)
    }

    #[inline]
    fn subst_id(&self, ctx: &mut (impl SubstCtx + ?Sized)) -> Result<Option<TermId>, Error> {
        if !ctx.intersects(self.get_filter(), self.code(), self.form()) {
            return Ok(None);
        }
        if let Some(cached) = ctx.try_subst(self) {
            return cached;
        }
        let result = (*self.0).subst_rec_id(ctx)?;
        //TODO: error caching?
        ctx.cache(self, result.as_ref())?;
        Ok(result)
    }

    #[inline]
    fn eq_term_in(&self, other: &Term, ctx: &mut (impl TermEqCtxMut + ?Sized)) -> Option<bool> {
        self.0.eq_term_in(other, ctx)
    }

    #[inline]
    fn eq_id_in(&self, other: &TermId, ctx: &mut (impl TermEqCtxMut + ?Sized)) -> Option<bool> {
        let result = (*self.0).eq_id_in(other, ctx);
        if let Some(true) = result {
            ctx.cache_eq(self, other);
        }
        result
    }

    #[inline]
    fn cons_id(&self, ctx: &mut (impl ConsCtx + ?Sized)) -> Option<TermId> {
        match ctx.try_cons(self) {
            Ok(Some(consed)) => {
                // Flag exchange
                consed.set_flags(self.load_flags());
                self.set_flags(consed.load_flags());
                Some(consed)
            }
            Ok(None) => None,
            // Note: we avoid calling `cons_id` to avoid a spurious call to `uncons`, since we already call `try_cons` beforehand.
            Err(_) => match self.0.cons(ctx) {
                Some(consed) => Some(consed.into_shallow_cons(ctx)),
                None => ctx.shallow_cons(self),
            },
        }
    }

    #[inline]
    fn into_id_with(self, ctx: &mut (impl ConsCtx + ?Sized)) -> TermId {
        self.cons_id(ctx).unwrap_or(self)
    }

    #[inline]
    fn apply(
        &self,
        args: &mut SmallVec<[TermId; 2]>,
        ctx: &mut (impl EvalCtx + ?Sized),
    ) -> Result<Option<TermId>, Error> {
        self.0.apply(args, ctx)
    }

    #[inline]
    fn apply_ty(
        &self,
        args: &mut SmallVec<[TermId; 2]>,
        ctx: &mut (impl EvalCtx + ?Sized),
    ) -> Result<Option<TermId>, Error> {
        self.0.apply_ty(args, ctx)
    }

    #[inline]
    fn into_id_direct(self) -> TermId {
        self
    }

    #[inline]
    fn clone_into_id_direct(&self) -> TermId {
        self.clone()
    }

    #[inline]
    fn clone_into_id_with(&self, ctx: &mut (impl ConsCtx + ?Sized)) -> TermId {
        self.cons_id(ctx).unwrap_or_else(|| self.clone())
    }

    #[inline]
    fn clone_into_term_direct(&self) -> Term {
        (*self.0).clone()
    }

    #[inline]
    fn into_shallow_cons(self, ctx: &mut (impl ConsCtx + ?Sized)) -> TermId {
        ctx.shallow_cons(&self).unwrap_or(self)
    }

    #[inline]
    fn form(&self) -> Form {
        self.0.form()
    }
}

impl Cons for Term {
    type Consed = Term;

    #[inline]
    fn cons(&self, ctx: &mut (impl ConsCtx + ?Sized)) -> Option<Self> {
        for_term!(self; t => t.cons(ctx).map(Value::into_term_direct))
    }

    #[inline]
    fn to_consed_ty(&self) -> Self::Consed {
        self.clone()
    }
}

impl Substitute for Term {
    type Substituted = TermId;

    #[inline]
    fn subst_rec(&self, ctx: &mut (impl SubstCtx + ?Sized)) -> Result<Option<TermId>, Error> {
        for_term!(self; t => t.subst_rec_id(ctx))
    }

    #[inline]
    fn from_consed(this: Self::Consed, ctx: &mut (impl ConsCtx + ?Sized)) -> Self::Substituted {
        this.into_id_with(ctx)
    }
}

impl Typecheck for Term {
    fn do_local_tyck(&self, ctx: &mut (impl ConsCtx + ?Sized)) -> bool {
        for_term!(self; t => t.do_local_tyck(ctx))
    }

    fn do_global_tyck(&self, ctx: &mut (impl TyCtxMut + ?Sized)) -> Option<bool> {
        for_term!(self; t => t.do_global_tyck(ctx))
    }

    #[inline]
    fn do_annot_tyck(&self, ctx: &mut (impl TyCtxMut + ?Sized)) -> Option<bool> {
        for_term!(self; t => t.do_annot_tyck(ctx))
    }

    #[inline]
    fn tyck(&self, ctx: &mut (impl TyCtxMut + ?Sized)) -> Option<bool> {
        for_term!(self; t => t.tyck(ctx))
    }

    #[inline]
    fn load_flags(&self) -> TyckFlags {
        for_term!(self; t => t.load_flags())
    }

    #[inline]
    fn set_flags(&self, flags: TyckFlags) {
        for_term!(self; t => t.set_flags(flags))
    }
}

impl HasDependencies for Term {
    #[inline]
    fn has_var_dep(&self, ix: u32, equiv: bool) -> bool {
        for_term!(self; t => t.has_var_dep(ix, equiv))
    }

    #[inline]
    fn get_filter(&self) -> VarFilter {
        for_term!(self; t => t.get_filter())
    }

    #[inline]
    fn fvb(&self) -> u32 {
        for_term!(self; t => t.fvb())
    }

    #[inline]
    fn has_dep_below(&self, ix: u32, base: u32) -> bool {
        for_term!(self; t => t.has_dep_below(ix, base))
    }
}

impl TermEq for Term {
    #[inline]
    fn eq_in(&self, other: &Self, ctx: &mut (impl TermEqCtxMut + ?Sized)) -> Option<bool> {
        self.eq_term_in(other, ctx)
    }
}

impl Value for Term {
    type ValueConsed = Term;

    type ValueSubstituted = TermId;

    #[inline]
    fn into_term_direct(self) -> Term {
        self
    }

    #[inline]
    fn annot(&self) -> Option<AnnotationRef> {
        for_term!(self; t => t.annot())
    }

    fn id(&self) -> Option<NodeIx> {
        for_term!(self; t => t.id())
    }

    #[inline]
    fn is_local_ty(&self) -> Option<bool> {
        for_term!(self; t => t.is_local_ty())
    }

    #[inline]
    fn is_local_universe(&self) -> Option<bool> {
        for_term!(self; t => t.is_local_universe())
    }

    #[inline]
    fn is_local_function(&self) -> Option<bool> {
        for_term!(self; t => t.is_local_function())
    }

    #[inline]
    fn is_local_pi(&self) -> Option<bool> {
        for_term!(self; t => t.is_local_pi())
    }

    #[inline]
    fn is_root(&self) -> bool {
        for_term!(self; t => t.is_root())
    }

    #[inline]
    fn coerce(
        &self,
        ty: Option<TermId>,
        ctx: &mut (impl TyCtxMut + ?Sized),
    ) -> Result<Option<Term>, Error> {
        for_term!(self; t => Ok(t.coerce(ty, ctx)?.map(Value::into_term_direct)))
    }

    #[inline]
    fn coerce_id(
        &self,
        ty: Option<TermId>,
        ctx: &mut (impl TyCtxMut + ?Sized),
    ) -> Result<Option<TermId>, Error> {
        for_term!(self; t => t.coerce_id(ty, ctx))
    }

    #[inline]
    fn coerced(
        &self,
        ty: Option<TermId>,
        ctx: &mut (impl TyCtxMut + ?Sized),
    ) -> Result<Term, Error> {
        for_term!(self; t => Ok(t.coerced(ty, ctx)?.into_term_direct()))
    }

    #[inline]
    fn coerced_id(
        &self,
        ty: Option<TermId>,
        ctx: &mut (impl TyCtxMut + ?Sized),
    ) -> Result<TermId, Error> {
        for_term!(self; t => t.coerced_id(ty, ctx))
    }

    #[inline]
    fn annotate_unchecked(
        &self,
        annot: Annotation,
        ctx: &mut (impl ConsCtx + ?Sized),
    ) -> Result<Option<Term>, Error> {
        for_term!(self; t => Ok(t.annotate_unchecked(annot, ctx)?.map(Value::into_term_direct)))
    }

    #[inline]
    fn annotate_unchecked_id(
        &self,
        annot: Annotation,
        ctx: &mut (impl ConsCtx + ?Sized),
    ) -> Result<Option<TermId>, Error> {
        for_term!(self; t => t.annotate_unchecked_id(annot, ctx))
    }

    #[inline]
    fn annotated_unchecked(
        &self,
        annot: Annotation,
        ctx: &mut (impl ConsCtx + ?Sized),
    ) -> Result<Term, Error> {
        for_term!(self; t => Ok(t.annotated_unchecked(annot, ctx)?.into_term_direct()))
    }

    #[inline]
    fn annotated_unchecked_id(
        &self,
        annot: Annotation,
        ctx: &mut (impl ConsCtx + ?Sized),
    ) -> Result<TermId, Error> {
        for_term!(self; t => t.annotated_unchecked_id(annot, ctx))
    }

    #[inline]
    fn is_const(&self) -> bool {
        for_term!(self; t => t.is_const())
    }

    #[inline]
    fn is_subtype_in(&self, other: &Term, ctx: &mut (impl TermEqCtxMut + ?Sized)) -> Option<bool> {
        Some(
            ctx.term_eq_mut(self, other).unwrap_or(false)
                || for_term!(self; t => t.is_subtype_in(other, ctx)?),
        )
    }

    #[inline]
    fn coerce_annot_ty(
        &self,
        target: &TermId,
        ctx: &mut (impl TyCtxMut + ?Sized),
    ) -> Result<Option<Annotation>, Error> {
        for_term!(self; t => t.coerce_annot_ty(target, ctx))
    }

    #[inline]
    fn universe(&self) -> Option<Universe> {
        for_term!(self; t => t.universe())
    }

    #[inline]
    fn code(&self) -> Code {
        for_term!(self; t => t.code())
    }

    #[inline]
    fn untyped_code(&self) -> Code {
        for_term!(self; t => t.untyped_code())
    }

    #[inline]
    fn subst_rec_id(&self, ctx: &mut (impl SubstCtx + ?Sized)) -> Result<Option<TermId>, Error> {
        for_term!(self; t => t.subst_rec_id(ctx))
    }

    #[inline]
    fn subst_id(&self, ctx: &mut (impl SubstCtx + ?Sized)) -> Result<Option<TermId>, Error> {
        if !ctx.intersects(self.get_filter(), self.code(), self.form()) {
            return Ok(None);
        }
        if let Some(cached) = ctx.try_subst(self) {
            return cached;
        }
        self.subst_rec_id(ctx)
    }

    #[inline]
    fn shift_id(
        &self,
        n: i32,
        base: u32,
        ctx: &mut (impl ConsCtx + ?Sized),
    ) -> Result<Option<TermId>, Error> {
        for_term!(self; t => t.shift_id(n, base, ctx))
    }

    #[inline]
    fn eq_term_in(&self, other: &Term, ctx: &mut (impl TermEqCtxMut + ?Sized)) -> Option<bool> {
        if let Some(result) = ctx.approx_term_eq_mut(self, other) {
            return result;
        }
        for_term!(self; t => t.eq_term_in(other, ctx))
    }

    #[inline]
    fn eq_id_in(&self, other: &TermId, ctx: &mut (impl TermEqCtxMut + ?Sized)) -> Option<bool> {
        if let Some(result) = ctx.approx_term_eq_mut(self, other) {
            return result;
        }
        for_term!(self; t => t.eq_id_in(other, ctx))
    }

    #[inline]
    fn cons_id(&self, ctx: &mut (impl ConsCtx + ?Sized)) -> Option<TermId> {
        for_term!(self; t => t.cons_id(ctx))
    }

    #[inline]
    fn into_id_with(self, ctx: &mut (impl ConsCtx + ?Sized)) -> TermId {
        for_term!(self; t => t.into_id_with(ctx))
    }

    fn apply(
        &self,
        args: &mut SmallVec<[TermId; 2]>,
        ctx: &mut (impl EvalCtx + ?Sized),
    ) -> Result<Option<TermId>, Error> {
        for_term!(self; t => t.apply(args, ctx))
    }

    #[inline]
    fn apply_ty(
        &self,
        args: &mut SmallVec<[TermId; 2]>,
        ctx: &mut (impl EvalCtx + ?Sized),
    ) -> Result<Option<TermId>, Error> {
        for_term!(self; t => t.apply_ty(args, ctx))
    }

    #[inline]
    fn into_id_direct(self) -> TermId {
        for_term!(self; t => t.into_id_direct())
    }

    #[inline]
    fn clone_into_id_direct(&self) -> TermId {
        for_term!(self; t => t.clone_into_id_direct())
    }

    #[inline]
    fn clone_into_id_with(&self, ctx: &mut (impl ConsCtx + ?Sized)) -> TermId {
        for_term!(self; t => t.clone_into_id_with(ctx))
    }

    #[inline]
    fn clone_into_term_direct(&self) -> Term {
        self.clone()
    }

    #[inline]
    fn form(&self) -> Form {
        for_term!(self; t => t.form())
    }
}

impl TermId {
    /// Check whether two `TermId`s point to the same data
    #[inline]
    pub fn ptr_eq(&self, other: &TermId) -> bool {
        Arc::ptr_eq(&self.0, &other.0)
    }

    /// Get a pointer to the data underlying this `TermId`
    #[inline]
    pub fn as_ptr(&self) -> *const Term {
        Arc::as_ptr(&self.0)
    }

    /// Shift this term's variables with index `>= base` up by `n` in a given context
    #[inline]
    pub fn shift_cow(
        &self,
        n: i32,
        base: u32,
        ctx: &mut (impl ConsCtx + ?Sized),
    ) -> Result<Cow<TermId>, Error> {
        if let Some(shifted) = self.shift(n, base, ctx)? {
            Ok(Cow::Owned(shifted))
        } else {
            Ok(Cow::Borrowed(self))
        }
    }
}

impl Deref for TermId {
    type Target = Term;

    #[inline]
    fn deref(&self) -> &Self::Target {
        &*self.0
    }
}

impl Borrow<Term> for TermId {
    #[inline]
    fn borrow(&self) -> &Term {
        &*self.0
    }
}

impl PartialEq for TermId {
    #[inline]
    fn eq(&self, other: &TermId) -> bool {
        Arc::ptr_eq(&self.0, &other.0) || self.0 == other.0
    }
}

impl PartialEq<Term> for TermId {
    #[inline]
    fn eq(&self, other: &Term) -> bool {
        Arc::as_ptr(&self.0) == other as *const _ || *self.0 == *other
    }
}

impl PartialEq<*const Term> for TermId {
    #[inline]
    fn eq(&self, other: &*const Term) -> bool {
        Arc::as_ptr(&self.0) == *other
    }
}

impl PartialEq<TermId> for *const Term {
    #[inline]
    fn eq(&self, other: &TermId) -> bool {
        Arc::as_ptr(&other.0) == *self
    }
}

impl PartialEq<TermId> for Term {
    #[inline]
    fn eq(&self, other: &TermId) -> bool {
        other.eq(self)
    }
}

impl Hash for TermId {
    #[inline]
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.code().hash(state)
    }
}

impl Hash for Term {
    #[inline]
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.code().hash(state)
    }
}

impl Debug for TermId {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        Debug::fmt(&self.0, f)
    }
}

impl Debug for Term {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        for_term!(self; t => Debug::fmt(t, f))
    }
}

/// A convenience trait implemented by `Option<V>` where `V` is a value
pub trait OptionalValue: Substitute + Cons {
    /// Get the type annotation of this term, or `None` if this term is `None`
    fn annot_opt(&self) -> Option<AnnotationRef>;

    /// Get whether this term is a type *in all contexts*, or `None` if this term is `None`
    fn is_local_ty(&self) -> Option<bool>;

    /// Get whether this term has a universe *in all contexts*, or `None` if this term is `None`
    fn universe_opt(&self) -> Option<Universe>;

    /// Get the hash-code of this term, or `None` if this term is `None`
    fn code(&self) -> Option<Code>;

    /// Get the hash-code of this term if it was untyped, or `None` if this term is `None`
    fn untyped_code(&self) -> Option<Code>;

    /// Load this term's current flags, or `None` if this term is `None`
    fn load_flags_opt(&self) -> Option<TyckFlags>;

    /// Whether two optional terms are equal
    fn eq_in_opt(&self, other: &Self, ctx: &mut (impl TermEqCtxMut + ?Sized)) -> Option<bool>;
}

impl<V: Value> OptionalValue for Option<V> {
    #[inline]
    fn annot_opt(&self) -> Option<AnnotationRef> {
        self.as_ref().map(|term| term.annot()).flatten()
    }

    #[inline]
    fn is_local_ty(&self) -> Option<bool> {
        self.as_ref().map(|term| term.is_local_ty()).flatten()
    }

    #[inline]
    fn universe_opt(&self) -> Option<Universe> {
        self.as_ref().map(|term| term.universe()).flatten()
    }

    #[inline]
    fn code(&self) -> Option<Code> {
        self.as_ref().map(|term| term.code())
    }

    #[inline]
    fn untyped_code(&self) -> Option<Code> {
        self.as_ref().map(|term| term.untyped_code())
    }

    #[inline]
    fn load_flags_opt(&self) -> Option<TyckFlags> {
        self.as_ref().map(|term| term.load_flags())
    }

    #[inline]
    fn eq_in_opt(&self, other: &Self, ctx: &mut (impl TermEqCtxMut + ?Sized)) -> Option<bool> {
        match (self, other) {
            (None, None) => Some(true),
            (Some(this), Some(other)) => this.eq_in(other, ctx),
            _ => Some(false),
        }
    }
}

impl<V: Cons> Cons for &'_ V {
    type Consed = V::Consed;

    #[inline]
    fn cons(&self, ctx: &mut (impl ConsCtx + ?Sized)) -> Option<Self::Consed> {
        (*self).cons(ctx)
    }

    #[inline]
    fn to_consed_ty(&self) -> Self::Consed {
        (*self).to_consed_ty()
    }
}

impl<V: Cons> Cons for Option<V> {
    type Consed = Option<V::Consed>;

    #[inline]
    fn cons(&self, ctx: &mut (impl ConsCtx + ?Sized)) -> Option<Self::Consed> {
        if let Some(this) = self {
            if let Some(consed) = this.cons(ctx) {
                Some(Some(consed))
            } else {
                None
            }
        } else {
            None
        }
    }

    #[inline]
    fn to_consed_ty(&self) -> Self::Consed {
        if let Some(this) = self {
            Some(this.to_consed_ty())
        } else {
            None
        }
    }
}

impl<V: Substitute> Substitute for &'_ V {
    type Substituted = V::Substituted;

    #[inline]
    fn subst_rec(
        &self,
        ctx: &mut (impl SubstCtx + ?Sized),
    ) -> Result<Option<V::Substituted>, Error> {
        (*self).subst_rec(ctx)
    }

    #[inline]
    fn from_consed(this: Self::Consed, ctx: &mut (impl ConsCtx + ?Sized)) -> Self::Substituted {
        V::from_consed(this, ctx)
    }
}

impl<V: Substitute> Substitute for Option<V> {
    type Substituted = Option<V::Substituted>;

    #[inline]
    fn subst_rec(
        &self,
        ctx: &mut (impl SubstCtx + ?Sized),
    ) -> Result<Option<Option<V::Substituted>>, Error> {
        if let Some(this) = self {
            this.subst_rec(ctx).map(|subst| subst.map(Some))
        } else {
            Ok(None)
        }
    }

    #[inline]
    fn from_consed(this: Self::Consed, ctx: &mut (impl ConsCtx + ?Sized)) -> Self::Substituted {
        if let Some(this) = this {
            Some(V::from_consed(this, ctx))
        } else {
            None
        }
    }
}

impl<V: Typecheck> Typecheck for &'_ V {
    fn do_local_tyck(&self, ctx: &mut (impl ConsCtx + ?Sized)) -> bool {
        (*self).do_local_tyck(ctx)
    }

    fn do_global_tyck(&self, ctx: &mut (impl TyCtxMut + ?Sized)) -> Option<bool> {
        (*self).do_global_tyck(ctx)
    }

    #[inline]
    fn do_annot_tyck(&self, ctx: &mut (impl TyCtxMut + ?Sized)) -> Option<bool> {
        (*self).do_annot_tyck(ctx)
    }

    #[inline]
    fn local_tyck(&self, ctx: &mut (impl ConsCtx + ?Sized)) -> bool {
        (*self).local_tyck(ctx)
    }

    #[inline]
    fn global_tyck(&self, ctx: &mut (impl TyCtxMut + ?Sized)) -> Option<bool> {
        (*self).global_tyck(ctx)
    }

    #[inline]
    fn annot_tyck(&self, ctx: &mut (impl TyCtxMut + ?Sized)) -> Option<bool> {
        (*self).annot_tyck(ctx)
    }

    #[inline]
    fn tyck(&self, ctx: &mut (impl TyCtxMut + ?Sized)) -> Option<bool> {
        (*self).tyck(ctx)
    }

    #[inline]
    fn load_flags(&self) -> TyckFlags {
        (*self).load_flags()
    }

    #[inline]
    fn set_flags(&self, flags: TyckFlags) {
        (*self).set_flags(flags)
    }

    #[inline]
    fn tyck_var(&self, ctx: &mut (impl TyCtxMut + ConsCtx + ?Sized)) -> Option<bool> {
        (*self).tyck_var(ctx)
    }

    #[inline]
    fn maybe_tyck(&self) -> bool {
        (*self).maybe_tyck()
    }
}

impl<V: Typecheck> Typecheck for Option<V> {
    fn do_local_tyck(&self, ctx: &mut (impl ConsCtx + ?Sized)) -> bool {
        if let Some(this) = self {
            this.do_local_tyck(ctx)
        } else {
            false
        }
    }

    fn do_global_tyck(&self, ctx: &mut (impl TyCtxMut + ?Sized)) -> Option<bool> {
        if let Some(this) = self {
            this.do_global_tyck(ctx)
        } else {
            Some(false)
        }
    }

    #[inline]
    fn do_annot_tyck(&self, ctx: &mut (impl TyCtxMut + ?Sized)) -> Option<bool> {
        if let Some(this) = self {
            this.do_annot_tyck(ctx)
        } else {
            Some(false)
        }
    }

    #[inline]
    fn local_tyck(&self, ctx: &mut (impl ConsCtx + ?Sized)) -> bool {
        if let Some(this) = self {
            this.local_tyck(ctx)
        } else {
            false
        }
    }

    #[inline]
    fn global_tyck(&self, ctx: &mut (impl TyCtxMut + ?Sized)) -> Option<bool> {
        if let Some(this) = self {
            this.global_tyck(ctx)
        } else {
            Some(false)
        }
    }

    #[inline]
    fn annot_tyck(&self, ctx: &mut (impl TyCtxMut + ?Sized)) -> Option<bool> {
        if let Some(this) = self {
            this.annot_tyck(ctx)
        } else {
            Some(false)
        }
    }

    #[inline]
    fn tyck(&self, ctx: &mut (impl TyCtxMut + ?Sized)) -> Option<bool> {
        if let Some(this) = self {
            this.tyck(ctx)
        } else {
            Some(false)
        }
    }

    #[inline]
    fn load_flags(&self) -> TyckFlags {
        if let Some(this) = self {
            this.load_flags()
        } else {
            TyckFlags::NIL
        }
    }

    #[inline]
    fn set_flags(&self, flags: TyckFlags) {
        if let Some(this) = self {
            this.set_flags(flags)
        }
        //TODO: check flags are compatible with nil
    }

    #[inline]
    fn tyck_var(&self, ctx: &mut (impl TyCtxMut + ConsCtx + ?Sized)) -> Option<bool> {
        if let Some(this) = self {
            this.tyck_var(ctx)
        } else {
            Some(false)
        }
    }

    #[inline]
    fn maybe_tyck(&self) -> bool {
        if let Some(this) = self {
            this.maybe_tyck()
        } else {
            false
        }
    }
}

impl<V: HasDependencies> HasDependencies for &'_ V {
    #[inline]
    fn has_var_dep(&self, ix: u32, equiv: bool) -> bool {
        (*self).has_var_dep(ix, equiv)
    }

    fn has_dep_below(&self, ix: u32, base: u32) -> bool {
        (*self).has_dep_below(ix, base)
    }

    #[inline]
    fn get_filter(&self) -> VarFilter {
        (*self).get_filter()
    }

    #[inline]
    fn fvb(&self) -> u32 {
        (*self).fvb()
    }
}

impl<V: HasDependencies> HasDependencies for Option<V> {
    #[inline]
    fn has_var_dep(&self, ix: u32, equiv: bool) -> bool {
        if let Some(this) = self {
            this.has_var_dep(ix, equiv)
        } else {
            false
        }
    }

    #[inline]
    fn has_dep_below(&self, ix: u32, base: u32) -> bool {
        if let Some(this) = self {
            this.has_dep_below(ix, base)
        } else {
            false
        }
    }

    #[inline]
    fn get_filter(&self) -> VarFilter {
        if let Some(this) = self {
            this.get_filter()
        } else {
            VarFilter::EMPTY
        }
    }
}

impl<V: TermEq> TermEq for &'_ V {
    #[inline]
    fn eq_in(&self, other: &Self, ctx: &mut (impl TermEqCtxMut + ?Sized)) -> Option<bool> {
        (*self).eq_in(*other, ctx)
    }
}

impl<V: TermEq> TermEq for Option<V> {
    #[inline]
    fn eq_in(&self, other: &Self, ctx: &mut (impl TermEqCtxMut + ?Sized)) -> Option<bool> {
        match (self, other) {
            (Some(this), Some(other)) => this.eq_in(other, ctx),
            (None, None) => Some(true),
            _ => Some(false),
        }
    }
}

impl<V: Value> Value for &'_ V {
    type ValueConsed = V::ValueConsed;
    type ValueSubstituted = V::ValueSubstituted;

    #[inline]
    fn into_term_direct(self) -> Term {
        (*self).clone_into_term_direct()
    }

    #[inline]
    fn annot(&self) -> Option<AnnotationRef> {
        (**self).annot()
    }

    // #[inline]
    fn id(&self) -> Option<NodeIx> {
        (*self).id()
    }

    #[inline]
    fn is_local_ty(&self) -> Option<bool> {
        (*self).is_local_ty()
    }

    #[inline]
    fn is_local_universe(&self) -> Option<bool> {
        (*self).is_local_universe()
    }

    #[inline]
    fn is_local_function(&self) -> Option<bool> {
        (**self).is_local_function()
    }

    #[inline]
    fn is_local_pi(&self) -> Option<bool> {
        (**self).is_local_pi()
    }

    #[inline]
    fn is_root(&self) -> bool {
        (*self).is_root()
    }

    #[inline]
    fn coerce(
        &self,
        ty: Option<TermId>,
        ctx: &mut (impl TyCtxMut + ?Sized),
    ) -> Result<Option<V::ValueConsed>, Error> {
        (*self).coerce(ty, ctx)
    }

    #[inline]
    fn coerce_id(
        &self,
        ty: Option<TermId>,
        ctx: &mut (impl TyCtxMut + ?Sized),
    ) -> Result<Option<TermId>, Error> {
        (*self).coerce_id(ty, ctx)
    }

    #[inline]
    fn coerced(
        &self,
        ty: Option<TermId>,
        ctx: &mut (impl TyCtxMut + ?Sized),
    ) -> Result<Self::ValueConsed, Error> {
        (*self).coerced(ty, ctx)
    }

    #[inline]
    fn coerced_id(
        &self,
        ty: Option<TermId>,
        ctx: &mut (impl TyCtxMut + ?Sized),
    ) -> Result<TermId, Error> {
        (*self).coerced_id(ty, ctx)
    }

    #[inline]
    fn annotate_unchecked(
        &self,
        annot: Annotation,
        ctx: &mut (impl ConsCtx + ?Sized),
    ) -> Result<Option<V::ValueConsed>, Error> {
        (*self).annotate_unchecked(annot, ctx)
    }

    #[inline]
    fn annotate_unchecked_id(
        &self,
        annot: Annotation,
        ctx: &mut (impl ConsCtx + ?Sized),
    ) -> Result<Option<TermId>, Error> {
        (*self).annotate_unchecked_id(annot, ctx)
    }

    #[inline]
    fn annotated_unchecked(
        &self,
        annot: Annotation,
        ctx: &mut (impl ConsCtx + ?Sized),
    ) -> Result<Self::ValueConsed, Error> {
        (*self).annotated_unchecked(annot, ctx)
    }

    #[inline]
    fn annotated_unchecked_id(
        &self,
        annot: Annotation,
        ctx: &mut (impl ConsCtx + ?Sized),
    ) -> Result<TermId, Error> {
        (*self).annotated_unchecked_id(annot, ctx)
    }

    #[inline]
    fn is_const(&self) -> bool {
        (*self).is_const()
    }

    fn is_subtype_in(&self, other: &Term, ctx: &mut (impl TermEqCtxMut + ?Sized)) -> Option<bool> {
        (*self).is_subtype_in(other, ctx)
    }

    #[inline]
    fn coerce_annot_ty(
        &self,
        target: &TermId,
        ctx: &mut (impl TyCtxMut + ?Sized),
    ) -> Result<Option<Annotation>, Error> {
        (*self).coerce_annot_ty(target, ctx)
    }

    #[inline]
    fn universe(&self) -> Option<Universe> {
        (*self).universe()
    }

    #[inline]
    fn fn_ty(&self) -> Option<&Pi> {
        (*self).fn_ty()
    }

    #[inline]
    fn code(&self) -> Code {
        (*self).code()
    }

    #[inline]
    fn untyped_code(&self) -> Code {
        (*self).untyped_code()
    }

    #[inline]
    fn subst_id(&self, ctx: &mut (impl SubstCtx + ?Sized)) -> Result<Option<TermId>, Error> {
        (*self).subst_id(ctx)
    }

    #[inline]
    fn shift_id(
        &self,
        n: i32,
        base: u32,
        ctx: &mut (impl ConsCtx + ?Sized),
    ) -> Result<Option<TermId>, Error> {
        (*self).shift_id(n, base, ctx)
    }

    #[inline]
    fn eq_term_in(&self, other: &Term, ctx: &mut (impl TermEqCtxMut + ?Sized)) -> Option<bool> {
        (*self).eq_term_in(other, ctx)
    }

    #[inline]
    fn eq_id_in(&self, other: &TermId, ctx: &mut (impl TermEqCtxMut + ?Sized)) -> Option<bool> {
        (*self).eq_id_in(other, ctx)
    }

    #[inline]
    fn cons_id(&self, ctx: &mut (impl ConsCtx + ?Sized)) -> Option<TermId> {
        (*self).cons_id(ctx)
    }

    #[inline]
    fn into_id_with(self, ctx: &mut (impl ConsCtx + ?Sized)) -> TermId {
        (*self).clone_into_id_with(ctx)
    }

    #[inline]
    fn ty(&self) -> Option<Cow<Term>> {
        (*self).ty()
    }

    #[inline]
    fn base(&self) -> Option<Cow<Term>> {
        (*self).base()
    }

    #[inline]
    fn apply(
        &self,
        args: &mut SmallVec<[TermId; 2]>,
        ctx: &mut (impl EvalCtx + ?Sized),
    ) -> Result<Option<TermId>, Error> {
        (*self).apply(args, ctx)
    }

    #[inline]
    fn apply_ty(
        &self,
        args: &mut SmallVec<[TermId; 2]>,
        ctx: &mut (impl EvalCtx + ?Sized),
    ) -> Result<Option<TermId>, Error> {
        (*self).apply_ty(args, ctx)
    }

    #[inline]
    fn into_id_direct(self) -> TermId {
        (*self).clone_into_id_direct()
    }

    #[inline]
    fn clone_into_id_direct(&self) -> TermId {
        (*self).clone_into_id_direct()
    }

    #[inline]
    fn clone_into_id_with(&self, ctx: &mut (impl ConsCtx + ?Sized)) -> TermId {
        (*self).clone_into_id_with(ctx)
    }

    #[inline]
    fn clone_into_term_direct(&self) -> Term {
        (*self).clone_into_term_direct()
    }

    #[inline]
    fn into_shallow_cons(self, ctx: &mut (impl ConsCtx + ?Sized)) -> TermId {
        (*self).clone_into_shallow_cons(ctx)
    }

    #[inline]
    fn clone_into_shallow_cons(&self, ctx: &mut (impl ConsCtx + ?Sized)) -> TermId {
        (*self).clone_into_shallow_cons(ctx)
    }

    #[inline]
    fn form(&self) -> Form {
        (**self).form()
    }
}

impl From<Term> for TermId {
    #[inline]
    fn from(t: Term) -> TermId {
        t.into_id_direct()
    }
}

impl Cons for Arc<[TermId]> {
    type Consed = Arc<[TermId]>;

    fn cons(&self, ctx: &mut (impl ConsCtx + ?Sized)) -> Option<Self::Consed> {
        let mut all_consed = true;
        for term in self.iter() {
            if !ctx.check_cons(term) {
                all_consed = false;
                break;
            }
        }
        if all_consed {
            return None;
        }
        Some(Arc::from(
            &self
                .iter()
                .map(|term| term.consed(ctx))
                .collect::<SmallVec<[TermId; 16]>>()[..],
        ))
    }

    #[inline]
    fn to_consed_ty(&self) -> Self::Consed {
        self.clone()
    }
}

impl Substitute for Arc<[TermId]> {
    type Substituted = Arc<[TermId]>;

    fn subst_rec(
        &self,
        ctx: &mut (impl SubstCtx + ?Sized),
    ) -> Result<Option<Self::Substituted>, Error> {
        //TODO: optimize?
        let mut all_null = true;
        let buf: SmallVec<[TermId; 16]> = self
            .iter()
            .map(|term| {
                if let Some(term) = term.subst_rec(ctx).transpose() {
                    all_null = false;
                    term
                } else {
                    Ok(term.clone())
                }
            })
            .collect::<Result<_, _>>()?;
        if all_null {
            Ok(None)
        } else {
            Ok(Some(Arc::from(&buf[..])))
        }
    }

    #[inline]
    fn from_consed(this: Self::Consed, _ctx: &mut (impl ConsCtx + ?Sized)) -> Self::Substituted {
        this
    }
}

impl TermEq for Arc<[TermId]> {
    fn eq_in(&self, other: &Self, ctx: &mut (impl TermEqCtxMut + ?Sized)) -> Option<bool> {
        (**self).eq_in(&**other, ctx)
    }
}

impl TermEq for [TermId] {
    fn eq_in(&self, other: &Self, ctx: &mut (impl TermEqCtxMut + ?Sized)) -> Option<bool> {
        if self.len() != other.len() {
            return Some(false);
        }
        let mut is_none = false;
        for (i, x) in self.iter().enumerate() {
            match x.eq_in(&other[i], ctx) {
                Some(true) => {}
                Some(false) => return Some(false),
                None => is_none = true,
            }
        }
        if is_none {
            None
        } else {
            Some(true)
        }
    }
}

impl HasDependencies for Arc<[TermId]> {
    #[inline]
    fn has_var_dep(&self, ix: u32, equiv: bool) -> bool {
        (**self).has_var_dep(ix, equiv)
    }

    #[inline]
    fn has_dep_below(&self, ix: u32, base: u32) -> bool {
        (**self).has_dep_below(ix, base)
    }

    #[inline]
    fn get_filter(&self) -> VarFilter {
        (**self).get_filter()
    }
}

impl HasDependencies for [TermId] {
    #[inline]
    fn has_var_dep(&self, ix: u32, equiv: bool) -> bool {
        for term in self.iter() {
            if term.has_var_dep(ix, equiv) {
                return true;
            }
        }
        false
    }

    #[inline]
    fn has_dep_below(&self, ix: u32, base: u32) -> bool {
        for term in self.iter() {
            if term.has_dep_below(ix, base) {
                return true;
            }
        }
        false
    }

    #[inline]
    fn get_filter(&self) -> VarFilter {
        let mut filter = VarFilter::EMPTY;
        for term in self.iter() {
            filter = filter.union(term.get_filter());
        }
        filter
    }
}
