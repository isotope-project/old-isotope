use isotope::builder::Builder;
use isotope::prelude::*;
use isotope::primitive::logical::*;
use isotope::term::Value;
use isotope_parser::*;
use smallvec::smallvec;

fn expr_total(input: &str) -> ast::Expr {
    let (rest, output) = expr(input).unwrap();
    assert_eq!(rest, "");
    output
}

#[test]
fn bool_lambda_binary_projections() {
    let mut builder: Builder = Default::default();
    let binary = builder
        .expr(&expr_total("#bool -> #bool -> #bool"))
        .unwrap();
    let unary = builder.expr(&expr_total("#bool -> #bool")).unwrap();
    let pi0 = builder.expr(&expr_total("λ_: #bool.λ_: #bool.#1")).unwrap();
    assert_eq!(*pi0.ty().unwrap(), binary);
    let pi1 = builder.expr(&expr_total("λ_: #bool.λ_: #bool.#0")).unwrap();
    assert_eq!(*pi1.ty().unwrap(), binary);
    let id = builder.expr(&expr_total("λ_: #bool.#0")).unwrap();
    assert_eq!(*id.ty().unwrap(), unary);
    let bool_ = BOOL.clone();
    let bools = [TRUE.clone(), FALSE.clone()];
    for x in &bools {
        let cx = Lambda::new_direct(Some(bool_.clone()), x.clone(), builder.ctx().cons_ctx())
            .into_id_with(builder.ctx().cons_ctx());
        assert_eq!(*cx.ty().unwrap(), unary);

        let mut argv = smallvec![x.clone()];
        assert_eq!(
            pi0.apply(&mut argv, &mut SubstVec::new(builder.ctx()))
                .unwrap()
                .unwrap(),
            cx
        );
        assert!(argv.is_empty());

        let pi0_x =
            App::new(pi0.clone(), x.clone(), builder.ctx()).into_id_with(builder.ctx().cons_ctx());
        assert_ne!(pi0_x, cx);
        assert_eq!(
            pi0_x
                .normalized(Form::Head, u64::MAX, builder.ctx())
                .unwrap(),
            cx
        );

        let mut argv = smallvec![x.clone()];
        assert_eq!(
            pi1.apply(&mut argv, &mut SubstVec::new(builder.ctx()))
                .unwrap()
                .unwrap(),
            id
        );
        assert!(argv.is_empty());

        let pi1_x =
            App::new(pi1.clone(), x.clone(), builder.ctx()).into_id_with(builder.ctx().cons_ctx());
        assert_ne!(pi1_x, id);
        assert_eq!(
            pi1_x
                .normalized(Form::Head, u64::MAX, builder.ctx())
                .unwrap(),
            id
        );

        for y in &bools {
            let mut argv = smallvec![y.clone()];
            assert_eq!(
                cx.apply(&mut argv, &mut SubstVec::new(builder.ctx()))
                    .unwrap()
                    .unwrap(),
                *x
            );
            assert!(argv.is_empty());

            let cx_y = App::new(cx.clone(), y.clone(), builder.ctx())
                .into_id_with(builder.ctx().cons_ctx());
            assert_ne!(cx_y, *x);
            assert_eq!(
                cx_y.normalized(Form::Head, u64::MAX, builder.ctx())
                    .unwrap(),
                *x
            );

            let mut argv = smallvec![y.clone(), x.clone()];
            assert_eq!(
                pi0.apply(&mut argv, &mut SubstVec::new(builder.ctx()))
                    .unwrap()
                    .unwrap(),
                *x
            );
            assert!(argv.is_empty());

            let pi0_x_y = App::new(pi0_x.clone(), y.clone(), builder.ctx())
                .into_id_with(builder.ctx().cons_ctx());
            assert_ne!(pi0_x_y, *x);
            assert_eq!(
                pi0_x_y
                    .normalized(Form::Head, u64::MAX, builder.ctx())
                    .unwrap(),
                *x
            );

            let mut argv = smallvec![y.clone(), x.clone()];
            assert_eq!(
                pi1.apply(&mut argv, &mut SubstVec::new(builder.ctx()))
                    .unwrap()
                    .unwrap(),
                *y
            );
            assert!(argv.is_empty());

            let pi1_x_y = App::new(pi1_x.clone(), y.clone(), builder.ctx())
                .into_id_with(builder.ctx().cons_ctx());
            assert_ne!(pi1_x_y, *y);
            assert_eq!(
                pi1_x_y
                    .normalized(Form::Head, u64::MAX, builder.ctx())
                    .unwrap(),
                *y
            );
        }
    }
}
